#!/usr/bin/env python
"""
color_multichrome - Colorize with gradient

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorops as co
from color_lib import colorconvert as cv
from color_lib import colorinterp as ci


class ColorMultichrome(modcol.ColorRGBA):
    """ColorEffect-based class to colorize SVG paints with gradient."""

    def __init__(self):
        """Init base class and ColorMultichrome()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.color1 = None
        self.color2 = None

        # substitute color for black
        self.OptionParser.add_option("--color1",
                                     action="store", type="int",
                                     dest="color1", default=1364325887,
                                     help="Substitute color (black)")
        # substitute color for white
        self.OptionParser.add_option("--color2",
                                     action="store", type="int",
                                     dest="color2", default=-65281,
                                     help="Substitute color (white)")
        # common
        self.OptionParser.add_option("--reverse",
                                     action="store", type="inkbool",
                                     dest="reverse", default=False,
                                     help="Reverse gradient")
        self.OptionParser.add_option("--color_interp",
                                     action="store", type="string",
                                     dest="color_interp", default="linear",
                                     help="Color interpolation mode")
        self.OptionParser.add_option("--color_space",
                                     action="store", type="string",
                                     dest="color_space", default="HSL",
                                     help="Color space for interpolation")
        self.OptionParser.add_option("--hue_sweep",
                                     action="store", type="string",
                                     dest="hue_sweep", default="min",
                                     help="Hue sweep angle")

    def remap_gradient(self, ink_color):
        """Substitute black and white, remap luminance to color range."""

        # parameters
        sweep = self.options.hue_sweep
        colorspace = self.options.color_space
        color_interp = self.options.color_interp

        if colorspace == 'linearRGB':
            # co.luma() for linearRGB (rec709)
            lum = co.luminance(*ink_color.rgb_linear())
        elif colorspace in ('LUV', 'LAB', 'LCH'):
            lum = cv.rgb_to_lab(*ink_color.rgb_float())[0] / 100.0
        else:
            # co.luma() rec601 for sRGB (rec601)
            lum = co.luma(*ink_color.rgb_float(), mode='rec601')

        # interpolate color1, color2 with luma as *time*
        func = getattr(ci, '{}_ic'.format(color_interp), None)
        if func is not None:
            new_color = func(self.color1, self.color2, lum, colorspace, sweep)

        # update ink_color
        ink_color.from_float(*new_color.rgb_float())

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if (self.options.tab == '"color1_tab"' or
                self.options.tab == '"color2_tab"'):
            self.remap_gradient(ink_color)

    def prepare_doc(self):
        """Prepare common stuff used later on to modify current color."""
        if (self.options.tab == '"color1_tab"' or
                self.options.tab == '"color2_tab"'):
            # modcol.inkex.debug(self.options.color1)
            ic_color1 = ic.InkColorRGBALong(self.options.color1)
            # modcol.inkex.debug(self.options.color2)
            ic_color2 = ic.InkColorRGBALong(self.options.color2)
            # store substitute colors as instance attributes
            if self.options.reverse:
                ic_color1, ic_color2 = ic_color2, ic_color1
            self.color1 = ic_color1
            self.color2 = ic_color2


if __name__ == '__main__':
    ME = ColorMultichrome()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
