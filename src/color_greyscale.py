#!/usr/bin/env python
"""
color_greyscale - adjust greyscale of SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colorops as co
from color_lib import colormatrix as cm


class ColorGreyscale(modcol.ColorRGBA):
    """ColorEffect-based class to convert SVG paint colors to greyscale."""

    def __init__(self):
        """Init base class and ColorGreyscale()."""
        modcol.ColorRGBA.__init__(self)

        # presets
        self.OptionParser.add_option("--grsc_preset",
                                     action="store", type="string",
                                     dest="grsc_preset", default="luma_rec601",
                                     help="Greyscale preset parameters")
        self.OptionParser.add_option("--rgb_posterize_count",
                                     action="store", type="int",
                                     dest="rgb_posterize_count", default="5",
                                     help="Number of RGB posterize levels")
        # custom
        self.OptionParser.add_option("--grsc_red",
                                     action="store", type="float",
                                     dest="grsc_red", default="0.213",
                                     help="Greyscale level of R component")
        self.OptionParser.add_option("--grsc_blue",
                                     action="store", type="float",
                                     dest="grsc_blue", default="0.715",
                                     help="Greyscale level of G component")
        self.OptionParser.add_option("--grsc_green",
                                     action="store", type="float",
                                     dest="grsc_green", default="0.072",
                                     help="Greyscale level of B component")
        self.OptionParser.add_option("--grsc_strength",
                                     action="store", type="float",
                                     dest="grsc_strength", default="0.0",
                                     help="Greyscale strength level")
        self.OptionParser.add_option("--grsc_custom_mode",
                                     action="store", type="string",
                                     dest="grsc_custom_mode", default="sRGB",
                                     help="Custom greyscale color space mode")

    def greyscale_custom(self, ink_color):
        """
        Customize greyscale components.

        Based on CPF (Inkscape 0.91)

        feColorMatrix:

             normal       transparency
            R G B St 0    0 0 0 0    0
            R G B St 0    0 0 0 0    0
            R G B St 0    0 0 0 0    0
            0 0 0 1  0    R G B 1-St 0

         """

        # Transparent option only works as filter effect (CPF)
        grsc_transparent = False

        # parameters
        red = self.options.grsc_red
        green = self.options.grsc_green
        blue = self.options.grsc_blue
        strength = self.options.grsc_strength

        # split ink_color
        if self.options.grsc_custom_mode == 'linearRGB':
            r, g, b = ink_color.rgb_linear()
        else:  # sRGB
            r, g, b = ink_color.rgb_float()

        mat = [[0.0 for _ in range(5)] for _ in range(5)]
        for i in (3, 4):
            mat[i][i] = 1.0

        if grsc_transparent:
            mat[3][0] = -1 * red
            mat[3][1] = -1 * green
            mat[3][2] = -1 * blue
            mat[3][3] = 1.0 - strength
        else:
            for i in range(3):
                mat[i][0] = red
                mat[i][1] = green
                mat[i][2] = blue
                mat[i][3] = strength

        # apply matrix to current RGB color
        mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))

        # update ink_color
        greyscale = cm.matrix2rgb(mat_rgb_new)
        if self.options.grsc_custom_mode == 'linearRGB':
            ink_color.from_linear(*greyscale)
        else:
            ink_color.from_float(*greyscale)

    def greyscale_preset_luma(self, ink_color, preset):
        """Check and apply greyscale preset based on Luma Y'."""
        # pylint: disable=no-self-use
        new_color = None
        if preset == 'rec601':
            new_color = co.greyscale_luma(ink_color, preset)
        elif preset == 'rec709':
            new_color = co.greyscale_luma(ink_color, preset)
        return new_color

    def greyscale_preset_luminance(self, ink_color, preset):
        """Check and apply greyscale preset based on linear luminance."""
        # pylint: disable=no-self-use
        new_color = None
        if preset == 'linear':
            new_color = co.greyscale_luminance(ink_color, preset)
        elif preset == 'wcag20':
            new_color = co.greyscale_luminance(ink_color, preset)
        return new_color

    def greyscale_preset_rgb(self, ink_color, preset):
        """Check and apply greyscale preset based on RGB channels."""
        new_color = None
        if preset.startswith('lightness'):
            new_color = co.greyscale_rgb_lightness(ink_color)
        elif preset.startswith('average'):
            new_color = co.greyscale_rgb_average(ink_color)
        elif preset.startswith('decompose'):
            if preset.endswith('max'):
                new_color = co.greyscale_rgb_decompose(ink_color, 'max')
            elif preset.endswith('min'):
                new_color = co.greyscale_rgb_decompose(ink_color, 'min')
        elif preset.startswith('channel'):
            if preset.endswith('red'):
                new_color = co.greyscale_rgb_channel(ink_color, 'r')
            elif preset.endswith('green'):
                new_color = co.greyscale_rgb_channel(ink_color, 'g')
            elif preset.endswith('blue'):
                new_color = co.greyscale_rgb_channel(ink_color, 'b')
        elif preset.startswith('posterize'):
            count = self.options.rgb_posterize_count
            new_color = co.greyscale_rgb_posterize(ink_color, count)
        return new_color

    def greyscale_presets(self, ink_color):
        """Apply selected greyscale preset."""
        new_color = None
        preset = self.options.grsc_preset
        if preset == 'none':
            pass
        elif preset.startswith('luma_'):
            # new_color = co.greyscale_luma(ink_color, preset[5:])
            new_color = self.greyscale_preset_luma(ink_color, preset[5:])
        elif preset.startswith('luminance_'):
            # new_color = co.greyscale_luminance(ink_color, preset[10:])
            new_color = self.greyscale_preset_luminance(ink_color, preset[10:])
        elif preset.startswith('rgb_'):
            new_color = self.greyscale_preset_rgb(ink_color, preset[4:])
        elif preset == 'css_grayscale':
            new_color = co.css_grayscale(ink_color, 1.0)
        elif preset == 'blend_luminosity':
            new_color = co.greyscale_blend_luminosity(ink_color)
        elif preset == 'hsl_lightness':
            new_color = co.greyscale_hsl_lightness(ink_color)
        elif preset == 'hsv_value':
            new_color = co.greyscale_hsv_value(ink_color)
        elif preset == 'lab_lightness':
            new_color = co.greyscale_lab_lightness(ink_color)
        elif preset == 'hsp_brightness':
            new_color = co.greyscale_hsp_brightness(ink_color)
        # update ink_color
        if new_color is not None:
            ink_color.from_float(*new_color.rgb_float())

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"grsc_preset_tab"':
            self.greyscale_presets(ink_color)
        elif self.options.tab == '"grsc_custom_tab"':
            self.options.grsc_preset = 'custom'
            self.greyscale_custom(ink_color)


if __name__ == '__main__':
    ME = ColorGreyscale()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
