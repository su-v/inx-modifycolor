#!/usr/bin/env python
"""
color_schemes - create color scheme palette for SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
from re import split

# local library
import inkex
from pathmodifier import zSort
# local library (color_lib)
from color_lib import common as modcol
from color_lib import util
from color_lib import inkcolor as ic
from color_lib import colorconvert as cv
from color_lib import colorsort as cs
from color_lib import colorinterp as ci


def create_rect(x, y, width, height, style):
    """Return a new SVG rect element with style from string *style*."""
    rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    rect.set('x', str(x))
    rect.set('y', str(y))
    rect.set('width', str(width))
    rect.set('height', str(height))
    rect.set('style', style)
    return rect


def create_square(x, y, width, style):
    """Return a square (SVG rect with equal width and height)."""
    return create_rect(x, y, width, width, style)


def range_attrib_ic(ic_color, clist, ic_attrib, delta):
    """Add steps of InkColor attrib *ic_attrib* to *clist*."""
    ic_orig = ic_color.copy()
    val = ic_attrib.fget(ic_color)
    delta = ic.clamp(0.1, abs(delta), 100.0) / 100.0
    while val > 0.0:
        val -= delta
        ic_attrib.fset(ic_color, val)
        clist.insert(0, ic_color.copy())
    ic_color = ic_orig.copy()
    val = ic_attrib.fget(ic_color)
    while val < 1.0:
        val += delta
        ic_attrib.fset(ic_color, val)
        clist.append(ic_color.copy())


def range_comp_cie(ic_color, clist, sprop, delta):
    """Add steps of CIE-based color component *sprop* to *clist*."""
    from_rgb = getattr(cv, 'rgb_to_{}'.format(sprop[:3]), None)
    to_rgb = getattr(cv, '{}_to_rgb'.format(sprop[:3]), None)
    delta = ic.clamp(0.1, abs(delta), 100.0)
    if from_rgb is not None and to_rgb is not None:
        color = from_rgb(*ic_color.rgb_float())
        index = sprop[:3].index(sprop[-1])
        orig_val = color[index]
        while color[index] > 0.0:
            color[index] -= delta
            ic_color.from_float(*to_rgb(*color))
            clist.insert(0, ic_color.copy())
        color[index] = orig_val
        while color[index] < 100.0:
            color[index] += delta
            ic_color.from_float(*to_rgb(*color))
            clist.append(ic_color.copy())


def interp_func_ic(ic_min, ic_max, intervals, colorspace, sweep='min',
                   mode='linear'):
    """Interpolate two InkColor objects in selected color space."""
    # pylint: disable=too-many-arguments
    func = getattr(ci, '{}_ic'.format(mode), None)
    if func is not None:
        icolors = []
        step = 1.0 / intervals
        for i in range(1, intervals):
            new_color = func(ic_min, ic_max, i * step, colorspace, sweep)
            if new_color is not None:
                icolors.append(new_color.copy())
        return icolors


def multihue_interp_func_ic(clist, intervals, colorspace, sweep='min',
                            mode='linear'):
    """Dispatch interpolation of a list of InkColor objects in *mode*."""
    icolors = []
    for i, color in enumerate(clist):
        icolors.append(color.copy())
        if i < len(clist) - 1:
            icolors.extend(
                interp_func_ic(
                    color, clist[i+1], intervals, colorspace, sweep, mode))
    return icolors


def multihue_interp_spline_ic(clist, intervals, colorspace):
    """Spline interpolation of a list of InkColor objects."""
    # pylint: disable=unused-argument,unused-variable
    icolors = []
    steps = ((len(clist) - 1) * intervals) + 1
    gradient = None
    if gradient is not None and len(gradient):
        icolors.extend(gradient)
    return icolors


def multihue_interp_bezier_ic(clist, intervals, colorspace):
    """Bezier interpolation of a list of InkColor objects."""
    icolors = []
    steps = ((len(clist) - 1) * intervals) + 1
    gradient = ci.bezier_ic(clist, steps, colorspace)
    if gradient is not None and len(gradient):
        icolors.extend(gradient)
    return icolors


class ColorPalette(modcol.ColorBase):
    """ColorEffect-based class to render a color scheme palette."""

    def __init__(self):
        """Init base class and ColorPalette()."""
        modcol.ColorBase.__init__(self)

        # palette (output)
        self.OptionParser.add_option("--sort",
                                     action="store",
                                     type="string",
                                     dest="sort",
                                     default="rgb_r",
                                     help="Sort order")
        self.OptionParser.add_option("--reverse",
                                     action="store",
                                     type="inkbool",
                                     dest="reverse",
                                     default=False,
                                     help="Reverse sort order")
        self.OptionParser.add_option("--swatch_size",
                                     action="store",
                                     type="int",
                                     dest="swatch_size",
                                     default=18,
                                     help="Approx. swatch size")
        self.OptionParser.add_option("--swatch_gap",
                                     action="store",
                                     type="int",
                                     dest="swatch_gap",
                                     default=2,
                                     help="Approx. swatch gap")
        self.OptionParser.add_option("--columns",
                                     action="store",
                                     type="int",
                                     dest="columns",
                                     default=0,
                                     help="Columns count, 0 = page width")
        self.OptionParser.add_option("--direction",
                                     action="store",
                                     type="string",
                                     dest="direction",
                                     default="horizontal",
                                     help="Layout of palette")
        self.OptionParser.add_option("--position",
                                     action="store",
                                     type="string",
                                     dest="position",
                                     default="top_left",
                                     help="Position of palette")
        self.OptionParser.add_option("--wrap_group",
                                     action="store",
                                     type="inkbool",
                                     dest="wrap_group",
                                     default=False,
                                     help="Wrap swatches in group")

    # ----- palette (from color_palette.py)

    def counted_colors(self):
        """Return counted colors list based on user input."""
        # counted colors based on color mode
        if self.options.mode == 'RGBA':
            counted = modcol.Counter(self.old_colors)
        else:  # if self.options.mode == 'RGB':
            counted = modcol.Counter(c[:-2] for c in self.old_colors)
        # limit counted colors based on user option
        count = self.options.count or None
        return [color[0] for color in counted.most_common(count)]

    def sort_colors(self, colors):
        """Return sorted colors list based on user input."""
        prop = self.options.sort
        reverse = self.options.reverse
        out_method = None
        if prop.startswith('rgb'):
            out_method = 'rgba_float'
        elif prop.startswith('hsl') or prop.startswith('hsv'):
            out_method = 'rgba_to_{}a'.format(prop[:3])
        elif (prop.startswith('lab') or prop.startswith('lch') or
              prop.startswith('luv')):
            cs.sort_colors_by_cie(colors, prop, reverse)
        if out_method is not None:
            cs.sort_colors_by_ic(colors, prop, out_method, reverse)

    def swatch_size(self, swatch):
        """Set *swatch* size, return palette spacing based on user input."""
        width = int(self.unittouu('{0}px'.format(self.options.swatch_size)))
        gap = int(self.unittouu('{0}px'.format(self.options.swatch_gap)))
        swatch['width'] = width
        return width + gap, gap

    def palette_size(self, colors, spacing, gap):
        """Return palette size based on colors, spacing and user input."""
        if self.options.direction == 'horizontal':
            max_len = self.unittouu(self.getDocumentWidth()) + gap
        else:
            max_len = self.unittouu(self.getDocumentHeight()) + gap
        columns = self.options.columns or int(max_len / spacing)
        rows = util.roundup(len(colors), columns) / columns
        return columns, rows

    def draw_palette(self, colors=None):
        """Draw palette with unique RGB(A) colors in use."""
        # create layer group for palette
        group = self.special_group('Palette', self.options.wrap_group)
        # get counted colors
        if colors is None:
            colors = self.counted_colors()
        # sort colors
        self.sort_colors(colors)
        # init swatch dict
        swatch = {}  # used keys: x, y, width, style
        # set swatch dimension, get palette spacing
        spacing, gap = self.swatch_size(swatch)
        # get palette dimensions
        columns, rows = self.palette_size(colors, spacing, gap)
        # set direction
        row_pos = 'x' if self.options.direction == 'horizontal' else 'y'
        col_pos = 'y' if self.options.direction == 'horizontal' else 'x'

        # set values before initial swatch
        row = 1
        swatch[row_pos] = -spacing
        swatch[col_pos] = 0

        # iterate through all colors and create swatch for each
        ink_color = ic.InkColor()
        for i, color in enumerate(colors):
            if i < row * columns:
                swatch[row_pos] += spacing
            else:
                row += 1
                swatch[row_pos] = 0
                swatch[col_pos] += spacing
            ink_color.from_hex(color)
            swatch['style'] = ink_color.rgba_to_style_str(prop='fill')
            group.append(create_square(**swatch))

        # adjust palette group as needed
        if self.options.direction == 'horizontal':
            if self.options.position == 'top_left':  # top
                transform = 'translate(0,-{})'.format(rows * spacing)
            else:  # bottom
                transform = 'translate(0,{})'.format(
                    self.unittouu(self.getDocumentHeight()) + gap)
        else:
            if self.options.position == 'top_left':  # left
                transform = 'translate(-{},0)'.format(rows * spacing)
            else:  # right
                transform = 'translate({},0)'.format(
                    self.unittouu(self.getDocumentWidth()) + gap)
        group.set('transform', transform)


class ColorSchemes(ColorPalette):
    """ColorEffect-based class to render a color scheme palette."""

    def __init__(self):
        """Init base class and ColorSchemes()."""
        ColorPalette.__init__(self)

        # instance attributes
        self.scheme_presets = {
            'analogous': [5, [30]],
            'complementary': [1, [180]],
            'triadic': [2, [120]],
            'split_complementary': [2, [150, 60]],
            'tetradic': [3, [60, 120]],
            'square': [3, [90]],
            'neutral': [5, [15]],
            'clash': [2, [90, 180]],
            'five_tone': [4, [115, 40, 50, 40]],
            'six_tone': [5, [30, 90]],
        }
        self.multihue_list = []
        self.interp_two = ['linear', 'cosine', 'cubic', 'accel', 'decel']
        self.interp_many = ['spline', 'bezier']

        # color scheme
        self.OptionParser.add_option("--scheme",
                                     action="store",
                                     type="string",
                                     dest="scheme",
                                     default="complementary",
                                     help="Color scheme")
        self.OptionParser.add_option("--scheme_interp_steps",
                                     action="store",
                                     type="int",
                                     dest="scheme_interp_steps",
                                     default=0,
                                     help="Interpolation steps")
        self.OptionParser.add_option("--scheme_interp_mode",
                                     action="store",
                                     type="string",
                                     dest="scheme_interp_mode",
                                     default="linear",
                                     help="Interpolation mode")
        self.OptionParser.add_option("--scheme_color_space",
                                     action="store",
                                     type="string",
                                     dest="scheme_color_space",
                                     default="sRGB",
                                     help="Scheme color model")
        self.OptionParser.add_option("--scheme_interp_sweep",
                                     action="store",
                                     type="string",
                                     dest="scheme_interp_sweep",
                                     default="min",
                                     help="Sweep angle for Hue")
        self.OptionParser.add_option("--scheme_base_source",
                                     action="store",
                                     type="string",
                                     dest="scheme_base_source",
                                     default="custom",
                                     help="Scheme color source")
        self.OptionParser.add_option("--scheme_base_color",
                                     action="store",
                                     type="int",
                                     dest="scheme_base_color",
                                     default=65535,
                                     help="Scheme base color")
        # shades
        self.OptionParser.add_option("--shade_mode",
                                     action="store",
                                     type="string",
                                     dest="shade_mode",
                                     default="hsl_l",
                                     help="Shade mode")
        self.OptionParser.add_option("--shade_step",
                                     action="store",
                                     type="float",
                                     dest="shade_step",
                                     default=10,
                                     help="Shade step")
        self.OptionParser.add_option("--shade_base_source",
                                     action="store",
                                     type="string",
                                     dest="shade_base_source",
                                     default="custom",
                                     help="Shade color source")
        self.OptionParser.add_option("--shade_base_color",
                                     action="store",
                                     type="int",
                                     dest="shade_base_color",
                                     default=65535,
                                     help="Shade base color")
        # multihue selection (option not in INX file)
        self.OptionParser.add_option("--record_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="record_alpha",
                                     default=False,
                                     help="Record alpha")
        # multi-hue interpolation
        self.OptionParser.add_option("--multihue_interp_steps",
                                     action="store",
                                     type="int",
                                     dest="multihue_interp_steps",
                                     default=0,
                                     help="Interpolation steps")
        self.OptionParser.add_option("--multihue_interp_mode",
                                     action="store",
                                     type="string",
                                     dest="multihue_interp_mode",
                                     default="linear",
                                     help="Interpolation mode")
        self.OptionParser.add_option("--multihue_color_space",
                                     action="store",
                                     type="string",
                                     dest="multihue_color_space",
                                     default="sRGB",
                                     help="Color model")
        self.OptionParser.add_option("--multihue_interp_sweep",
                                     action="store",
                                     type="string",
                                     dest="multihue_interp_sweep",
                                     default="min",
                                     help="Sweep angle for Hue")
        self.OptionParser.add_option("--multihue_base_source",
                                     action="store",
                                     type="string",
                                     dest="multihue_base_source",
                                     default="custom",
                                     help="Multihue color source")
        self.OptionParser.add_option("--multihue_base_colors",
                                     action="store",
                                     type="string",
                                     dest="multihue_base_colors",
                                     default="#ff0000, lime, rgb(0,0,255)",
                                     help="Multihue base colors (CSV)")
        self.OptionParser.add_option("--multihue_verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="multihue_verbose",
                                     default=False,
                                     help="Multihue debug output")

    # ----- parse selection or custom input for base color(s)

    def base_colors_named(self, source):
        """Get list of base colors from named colors."""
        # pylint: disable=no-self-use,unused-argument,unused-variable
        msg = ""
        ic_list = []
        msg = ("Base colors from named colors (custom swatches) " +
               "not yet implemented.")
        if msg:
            modcol.inkex.errormsg(msg)
        return ic_list

    def base_colors_custom(self, source):
        """Get list of base colors from custom input (CSV)."""
        # pylint: disable=unused-argument
        msg = ""
        ic_list = []
        string = self.options.multihue_base_colors
        vlist = [v.strip() for v in split(r',\s*(?![^()]*\))', string)]
        if len(vlist):
            ic_color = ic.InkColor()
            for value in vlist:
                if ic.is_color(value):
                    ic_color.from_css(value)
                    ic_list.append(ic_color.copy())
                else:
                    msg = ("'{}' ".format(value) +
                           "is not a valid color specification.")
        else:
            msg = "Failed to split custom input string into list of colors."
        if msg:
            modcol.inkex.errormsg(msg)
        return ic_list

    def base_colors_selected(self, source):
        """Get list of base colors from selection."""
        msg = ""
        ic_list = []
        if source.endswith('obj'):
            sorted_ids = zSort(self.document.getroot(), self.selected.keys())
            if len(sorted_ids) >= 2:
                for node_id in sorted_ids:
                    node = self.selected[node_id]
                    if not modcol.is_group(node):
                        ic_list.append(modcol.ic_object(node))
            if not len(ic_list):
                msg = "This mode reqires two or more selected objects."
        elif source.endswith('group'):
            if not len(self.selected) or len(self.selected) > 1:
                msg = "This mode requires one selected group."
            else:
                node = self.selected[self.options.ids[0]]
                if modcol.is_group(node):
                    for child in node:
                        ic_list.append(modcol.ic_object(child))
                else:
                    msg = "The selected object is not a group."
        if msg:
            modcol.inkex.errormsg(msg)
        return ic_list

    def base_colors(self, mode):
        """Get list of base colors from selection or custom input."""
        ic_list = []
        source = getattr(self.options, '{}_base_source'.format(mode), None)
        if source is not None:
            if source.startswith('selection'):
                ic_list = self.base_colors_selected(source)
            elif source == 'custom':
                ic_list = self.base_colors_custom(source)
            elif source == 'named_colors':
                ic_list = self.base_colors_named(source)
        if self.options.multihue_verbose:
            colors_msg = repr(ic_list)[1:-1]
            modcol.inkex.debug(colors_msg)
        return ic_list

    def base_color_custom(self, mode):
        """Get custom color from input."""
        msg = ""
        custom = getattr(self.options, '{}_base_color'.format(mode), None)
        ic_color = ic.InkColor()
        ic_color.from_long(custom)
        if msg:
            modcol.inkex.errormsg(msg)
        return ic_color

    def base_color_selected(self, source):
        """Get selected object."""
        msg = ""
        ic_color = None
        if source.endswith('obj'):
            if len(self.selected) == 1:
                node = self.selected[self.options.ids[0]]
                if not modcol.is_group(node):
                    ic_color = modcol.ic_object(node)
                else:
                    msg = "Please select an object, not a group."
            else:
                msg = "This mode requires a single selected object."
        elif source.endswith('group'):
            msg = "Unsupported selection mode: 'group'"
        if msg:
            modcol.inkex.errormsg(msg)
        return ic_color

    def base_color(self, mode):
        """Get single base color from selection or custom input."""
        ic_color = None
        source = getattr(self.options, '{}_base_source'.format(mode), None)
        if source is not None:
            if source.startswith('selection'):
                ic_color = self.base_color_selected(source)
            elif source == 'custom':
                ic_color = self.base_color_custom(mode)
        return ic_color

    # ----- generate color lists for palette

    def multihue_interp(self, ic_list, intervals):
        """Interpolate multiple colors from selection or custom input."""
        clist = []
        interp_mode = self.options.multihue_interp_mode
        colorspace = self.options.multihue_color_space
        sweep = self.options.multihue_interp_sweep
        if len(ic_list):
            if interp_mode in self.interp_two:
                clist.extend(
                    multihue_interp_func_ic(
                        ic_list, intervals, colorspace, sweep, interp_mode))
            elif interp_mode == 'spline':
                pass
            elif interp_mode == 'bezier':
                clist.extend(
                    multihue_interp_bezier_ic(ic_list, intervals, colorspace))
        # return final list of colors (as RGBA hex values)
        return [c.rgba_hex() for c in clist]

    def monochromatic_colors(self, ic_color):
        """Return monochromatic list of colors based on shade mode."""
        sprop = self.options.shade_mode
        delta = self.options.shade_step
        clist = [ic_color.copy()]
        ic_attrib = getattr(ic.InkColor, sprop, None)
        if ic_attrib is not None:
            range_attrib_ic(ic_color, clist, ic_attrib, delta)
        else:
            range_comp_cie(ic_color, clist, sprop, delta)
        # return final list of colors (as RGBA hex values)
        return [c.rgba_hex() for c in clist]

    def colors_from_preset(self, ic_color, params, intervals):
        """Return list of colors based on parameters of scheme preset."""
        count, deltas = params
        turns = len(deltas)
        slist = [ic_color.copy()]
        clist = []
        interp_mode = self.options.scheme_interp_mode
        colorspace = self.options.scheme_color_space
        sweep = self.options.scheme_interp_sweep
        # create list with color scheme colors
        for i in range(count):
            ic_color.hsl_h_deg += deltas[i % turns]
            slist.append(ic_color.copy())
        # interpolate in-between if requested
        if intervals == 0:
            clist.extend(slist)
        else:
            if interp_mode in self.interp_two:
                clist.extend(
                    multihue_interp_func_ic(
                        slist, intervals, colorspace, sweep, interp_mode))
            elif interp_mode == 'spline':
                pass
            elif interp_mode == 'bezier':
                clist.extend(
                    multihue_interp_bezier_ic(slist, intervals, colorspace))
        # return final list of colors (as RGBA hex values)
        return [c.rgba_hex() for c in clist]

    # get colors specific to feature (tab)

    def multihue_colors(self):
        """Create a list of RGBA colors based on selected colors."""
        colors = None
        ic_list = self.base_colors('multihue')
        if len(ic_list):
            intervals = self.options.multihue_interp_steps + 1
            colors = self.multihue_interp(ic_list, intervals)
        return colors

    def shade_colors(self):
        """Create a list of RGBA colors based on chosen shading options."""
        colors = None
        ic_color = self.base_color('shade')
        if ic_color is not None:
            colors = self.monochromatic_colors(ic_color)
        return colors

    def scheme_colors(self):
        """Create a list of RGBA colors based on chosen color scheme."""
        colors = None
        ic_color = self.base_color('scheme')
        if ic_color is not None:
            params = self.scheme_presets[self.options.scheme]
            intervals = self.options.scheme_interp_steps + 1
            colors = self.colors_from_preset(ic_color, params, intervals)
        return colors

    def get_colors(self):
        """Dispatcher to retrieve palette colors. tab-based."""
        if self.options.tab == '"schemes_tab"':
            return self.scheme_colors()
        elif self.options.tab == '"shades_tab"':
            return self.shade_colors()
        elif self.options.tab == '"multihue_tab"':
            return self.multihue_colors()

    # ----- main

    def color_schemes(self):
        """Create color scheme palette based on selected or custom color."""
        colors = self.get_colors()
        if colors is not None and len(colors):
            self.draw_palette(colors)

    def cleanup_doc(self):
        """ColorBase() hook for post-processing."""
        self.color_schemes()


if __name__ == '__main__':
    ME = ColorSchemes()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
