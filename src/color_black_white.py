#!/usr/bin/env python
"""
color_black_white - convert SVG paint colors to black-and-white

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colorops as co


class ColorBlackWhite(modcol.ColorRGBA):
    """ColorEffect-based class to convert colors to black-and-white."""

    def __init__(self):
        """Init base class and ColorBlackWhite()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--threshold",
                                     action="store", type="int",
                                     dest="threshold", default=50,
                                     help="Black-White threshold")
        self.OptionParser.add_option("--grey_mode",
                                     action="store", type="string",
                                     dest="grey_mode", default="luma_rec601",
                                     help="Greyscale mode")

    def black_white(self, ink_color):
        """Convert InkColor to B&W based on threshold."""

        # Parameters
        threshold = self.options.threshold / 100.0
        greymode = self.options.grey_mode

        # luma, luminance, or fallback greyscale
        if greymode.startswith('luma'):
            lum = co.luma(*ink_color.rgb_float(), mode=greymode[5:])
        elif greymode.startswith('luminance'):
            lum = co.luminance(*ink_color.rgb_linear())
        else:  # fallback to RGB "lightness"
            lum = sum(ink_color.rgb_float()) / 3.0

        # Compare luminance to threshold
        if lum is not None:
            result = 1.0 if lum > threshold else 0.0

        # Update ink_color
        ink_color.from_float(result, result, result)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"black_white_tab"':
            self.black_white(ink_color)


if __name__ == '__main__':
    ME = ColorBlackWhite()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
