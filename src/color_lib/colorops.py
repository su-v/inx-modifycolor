#!/usr/bin/env python
"""
color_lib.colorops      - module with functions for color operations

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math  # sqrt, pow

# local library
from color_lib import colorconst as cc
from color_lib.util import clamp
from color_lib import colorblend as cb
from color_lib import colorconvert as cv
from color_lib import colormatrix as cm


# ----- conversion functions

# A gamma value y < 1 is sometimes called an encoding gamma, and the
# process of encoding with this compressive power-law nonlinearity is
# called gamma compression; conversely a gamma value y > 1 is called a
# decoding gamma and the application of the expansive power-law
# nonlinearity is called gamma expansion.


# gamma > 1: decompress
def srgb_gamma_decompress(r, g, b):
    """WCAG2 gamma decompression for sRGB, returns linearRGB"""
    # Based on:
    # https://www.w3.org/TR/WCAG20/#relativeluminancedef
    a = 0.055
    return [(c / 12.92 if c <= 0.03928 else
             math.pow((c + a) / (1 + a), 2.4))
            for c in (r, g, b)]


# gamma < 1: compress
def linear_gamma_compress(r, g, b):
    """WCAG2 gamma compression for linearRGB, returns sRGB"""
    # Based on:
    # https://www.w3.org/TR/WCAG20/#relativeluminancedef
    a = 0.055
    return [(c * 12.92 if c <= 0.0030403 else
             ((1 + a) * math.pow(c, 1 / 2.4)) - a)
            for c in (r, g, b)]


def luma(r, g, b, mode='rec601'):
    """Return Luma L' based on gamma-compressed R', G', B' values."""
    if mode == 'rec601':
        return (cc.LUMA_REC601_R * r +
                cc.LUMA_REC601_G * g +
                cc.LUMA_REC601_B * b)
    elif mode == 'rec709':
        return (cc.LUMA_REC709_R * r +
                cc.LUMA_REC709_G * g +
                cc.LUMA_REC709_B * b)
    else:
        return None


def luminance(r, g, b):
    """Return relative luminance L based on gamma-uncompressed R, G, B."""
    return (cc.IPR * r +
            cc.IPG * g +
            cc.IPB * b)


def hsp_grey(r, g, b):
    """Convert RGB channels to greyscale based on HSP.

    ref: http://alienryderflex.com/hsp.html
    """
    return math.sqrt((0.299 * r**2) + (0.587 * g**2) + (0.144 * b**2))


# ----- Color mixing

def color_mix(color1, color2, percent=0.5, linear=False):
    """Mix two colors together in variable proportion.

    Opacity is included in the calculations.

    ref: http://lesscss.org/functions/#color-operations-mix
    """
    # copy InkColor object with current color
    new_color = color2.copy()
    # percent (0.0 to 1.0) to weight range (-1.0 to 1.0)
    weight = percent * 2.0 - 1
    # alpha difference (directed)
    delta_a = color1.alpha - color2.alpha
    # color_mix is not 'src over dest' default blending:
    if weight * delta_a == -1:
        # avoid division by zero
        pass
    else:
        weight = (weight + delta_a) / (1 + (weight * delta_a))
    # weight range (-1.0 to 1.0) to percent (0.0 to 1.0)
    percent1 = (weight + 1) / 2.0
    percent2 = 1 - percent1
    # mix alpha
    a = (color1.alpha * percent) + (color2.alpha * (1 - percent))
    # mix color in selected color space
    if linear:
        r, g, b = [c1 * percent1 + c2 * percent2
                   for c1, c2 in
                   zip(color1.rgb_linear(), color2.rgb_linear())]
        # update new InkColor object
        new_color.from_linear(r, g, b, a)
    else:
        r, g, b = [c1 * percent1 + c2 * percent2
                   for c1, c2 in
                   zip(color1.rgb_float(), color2.rgb_float())]
        # update new InkColor object
        new_color.from_float(r, g, b, a)
    return new_color


def color_tint(color, amount=0.1, linear=False):
    """Mix color with white in variable proportion."""
    white = color.copy()
    white.from_css('white')
    white.alpha = 1.0
    return color_mix(white, color, amount, linear)


def color_shade(color, amount=0.1, linear=False):
    """Mix color with black in variable proportion."""
    black = color.copy()
    black.from_css('black')
    black.alpha = 1.0
    return color_mix(black, color, amount, linear)


def color_tone(color, amount=0.1, linear=False):
    """Mix color with grey in variable proportion."""
    grey = color.copy()
    if linear:
        grey.from_linear(0.5, 0.5, 0.5, 1.0)
    else:
        grey.from_float(0.5, 0.5, 0.5, 1.0)
    return color_mix(grey, color, amount, linear)


# ----- Greyscale

def greyscale_rgb_lightness(color):
    """Convert to greyscale based on RGB lightness."""
    new_color = color.copy()
    r, g, b = new_color.rgb_float()
    grey = 0.5 * (max(r, g, b) + min(r, g, b))
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_rgb_average(color):
    """Convert to greyscale based on RGB average."""
    new_color = color.copy()
    r, g, b = new_color.rgb_float()
    grey = (r + g + b) / 3.0
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_rgb_decompose(color, mode='max'):
    """Convert to greyscale based on RGB decomposition."""
    new_color = color.copy()
    r, g, b = new_color.rgb_float()
    grey = 0  # fallback
    if mode == 'max':
        grey = max(r, g, b)
    elif mode == 'min':
        grey = min(r, g, b)
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_rgb_channel(color, channel='r'):
    """Convert to greyscale based on one RGB channel."""
    new_color = color.copy()
    grey = 0  # fallback
    if channel == 'r' or channel.lower() == 'red':
        grey = color.rgb_r
    elif channel == 'g' or channel.lower() == 'green':
        grey = color.rgb_g
    elif channel == 'b' or channel.lower() == 'blue':
        grey = color.rgb_g
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_rgb_posterize(color, count=2):
    """Convert to greyscale based on number of levels for RGB average."""
    new_color = color.copy()
    r, g, b = new_color.rgb_int()
    if count < 2:
        count = 2
    elif count > 256:
        count = 256
    factor = int(255 / (count - 1))
    average = (r + g + b) / 3.0
    grey = int(round(average / factor)) * factor
    new_color.from_int(grey, grey, grey)
    return new_color


def greyscale_hsl_lightness(color):
    """Convert to greyscale based on HSL lightness."""
    new_color = color.copy()
    new_color.hsl_s = 0.0
    return new_color


def greyscale_hsv_value(color):
    """Convert to greyscale based on HSV value."""
    new_color = color.copy()
    new_color.hsv_s = 0.0
    return new_color


def greyscale_lab_lightness(color):
    """Convert to greyscale based on CIELAB lightness."""
    new_color = color.copy()
    l = cv.rgb_to_lab(*color.rgb_float())[0]
    new_color.from_float(*cv.lab_to_rgb(l, 0, 0))
    return new_color


def greyscale_hsp_brightness(color):
    """Convert to greyscale based on HSP perceived brightness."""
    new_color = color.copy()
    grey = hsp_grey(*color.rgb_float())
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_blend_luminosity(color):
    """Convert to greyscale based on CSS blend mode 'Luminosity'."""
    backdrop = color.copy()
    backdrop.from_css('white')
    backdrop.alpha = 1.0
    source = color.copy()
    source.alpha = 1.0
    func = getattr(cb, 'css_blend_hsl_luminosity')
    new_color = cb.css_blend_hsl(backdrop, source, func, linear=False)
    new_color.alpha = color.alpha
    return new_color


def greyscale_luma(color, mode='rec601'):
    """Convert to greyscale based on Luma (Rec. 601, Rec. 709)."""
    new_color = color.copy()
    grey = luma(*color.rgb_float(), mode=mode)
    new_color.from_float(grey, grey, grey)
    return new_color


def greyscale_luminance(color, mode='linear'):
    """Convert to greyscale based on relative luminance (CIE 1931)."""
    new_color = color.copy()
    if mode == 'linear':
        grey = luminance(*color.rgb_linear())
        new_color.from_linear(grey, grey, grey)
    elif mode == 'wcag20':
        grey = luminance(*srgb_gamma_decompress(*color.rgb_float()))
        new_color.from_float(*linear_gamma_compress(grey, grey, grey))
    return new_color


def color_greyscale(color, mode="default"):
    """Return *color* as new greyscale color."""
    # pylint: disable=unused-argument
    # TODO: write dispatcher for supported greyscale options
    pass


# ----- Miscellaneous

def color_extract(color, channel, greyscale=False):
    """Extract *channel* color component, optionally convert to greyscale."""
    # copy InkColor object with current color
    new_color = color.copy()
    # map channel to SVG color name
    channel_map = {
        'red': 'red',
        'green': 'lime',
        'blue': 'blue',
        'cyan': 'cyan',
        'magenta': 'fuchsia',
        'yellow': 'yellow',
    }
    # extract value for channel
    coverage = 0
    if channel == 'red':
        coverage = color.rgb_r
    elif channel == 'green':
        coverage = color.rgb_g
    elif channel == 'blue':
        coverage = color.rgb_b
    elif channel == 'cyan':
        coverage = 1 - color.rgb_r
    elif channel == 'magenta':
        coverage = 1 - color.rgb_g
    elif channel == 'yellow':
        coverage = 1 - color.rgb_b
    # set color for new_color
    if greyscale:
        new_color.from_css('black')
    else:
        new_color.from_css(channel_map[channel])
    # set alpha to channel coverage
    new_color.rgb_a = coverage
    return new_color


# ----- feComponentTransfer functions for CSS3 filter functions

def comp_transfer_identity(comp):
    """Map identity transfer function to RGBA component *comp*."""
    return comp


def comp_transfer_table(comp, values=None):
    """Map table transfer function to RGBA component *comp*."""
    if values is None or len(values) < 1:
        # undefined or empty list of table values
        return comp_transfer_identity(comp)
    else:
        # number of interpolation regions
        n = len(values) - 1
        if comp == 1:
            # last value
            return values[-1]
        else:
            # TODO: verify k/n <= C < (k+1)/n
            k = int(n * comp)
            if comp < k / float(n):
                k -= 1
            elif comp >= (k + 1) / float(n):
                k += 1
            return values[k] + ((comp - k/float(n))*n *
                                (values[k+1] - values[k]))


def comp_transfer_discrete(comp, values=None):
    """Map discrete transfer function to RGBA component *comp*."""
    # TODO: verify output
    if values is None or len(values) < 1:
        # undefined or empty list of table values
        return comp_transfer_identity(comp)
    else:
        # number of discrete values
        n = len(values)
        if comp == 1:
            # last value
            return values[-1]
        else:
            # TODO: verify k/n <= C < (k+1)/n
            k = int(n * comp)
            if comp < k / float(n):
                k -= 1
            elif comp >= (k + 1) / float(n):
                k += 1
            return values[k]


def comp_transfer_linear(comp, slope=1.0, intercept=0.0):
    """Map linear transfer function to RGBA component *comp*."""
    # substitute fallback values
    slope = 1.0 if slope is None else slope
    intercept = 0.0 if intercept is None else intercept
    # apply function
    return (slope * comp) + intercept


def comp_transfer_gamma(comp, amplitude=1.0, exponent=1.0, offset=0.0):
    """Map gamma transfer function to RGBA component *comp*."""
    # substitute fallback values
    amplitude = 1.0 if amplitude is None else amplitude
    exponent = 1.0 if exponent is None else exponent
    offset = 0.0 if offset is None else offset
    # apply function
    return (amplitude * math.pow(comp, exponent)) + offset


# ----- CSS3 filter functions
#       ref: https://drafts.fxtf.org/filters/#ShorthandEquivalents

def rgba_transfer_linear(rgba, slopes, intercepts):
    """Map linear transfer for feComponentTransfer to RGBA list."""
    # return [(c * slope) + intercept for c in color.rgb_float()]
    out = 4 * [0]
    for i, comp in enumerate(rgba):
        if slopes[i] is None and intercepts[i] is None:
            out[i] = comp_transfer_identity(comp)
        else:
            out[i] = comp_transfer_linear(comp, slopes[i], intercepts[i])
    return out


def rgba_transfer_table(rgba, values):
    """Map table ranges for feComponentTransfer to RGBA list."""
    out = 4 * [0]
    for i, comp in enumerate(rgba):
        if values[i] is None:
            out[i] = comp_transfer_identity(comp)
        else:
            out[i] = comp_transfer_table(comp, values[i])
    return out


def rgba_transfer_discrete(rgba, values):
    """Map discrete ranges for feComponentTransfer to RGBA list."""
    out = 4 * [0]
    for i, comp in enumerate(rgba):
        if values[i] is None:
            out[i] = comp_transfer_identity(comp)
        else:
            out[i] = comp_transfer_discrete(comp, values[i])
    return out


def css_grayscale(color, val=0.0):
    """Convert to grayscale."""

    # Values over 100% are allowed
    # but UAs must clamp the values to 1.
    # Negative values are not allowed
    val = clamp(val, 0.0, 1.0)

    # define empty matrix
    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0
    # fill matrix
    mat[0][0] = 0.2126 + 0.7874 * (1 - val)
    mat[0][1] = 0.7152 - 0.7152 * (1 - val)
    mat[0][2] = 0.0722 - 0.0722 * (1 - val)
    mat[1][0] = 0.2126 - 0.2126 * (1 - val)
    mat[1][1] = 0.7152 + 0.2848 * (1 - val)
    mat[1][2] = 0.0722 - 0.0722 * (1 - val)
    mat[2][0] = 0.2126 - 0.2126 * (1 - val)
    mat[2][1] = 0.7152 - 0.7152 * (1 - val)
    mat[2][2] = 0.0722 + 0.9278 * (1 - val)
    # matrix multiplication and conversion to RGB triplet
    rgb = cm.matrix2rgb(cm.matrixmult(mat, cm.rgb2matrix(*color.rgb_float())))
    # update color and return
    new_color = color.copy()
    new_color.from_float(*rgb)
    return new_color


def css_sepia(color, val=0.0):
    """Convert to sepia."""

    # Values over 100% are allowed
    # but UAs must clamp the values to 1.
    # Negative values are not allowed
    val = clamp(val, 0.0, 1.0)

    # define empty matrix
    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0
    # fill matrix
    mat[0][0] = 0.393 + 0.607 * (1 - val)
    mat[0][1] = 0.769 - 0.769 * (1 - val)
    mat[0][2] = 0.189 - 0.189 * (1 - val)
    mat[1][0] = 0.349 - 0.349 * (1 - val)
    mat[1][1] = 0.686 + 0.314 * (1 - val)
    mat[1][2] = 0.168 - 0.168 * (1 - val)
    mat[2][0] = 0.272 - 0.272 * (1 - val)
    mat[2][1] = 0.534 - 0.534 * (1 - val)
    mat[2][2] = 0.131 + 0.869 * (1 - val)
    # matrix multiplication and conversion to RGB triplet
    rgb = cm.matrix2rgb(cm.matrixmult(mat, cm.rgb2matrix(*color.rgb_float())))
    # update color and return
    new_color = color.copy()
    new_color.from_float(*rgb)
    return new_color


def css_saturate(color, val=1.0):
    """Change saturation of color (val = 1.0: no change).

    <feColorMatrix type="saturate" values="(1 - [val])"/>

    """

    # Values over 100% are allowed
    # Negative values are not allowed
    val = 1.0 if val < 0 else val

    # colormatrix attributes
    type_ = 'saturate'
    values = 1.0 - val

    new_color = color.copy()
    func = getattr(cm, type_, None)
    if func is not None:
        r, g, b = color.rgb_float()
        new_color.from_float(*func(r, g, b, values))
    return new_color


def css_huerotate(color, val=0.0):
    """Apply hue rotation to color (angle in degrees).

    <feColorMatrix type="hueRotate" values="[angle]"/>

    """

    # Implementations must not normalize this value
    # in order to allow animations beyond 360deg.
    val = val

    # colormatrix attributes
    type_ = 'hue_rotate'
    values = val

    new_color = color.copy()
    func = getattr(cm, type_, None)
    if func is not None:
        r, g, b = color.rgb_float()
        new_color.from_float(*func(r, g, b, values))
    return new_color


def css_invert(color, val=0.0):
    """Invert color by val (val = 0.0: no change).

    <feComponentTransfer>
      <feFuncR type="table" tableValues="[val] (1 - [val])"/>
      <feFuncG type="table" tableValues="[val] (1 - [val])"/>
      <feFuncB type="table" tableValues="[val] (1 - [val])"/>
    </feComponentTransfer>

    """

    # Values over 100% are allowed
    # but UAs must clamp the values to 1.
    # Negative values are not allowed
    val = clamp(val, 0.0, 1.0)

    # transfer function element attributes
    values = 4 * [None]
    for i in range(3):
        values[i] = [val, 1 - val]

    new_color = color.copy()
    new_color.from_float(
        *rgba_transfer_table(color.rgba_float(), values))
    return new_color


def css_opacity(color, val=1.0):
    """Apply transparency to color.

    <feComponentTransfer>
      <feFuncA type="table" tableValues="0 [val]"/>
    </feComponentTransfer>

    """

    # Values over 100% are allowed
    # but UAs must clamp the values to 1.
    # Negative values are not allowed
    val = clamp(val, 0.0, 1.0)

    # transfer function element attributes
    values = 4 * [None]
    values[3] = [0, val]

    new_color = color.copy()
    new_color.from_float(
        *rgba_transfer_table(color.rgba_float(), values))
    return new_color


def css_brightness(color, val=1.0):
    """Apply a linear multiplier to color (val = 1.0: no change).

    <feComponentTransfer>
      <feFuncR type="linear" slope="[val]"/>
      <feFuncG type="linear" slope="[val]"/>
      <feFuncB type="linear" slope="[val]"/>
    </feComponentTransfer>

    """

    # Values over 100% are allowed
    # Negative values are not allowed
    val = 1.0 if val < 0 else val

    # transfer function element attributes
    slopes = 4 * [None]
    intercepts = 4 * [None]
    for i in range(3):
        slopes[i] = val
        intercepts[i] = 0

    new_color = color.copy()
    new_color.from_float(
        *rgba_transfer_linear(color.rgba_float(), slopes, intercepts))
    return new_color


def css_contrast(color, val=1.0):
    """Adjust the contrast of the color (val = 1.0: no change).

    <feComponentTransfer>
      <feFuncR type="linear" slope="[val]" intercept="-(0.5 * [val]) + 0.5"/>
      <feFuncG type="linear" slope="[val]" intercept="-(0.5 * [val]) + 0.5"/>
      <feFuncB type="linear" slope="[val]" intercept="-(0.5 * [val]) + 0.5"/>
    </feComponentTransfer>

    """

    # Values over 100% are allowed
    # Negative values are not allowed
    val = 1.0 if val < 0 else val

    # transfer function element attributes
    slopes = 4 * [None]
    intercepts = 4 * [None]
    for i in range(3):
        slopes[i] = val
        intercepts[i] = -(0.5 * val) + 0.5

    new_color = color.copy()
    new_color.from_float(
        *rgba_transfer_linear(color.rgba_float(), slopes, intercepts))
    return new_color


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
