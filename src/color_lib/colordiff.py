#!/usr/bin/env python
"""
color_lib.colordiff     - module with functions for color differences

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math  # pow, sqrt

# local library
from color_lib import colorconvert as cv


def delta_e_cie1994(color1, color2):
    """Calculates the Delta E (CIE1994) of two CIELAB colors."""
    # pylint: disable=invalid-name
    # pylint: disable=too-many-locals

    # Based on:
    # http://stackoverflow.com/a/25726414
    # http://www.easyrgb.com/index.php?X=DELT&H=04#text4

    l1, a1, b1 = color1             # Color #1 CIE-L*ab values
    l2, a2, b2 = color2             # Color #2 CIE-L*ab values
    wht_l = wht_c = wht_h = 1.0     # Weighting factors (default)

    x_c1 = math.sqrt(a1**2 + b1**2)
    x_c2 = math.sqrt(a2**2 + b2**2)

    x_dl = l2 - l1
    x_dc = x_c2 - x_c1
    x_de = math.sqrt((l1 - l2)**2 + (a1 - a2)**2 + (b1 - b2)**2)

    if math.sqrt(x_de) > math.sqrt(abs(x_dl)) + math.sqrt(abs(x_dc)):
        x_dh = math.sqrt(x_de**2 - x_dl**2 - x_dc**2)
    else:
        x_dh = 0

    x_sc = 1.0 + (0.045 * x_c1)
    x_sh = 1.0 + (0.015 * x_c1)

    x_dl = x_dl / wht_l
    x_dc = x_dc / (wht_c * x_sc)
    x_dh = x_dh / (wht_h * x_sh)

    return math.sqrt(x_dl**2 + x_dc**2 + x_dh**2)   # delta_e94


def color_diff(color1, color2):
    """Compare two InkColor objects in CIELAB, return Delta E (CIE1994)."""
    lab1 = cv.xyz_to_lab(*cv.rgb_to_xyz(*color1.rgb_float()))
    lab2 = cv.xyz_to_lab(*cv.rgb_to_xyz(*color2.rgb_float()))
    return delta_e_cie1994(lab1, lab2)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
