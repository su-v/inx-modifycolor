#!/usr/bin/env python
"""
color_lib.colormatrix   - module with matrix helper functions
                          hue_rotate, saturate from feColorMatrix

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
from math import sin, cos, pi

# local library
from color_lib import colorconst as cc


def clamp(minimum, val, maximum):
    """Clamp *val* to min/max values."""
    return max(minimum, min(val, maximum))


def values_to_matrix(string):
    """Convert feColorMatrix values *string* to matrix.

    General matrix (SVG 1.1, feColorMatrix):

        | R' |     | a00 a01 a02 a03 a04 |   | R |
        | G' |     | a10 a11 a12 a13 a14 |   | G |
        | B' |  =  | a20 a21 a22 a23 a24 | * | B |
        | A' |     | a30 a31 a32 a33 a34 |   | A |
        | 1  |     |  0   0   0   0   1  |   | 1 |

    For type="matrix", 'values' is a list of 20 matrix values
    (a00 a01 a02 a03 a04 a10 a11 ... a34)

    With color_lib.colormatrix:

        mat[0][0] = a00
        mat[0][1] = a01
        mat[0][2] = a02
        ...
        mat[1][0] = a10
        mat[1][1] = a11
        mat[1][2] = a12
        ...
        mat[2][0] = a20
        mat[2][1] = a21
        mat[2][2] = a22
        ...

    """
    # split stripped string, convert values to float
    mlist = [float(i) for i in string.strip().split(' ')]
    # create empty matrix
    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0
    # fill matrix
    if len(mlist) == 20:
        for i in range(4):
            mat[i] = mlist[i*5:(i+1)*5]
    return mat


def matrix_to_values(mat):
    """Convert matrix to values string for feColorMatrix."""
    values = []
    if len(mat) >= 4:
        for i in range(4):
            if len(mat[i]) == 5:
                values.extend(mat[i])
    if len(values) == 20:
        return ' '.join(str(v) for v in values).strip()


def list_to_matrix(a, b, c, d=1.0):
    """Convert values *a*, *b*, *c*, *d* to matrix."""
    mat = [[0.0 for _ in range(1)] for _ in range(5)]
    for i in range(3):
        mat[i][0] = (a, b, c)[i]
    for i in (3, 4):
        mat[i][0] = d
    return mat


def matrix_to_list(mat):
    """Return a, b, c, d values from matrix *mat*."""
    return [clamp(0.0, mat[i][0], 1.0) for i in range(4)]


def rgba2matrix(r, g, b, a=1.0):
    """Represent RGBA color (*r*, *g*, *b*, *a* as floats) as matrix."""
    return list_to_matrix(r, g, b, a)


def matrix2rgba(mat):
    """Return r, g, b, a values as floats from RGBA matrix *mat*."""
    return matrix_to_list(mat)


def rgb2matrix(r, g, b):
    """Represent RGB color (*r*, *g*, *b* as floats) as matrix."""
    return rgba2matrix(r, g, b, 1.0)


def matrix2rgb(mat):
    """Return r, g, b values as floats from RGBA matrix *mat*."""
    return matrix2rgba(mat)[:-1]


def matrixmult(mat_a, mat_b):
    """
    Multiply matrix A * B
    based on <http://stackoverflow.com/a/10508133>
    """
    rows_a = len(mat_a)
    cols_a = len(mat_a[0])
    rows_b = len(mat_b)
    cols_b = len(mat_b[0])

    if cols_a != rows_b:
        return None  # FIXME: fail silently or assert?

    # Create the result matrix (dimensions would be rows_a x cols_b)
    mat_c = [[0.0 for _ in range(cols_b)] for _ in range(rows_a)]
    for i in range(rows_a):
        for j in range(cols_b):
            for k in range(cols_a):
                mat_c[i][j] += mat_a[i][k]*mat_b[k][j]
    return mat_c


def hue_rotate(r, g, b, angle):
    """
    Hue Rotate: hue is shifted by specifying one number (*angle* in degrees)
    Only RGB values are changed.

    Arguments *r*, *g*, *b* are floats in range 0.0-1.0
    """
    # pylint: disable=too-many-locals

    # hue rotate (lightness: 180 degrees)
    hue = angle * pi/180.0

    # feColorMatrix: Rotate Hue
    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    a00 = 0.213 + cos(hue)*(0.787) + sin(hue)*(-0.213)
    a10 = 0.213 + cos(hue)*(-0.213) + sin(hue)*(0.143)
    a20 = 0.213 + cos(hue)*(-0.213) + sin(hue)*(-0.787)

    a01 = 0.715 + cos(hue)*(-0.715) + sin(hue)*(-0.715)
    a11 = 0.715 + cos(hue)*(0.285) + sin(hue)*(0.140)
    a21 = 0.715 + cos(hue)*(-0.715) + sin(hue)*(0.715)

    a02 = 0.072 + cos(hue)*(-0.072) + sin(hue)*(0.928)
    a12 = 0.072 + cos(hue)*(-0.072) + sin(hue)*(-0.283)
    a22 = 0.072 + cos(hue)*(0.928) + sin(hue)*(0.072)

    mat[0][0] = a00
    mat[0][1] = a01
    mat[0][2] = a02
    mat[1][0] = a10
    mat[1][1] = a11
    mat[1][2] = a12
    mat[2][0] = a20
    mat[2][1] = a21
    mat[2][2] = a22

    # multiply the two matrices
    mat_rgb_new = matrixmult(mat, rgb2matrix(r, g, b))

    # return RGB
    return matrix2rgb(mat_rgb_new)


def saturate(r, g, b, val):
    """
    Reduce saturation by specifying one number, s (*val*).
    The range of s is 0.0 (completely desaturated) to 1.0 (unchanged).
    Only the RGB values are changed.

    Arguments *r*, *g*, *b* are floats in range 0.0-1.0

    feColorMatrix: Saturate

       | R' |     |0.213+0.787s  0.715-0.715s  0.072-0.072s 0  0 |   | R |
       | G' |     |0.213-0.213s  0.715+0.285s  0.072-0.072s 0  0 |   | G |
       | B' |  =  |0.213-0.213s  0.715-0.715s  0.072+0.928s 0  0 | * | B |
       | A' |     |           0            0             0  1  0 |   | A |
       | 1  |     |           0            0             0  0  1 |   | 1 |

    """
    # pylint: disable=too-many-locals

    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    a00 = cc.IPR + val*(1-cc.IPR)
    a10 = cc.IPR + val*(-cc.IPR)
    a20 = cc.IPR + val*(-cc.IPR)

    a01 = cc.IPG + val*(-cc.IPG)
    a11 = cc.IPG + val*(1-cc.IPG)
    a21 = cc.IPG + val*(-cc.IPG)

    a02 = cc.IPB + val*(-cc.IPB)
    a12 = cc.IPB + val*(-cc.IPB)
    a22 = cc.IPB + val*(1-cc.IPB)

    mat[0][0] = a00
    mat[0][1] = a01
    mat[0][2] = a02
    mat[1][0] = a10
    mat[1][1] = a11
    mat[1][2] = a12
    mat[2][0] = a20
    mat[2][1] = a21
    mat[2][2] = a22

    # multiply the two matrices
    mat_rgb_new = matrixmult(mat, rgb2matrix(r, g, b))

    # return RGB
    return matrix2rgb(mat_rgb_new)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
