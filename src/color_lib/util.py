#!/usr/bin/env python
"""
color_lib.util          - module with common utility functions
                          for color operations

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import random
import math  # pow


def cbrt(val):
    """Cubic root."""
    if val >= 0:
        return math.pow(val, 1 / 3.0)
    return -math.pow(-val, 1 / 3.0)


def clamp(val, val_min, val_max):
    """Clamp value to min/max values."""
    return max(val_min, min(val, val_max))


def roundup(val, mult):
    """Round up *val* to next multiple of *mult*."""
    return val if val % mult == 0 else val + mult - val % mult


def index_min(values):
    """Return index of lowest value among items in list *values*."""
    return min(range(len(values)), key=values.__getitem__)


def index_max(values):
    """Return index of highest value among items in list *values*."""
    return max(range(len(values)), key=values.__getitem__)


def min_mid_max(alist):
    """Return indices to min, mid, max values in list of 3 items."""
    if len(alist) == 3:
        # TODO: verify, optimize
        if alist[0] == alist[1] and alist[1] == alist[2]:
            # special case: all values are equal
            v_min, v_mid, v_max = [0, 1, 2]
        else:
            # lowest color value with lowest index
            v_min = index_min(alist)
            # highest color value with highest index (max in reversed list)
            alist.reverse()
            v_max = (len(alist) - 1) - index_max(alist)
            alist.reverse()
            # c_mid is the remaining one
            tlist = [0, 1, 2]
            tlist.remove(v_min)
            tlist.remove(v_max)
            v_mid = tlist[0]
        return v_min, v_mid, v_max


def min_mid_max_steps(alist):
    """Expliticly compare all 3 items in list to return sorted indices."""
    if len(alist) == 3:
        if alist[0] > alist[1]:
            if alist[0] > alist[2]:
                v_max = 0
                if alist[1] > alist[2]:
                    v_mid = 1
                    v_min = 2
                else:
                    v_mid = 2
                    v_min = 1
            else:
                v_max = 2
                v_mid = 0
                v_min = 1
        else:
            if alist[0] > alist[2]:
                v_max = 1
                v_mid = 0
                v_min = 2
            else:
                v_min = 0
                if alist[1] > alist[2]:
                    v_max = 1
                    v_mid = 2
                else:
                    v_max = 2
                    v_mid = 1
        return v_min, v_mid, v_max


def randomize_int(limit, val):
    """Return random value within integer-based limited range."""
    limit = 255.0 * limit / 100.0
    limit /= 2
    r_max = int((val * 255.0) + limit)
    r_min = int((val * 255.0) - limit)
    if r_max > 255:
        r_min = r_min - (r_max - 255)
        r_max = 255
    if r_min < 0:
        r_max = r_max - r_min
        r_min = 0
    return random.randrange(r_min, r_max) / 255.0


def randomize_float(limit, val):
    """Return random value within float-based limited range."""
    limit /= 2
    r_max = (val * 100) + limit
    r_min = (val * 100) - limit
    if r_max > 100:
        r_min = r_min - (r_max - 100)
        r_max = 100
    if r_min < 0:
        r_max = r_max - r_min
        r_min = 0
    return random.uniform(r_min, r_max) / 100


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
