#!/usr/bin/env python
"""
color_lib.common - module to aid modifying colors in SVG files

This module is based on Inkscape's coloreffect.py

Copyright (C) 2006 Jos Hirth, kaioa.com
Copyright (C) 2007 Aaron C. Spike
Copyright (C) 2009 Monash University
Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import copy
import random
from collections import Counter

# local library
import simplestyle
import inkex
from color_lib import inkcolor as ic


# Constants
inkex.NSS[u'osb'] = u'http://www.openswatchbook.org/uri/2009/osb'

BASE_ID = 'modcol'
IC_PROPS = {
    'fill': 'fill-opacity',
    'stroke': 'stroke-opacity',
    'stop-color': 'stop-opacity',
    'flood-color': 'flood-opacity',
    'lighting-color': None,
    'solid-color': 'solid-opacity',
    'color': None,
    'filter': None,
}


def random_id(old_id):
    """Return randomized new id based on *old_id*."""
    return '%s-%d' % (old_id, int(random.random() * 1000))


def skip(node):
    """Check whether *node* is to be skipped for direct processing."""
    skip_elements = [
        inkex.addNS('defs', 'svg'),
        inkex.addNS('title', 'svg'),
        inkex.addNS('desc', 'svg'),
        inkex.addNS('metadata', 'svg'),
        inkex.addNS('namedview', 'sodipodi'),
        inkex.addNS('linearGradient', 'svg'),
        inkex.addNS('radialGradient', 'svg'),
        inkex.addNS('meshGradient', 'svg'),  # SVG2 legacy
        inkex.addNS('meshgradient', 'svg'),  # SVG2
        inkex.addNS('mesh', 'svg'),  # SVG2
        inkex.addNS('solidcolor', 'svg'),  # SVG2
        inkex.addNS('pattern', 'svg'),
        inkex.addNS('hatch', 'svg'),  # SVG2
        inkex.addNS('image', 'svg'),
        inkex.etree.Comment,
    ]
    return node.tag in skip_elements


def skip_custom(node):
    """Check whether *node* has speficic 'id' attribute."""
    return node.get('id', "").startswith(BASE_ID)


def is_group(node):
    """Check node for group tag."""
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    """Check node for group and groupmode 'layer'."""
    return (is_group(node) and
            node.get(inkex.addNS('groupmode', 'inkscape')) == 'layer')


def layers(document, rev=False):
    """Iterator for top-level layer groups."""
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            label = node.get(inkex.addNS('label', 'inkscape'), None)
            yield (label, node)


def create_group():
    """Return a new SVG group element."""
    return inkex.etree.Element(inkex.addNS('g', 'svg'))


def create_layer(label):
    """Return a new Inkscape layer group."""
    layer = create_group()
    layer.set(inkex.addNS('label', 'inkscape'), label)
    layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
    return layer


def get_defs(node):
    """Find <defs> in children of *node*, return first one found."""
    path = '/svg:svg//svg:defs'
    try:
        return node.xpath(path, namespaces=inkex.NSS)[0]
    except IndexError:
        return inkex.etree.SubElement(node, inkex.addNS('defs', 'svg'))


def is_osb_paint(node, kind=''):
    """Check node for osb paint."""
    osb_attr = inkex.addNS('paint', 'osb')
    if node.tag == inkex.addNS('linearGradient', 'svg'):
        if not kind:
            return osb_attr in node.attrib
        else:
            if osb_attr in node.attrib and node.attrib[osb_attr] == kind:
                if kind == 'solid':
                    return len(node) == 1
                elif kind == 'gradient':
                    return len(node) > 1
    return False


def is_solid_color(node):
    """Check node for <solidcolor> tag."""
    return node.tag == inkex.addNS('solidcolor', 'svg')


def is_named_color(node):
    """Check whether node is named color definition."""
    return is_osb_paint(node) or is_solid_color(node)


def ic_object(node, paint='fill'):
    """Return RGBA as InkColor object from element *node*."""
    # try style attribute
    if 'style' in node.attrib:
        sdict = ic.string_to_dict(node.get('style'))
        if paint in sdict:
            if ic.is_color(sdict[paint]):
                color = ic.InkColor()
                color.from_style_dict(sdict, prop=paint)
                return color
    # paint not in style: try presentation attribute
    if paint in node.attrib:
        if ic.is_color(sdict[paint]):
            color = ic.InkColor()
            color.from_style_dict(node.attrib, prop=paint)
            return color


def hexcolor(node, paint='fill'):
    """Return RGBA as hex color from object *node*."""
    # try style attribute
    if 'style' in node.attrib:
        sdict = ic.string_to_dict(node.get('style'))
        if paint in sdict:
            color = ic.InkColor()
            color.from_style_dict(sdict, prop=paint)
            return color.rgba_hex()
    # paint not in style: try presentation attribute
    if paint in node.attrib:
        color = ic.InkColor()
        color.from_style_dict(dict(node.attrib), prop=paint)
        return color.rgba_hex()


# ----- Common class

class ColorCommon(inkex.Effect):
    """Common class for color effects in Inkscape."""

    def __init__(self):
        """Init parent class."""
        inkex.Effect.__init__(self)

    def special_group(self, label='Palette', wrap_group=True):
        """Prepare special structure (layer, optionally group)."""
        layer = None
        for node_label, node in layers(self.document):
            if node_label == label:
                layer = node
                break
        if layer is None:
            layer = create_layer(label)
            layer_id = "".join(label.split()).lower()
            layer.set('id', self.uniqueId(layer_id))
            self.document.getroot().append(layer)
        if wrap_group:
            group = create_group()
            group.set('id', self.uniqueId(BASE_ID))
            layer.append(group)
            return group
        else:
            return layer


# ----- Base sub-classes

class ColorBase(ColorCommon):
    """Base class for color effects in Inkscape."""

    def __init__(self):
        """Init parent class."""
        ColorCommon.__init__(self)

        # instance attributes
        self.old_colors = []
        self.new_colors = []

        # general options, available in all sub-classes
        self.OptionParser.add_option("--scope",
                                     action="store",
                                     type="string",
                                     dest="scope",
                                     default="default",
                                     help="Processing scope")
        self.OptionParser.add_option("--track",
                                     action="store",
                                     type="inkbool",
                                     dest="track",
                                     default=False,
                                     help="Track processed colors")
        self.OptionParser.add_option("--track_only",
                                     action="store",
                                     type="inkbool",
                                     dest="track_only",
                                     default=False,
                                     help="Only track processed colors")
        self.OptionParser.add_option("--stats",
                                     action="store",
                                     type="inkbool",
                                     dest="stats",
                                     default=False,
                                     help="Show color stats")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store",
                                     type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    # override in sub-classes

    def custom_scope(self, props_dict):
        """Override to adjust IC_PROPS *props_dict* for custom scope."""
        pass

    def prepare_doc(self):
        """Override to pre-process document or selection."""
        pass

    def process_allcolors(self):
        """Override to access lists of all processed colors."""
        pass

    def cleanup_doc(self):
        """Override to post-process document or selection."""
        pass

    # internal

    def _process_style(self, node):
        """Process style of *node*."""
        pass

    def _get_attribs(self, node):
        """Recursively process style on *node* and its children."""
        # NOTE: paint-layer sources specified as 'child', 'child(n)' in
        # SVG2 are not supported (paint definitions as children of
        # current node are skipped).
        changed = False
        if skip(node):
            # If nothing is selected: make sure to not directly process
            # definitions of (shared) resources.  Otherwise referenced
            # resources are processed twice and operations like 'Invert'
            # may produce incorrect results.
            # TODO: find better solution to identify "changeable" nodes
            pass
        elif skip_custom(node):
            # A rather fragile exclusion of special nodes based on id
            # (or alternatively inkscape:label).
            # TODO: find better solution to identify "excluded" nodes
            pass
        else:
            changed = self._process_style(node)
            for child in node:
                child_changed = self._get_attribs(child)
                changed = changed or child_changed
        return changed

    def effect(self):
        """Main effect."""

        # configure scope for color modifications
        if self.options.scope == 'default':
            pass
        elif self.options.scope == 'fill_only':
            IC_PROPS.pop('stroke', None)
            IC_PROPS.pop('color', None)
            IC_PROPS.pop('filter', None)
        elif self.options.scope == 'stroke_only':
            IC_PROPS.pop('fill', None)
            IC_PROPS.pop('color', None)
            IC_PROPS.pop('filter', None)
        elif self.options.scope == 'filter_only':
            IC_PROPS.pop('fill', None)
            IC_PROPS.pop('stroke', None)
            IC_PROPS.pop('stop-color', None)
            IC_PROPS.pop('solid-color', None)
            IC_PROPS.pop('color', None)
        elif self.options.scope == 'filter_flood_only':
            IC_PROPS.pop('fill', None)
            IC_PROPS.pop('stroke', None)
            IC_PROPS.pop('stop-color', None)
            IC_PROPS.pop('solid-color', None)
            IC_PROPS.pop('lighting-color', None)
            IC_PROPS.pop('color', None)
        elif self.options.scope == 'filter_light_only':
            IC_PROPS.pop('fill', None)
            IC_PROPS.pop('stroke', None)
            IC_PROPS.pop('stop-color', None)
            IC_PROPS.pop('solid-color', None)
            IC_PROPS.pop('flood-color', None)
            IC_PROPS.pop('color', None)
        else:
            self.custom_scope(IC_PROPS)

        # pre-process document or seletion
        self.prepare_doc()

        # configure tracking of processed colors
        if self.options.stats or self.options.track_only:
            self.options.track = True

        # recursively process selection or document
        if len(self.selected):
            for node in self.selected.values():
                self._get_attribs(node)
        else:
            self._get_attribs(self.document.getroot())

        # post-process tracked colors
        if self.options.track:
            self.process_allcolors()

        # post-process document or selection
        self.cleanup_doc()


class ColorProps(ColorBase):
    """Base class for modifying style properties of a node."""

    def __init__(self):
        """Init parent class."""
        ColorBase.__init__(self)

    # override in sub-classes

    def check_props(self, sdict):
        """Override to check style properties before processing."""
        pass

    def modify_props(self, sdict, has_opacity):
        """Override in sub-class to modify style properties."""
        pass

    def update_props(self, sdict, flags):
        """Override to update style properties after processing."""
        pass

    # internal

    def _modify_props(self, sdict, flags, has_opacity):
        """Modify properties in style dict *props* based on *flags"."""
        if flags or not flags:  # flags not used here
            return self.modify_props(sdict, has_opacity)

    def _process_props(self, sdict, has_opacity=True):
        """Process properties in style dict *sdict*."""
        flags = self.check_props(sdict)
        changed = self._modify_props(sdict, flags, has_opacity)
        self.update_props(sdict, flags)
        return changed

    def _process_style(self, node):
        """Process style of *node*."""
        changed = False
        # Presentation attributes
        adict = dict(node.attrib)
        style = adict.pop('style', None)
        changed = self._process_props(adict, 'opacity' in adict or False)
        if not self.options.track_only:
            node.attrib.update(adict)
        else:
            changed = False
        # Inline CSS style properties
        if style is not None:
            sdict = simplestyle.parseStyle(style)
            style_changed = self._process_props(sdict)
            if not self.options.track_only:
                node.set('style', simplestyle.formatStyle(sdict))
            else:
                style_changed = False
            changed = changed or style_changed
        return changed


class ColorStats(ColorProps):
    """Base class for color stats in color-modifying extensions."""

    def __init__(self):
        """Init parent class."""
        ColorProps.__init__(self)

    # override in sub-classes

    def color_stats(self, mode='RGBA', count=None):
        """Display color stats (optionally overridden in subclasses)."""

        def show_counted(counter):
            """Iterate counter dict, format output based on count, mode."""
            for key, val in counter.most_common(count):
                color = ic.InkColorRGBAHex(key)
                if mode == 'RGBA':
                    showme('{}:\t{}'.format(val, color.rgba_int()))
                elif mode == 'RGB':
                    showme('{}:\t{}'.format(val, color.rgb_int()))

        showme = inkex.debug
        # total colors processed
        showme('Total colors processed: {}'.format(len(self.old_colors)))
        # create Counter dicts
        if mode == 'RGBA':
            counted_old = Counter(self.old_colors)
            if not self.options.track_only:
                counted_new = Counter(self.new_colors)
        elif mode == 'RGB':
            counted_old = Counter(c[:-2] for c in self.old_colors)
            if not self.options.track_only:
                counted_new = Counter(c[:-2] for c in self.new_colors)
        # total count of used colors
        showme('Unique {0} colors (original): {1}'.format(
            mode, len(counted_old)))
        if not self.options.track_only:
            showme('Unique {0} colors (new): {1}'.format(
                mode, len(counted_new)))
        # show unique colors sorted by count
        showme('\nCounted {0} colors (original):'.format(mode))
        show_counted(counted_old)
        if not self.options.track_only:
            showme('\nCounted {0} colors (new):'.format(mode))
            show_counted(counted_new)

    def process_allcolors(self):
        """Method optionally implemented in subclasses."""
        if self.options.stats:
            self.color_stats()


class ColorRGBA(ColorStats):
    """Base class for modifying RGBA colors in paints and paint servers."""

    def __init__(self):
        """Init parent class."""
        ColorStats.__init__(self)

        # instance attributes
        self.visited_nc = {}

        # options for modify_color()
        self.OptionParser.add_option("--fork_named_color",
                                     action="store",
                                     type="inkbool",
                                     dest="fork_named_color",
                                     default=True,
                                     help="Fork named colors")
        self.OptionParser.add_option("--paint_server",
                                     action="store",
                                     type="inkbool",
                                     dest="paint_server",
                                     default=True,
                                     help="Process paint servers")

    # override in sub-classes

    def modify_color(self, ink_color):
        """Method overridden in sub-classes to modify RGB(A) color."""
        pass

    # internal

    def _process_fork(self, node, old_id, new_id, fork):
        """Another duplicate sub-routine split into separate method."""
        changed = False
        visited = False
        if is_named_color(node):
            visited = True
            if not self.options.fork_named_color:
                fork = False
                new_id = old_id
        if old_id not in self.visited_nc or fork:
            changed = self._process_resource(node, new_id, fork)
        if visited:
            self.visited_nc[old_id] = self.visited_nc.get(old_id, 0) + 1
        return changed

    def _process_chained(self, new_node, fork):
        """Process chained resource (linked via xlink:href)."""
        changed = False
        xlink = inkex.addNS('href', 'xlink')
        href = new_node.get(xlink, '')
        if href.startswith('#'):
            old_id = href[1:]
            new_id = random_id(old_id) if fork else old_id
            if fork:
                new_node.set(xlink, '#{}'.format(new_id))
            linked_node = self.getElementById(old_id)
            if linked_node is not None:
                changed = self._process_fork(linked_node, old_id, new_id, fork)
            if fork and not changed:  # TODO: verfiy resetting link to old_id
                new_node.set(xlink, '#{}'.format(old_id))
        return changed if fork else False  # TODO: verify checking for fork

    def _process_resource(self, node, new_id, fork=True):
        """Recursively process style of resource, fork first if requested."""
        changed = False
        # 1. process resource itself
        if fork:
            new_node = copy.deepcopy(node)
            new_node.set('id', new_id)
            node.getparent().append(new_node)
        else:
            new_node = node
        changed = self._process_style(new_node)
        for child in new_node:
            child_changed = self._get_attribs(child)
            changed = changed or child_changed
        # 2. process chained (linked) resource
        chain_changed = self._process_chained(new_node, fork)
        changed = changed or chain_changed
        if fork and not changed:
            node.getparent().remove(new_node)
        return changed if fork else False

    def _process_link(self, val):
        """Prepare resource fork if required, process and return (new) id."""
        changed = False
        fork = False if self.options.track_only else True
        old_id = val[len('url(#'):val.find(')')]
        new_id = random_id(old_id) if fork else old_id
        linked_node = self.getElementById(old_id)
        if linked_node is not None:
            changed = self._process_fork(linked_node, old_id, new_id, fork)
        return 'url(#{})'.format(new_id if changed else old_id)

    def _process_rgba_dict(self, cdict):
        """Process and update *cdict* with modified color properties."""
        # Python 3 compat: iterate copy of dict, update original
        changed = False
        for key, val in dict(cdict).items():
            key_changed = False
            if key in IC_PROPS:
                if ic.is_color(val):
                    ink_color = ic.InkColor()
                    ink_color.from_style_dict(cdict, prop=key)
                    old_val = ink_color.rgba_hex()
                    if self.options.track:
                        self.old_colors.append(old_val)
                    # inkex.debug('ic before: \t{}'.format(repr(ink_color)))
                    self.modify_color(ink_color)
                    # inkex.debug('ic after:  \t{}'.format(repr(ink_color)))
                    new_val = ink_color.rgba_hex()
                    if self.options.track:
                        self.new_colors.append(new_val)
                    cdict.update(ink_color.rgba_to_style_dict(prop=key))
                    key_changed = old_val != new_val
                elif val.startswith('url(#'):
                    if (self.options.paint_server or
                            self.options.scope.startswith('filter')):
                        new_val = self._process_link(val)
                        cdict[key] = new_val
                        key_changed = val != new_val
            changed = changed or key_changed
        return changed

    def _process_rgba_props(self, sdict):
        """Process color properties in style dict *sdict*."""
        changed = False
        for key, val in IC_PROPS.items():
            key_changed = False
            cdict = {}
            if key in sdict:
                cdict[key] = sdict[key]
                if val in sdict:
                    cdict[val] = sdict[val]
            # if cdict: inkex.debug(cdict)
            key_changed = self._process_rgba_dict(cdict)
            # if cdict: inkex.debug(cdict)
            for key, val in cdict.items():
                if key in sdict:
                    sdict[key] = str(val)
                elif key in IC_PROPS.values():
                    # only add missing opacity prop if not default value
                    if val != 1:
                        sdict[key] = str(val)
            changed = changed or key_changed
        return changed

    def _modify_props(self, sdict, flags, has_opacity=True):
        """Modify properties in style dict *props* based on *flags"."""
        changed = False
        if not (flags and 'skip_color' in flags):
            changed = self._process_rgba_props(sdict)
        return changed


class ColorRGBAO(ColorRGBA):
    """Base class for modifying RGBA colors and/or object opacity."""

    def __init__(self):
        """Init parent class."""
        ColorRGBA.__init__(self)

        # options for check_props, update_props()
        self.OptionParser.add_option("--object_opacity",
                                     action="store",
                                     type="inkbool",
                                     dest="object_opacity",
                                     default=False,
                                     help="Modify object opacity")

    # override in sub-classes

    def modify_opacity(self, opacity):
        """Method overridden in sub-classes to modify object opacity."""
        pass

    # internal

    def _process_opacity(self, sdict):
        """Process object opacity in style dict *sdict* if requested."""
        changed = False
        if self.options.object_opacity:
            scope_props = ['fill', 'stroke']
            apply_scope = [prop for prop in scope_props if prop in IC_PROPS]
            if apply_scope:
                key = 'opacity'
                if [True for prop in apply_scope if prop in sdict]:
                    # simple check for graphics elements: omits (most)
                    # containers with unset opacity, gradient stops and other
                    # non-graphical elements.
                    old_val = sdict.get(key, None)
                    val = self.modify_opacity(
                        '1.0' if key not in sdict else sdict[key])
                    if val is not None:
                        sdict[key] = str(val)
                    changed = old_val == sdict.get(key, None)
        return changed

    def _modify_props(self, sdict, flags, has_opacity=True):
        """Modify properties in style dict *props* based on *flags"."""
        changed = False
        if not (flags and 'skip_color' in flags):
            changed = self._process_rgba_props(sdict)
        if not (flags and 'skip_opacity' in flags):
            if has_opacity:
                changed_opacity = self._process_opacity(sdict)
                changed = changed or changed_opacity
        return changed


class ColorOpacity(ColorProps):
    """Base class for modifying object opacity."""

    def __init__(self):
        """Init parent class."""
        ColorProps.__init__(self)

        # options for check_props, update_props()
        self.OptionParser.add_option("--object_opacity",
                                     action="store",
                                     type="inkbool",
                                     dest="object_opacity",
                                     default=False,
                                     help="Modify object opacity")

    # override in sub-classes

    def modify_opacity(self, opacity):
        """Method overridden in sub-classes to modify object opacity."""
        pass

    # internal

    def _process_opacity(self, sdict):
        """Process object opacity in style dict *sdict* if requested."""
        changed = False
        if self.options.object_opacity:
            scope_props = ['fill', 'stroke']
            apply_scope = [prop for prop in scope_props if prop in IC_PROPS]
            if apply_scope:
                key = 'opacity'
                if [True for prop in apply_scope if prop in sdict]:
                    # simple check for graphics elements: omits (most)
                    # containers with unset opacity, gradient stops and other
                    # non-graphical elements.
                    old_val = sdict.get(key, None)
                    val = self.modify_opacity(
                        '1.0' if key not in sdict else sdict[key])
                    if val is not None:
                        sdict[key] = str(val)
                    changed = old_val == sdict.get(key, None)
        return changed

    def _modify_props(self, sdict, flags, has_opacity=True):
        """Modify properties in style dict *props* based on *flags"."""
        changed = False
        if not (flags and 'skip_opacity' in flags):
            if has_opacity:
                changed = self._process_opacity(sdict)
        return changed


# ----- Legacy base class

class ColorEffect(ColorRGBAO):
    """Base class for color effects in Inkscape."""

    def __init__(self):
        """Init parent class."""
        ColorRGBAO.__init__(self)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
