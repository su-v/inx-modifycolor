#!/usr/bin/env python
"""
color_lib.colorinterp   - module with functions for color interpolations

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name

# standard library
import math

# local library
from color_lib import colorconvert as cv

# for debugging
# import inkex


# ----- linear interpolation

def simple_lerp(val_min, val_max, time):
    """Simple linear interpolation at *time* with 0.0 <= time <= 1.0."""
    return val_min + ((val_max - val_min) * time)


def lerp(val_min, val_max, time):
    """Precise linear interpolation at *time* with 0.0 <= time <= 1.0."""
    result = ((1.0 - time) * val_min) + (time * val_max)
    return result


def lerp_angle(v_min, v_max, time, turn=1.0, sweep='min'):
    """Lerp angle covering *sweep*."""
    func = lerp
    return interp_angle(v_min, v_max, time, turn, sweep, func)


# ----- interpolation of polar coordinate/angle

def interp_angle_cw(v_min, v_max, time, turn=1.0, func=lerp):
    """Interpolate angle CW at *time*."""
    if v_min < v_max:
        v_max = v_max - turn
    val = func(v_min, v_max, time)
    return val


def interp_angle_ccw(v_min, v_max, time, turn=1.0, func=lerp):
    """Interpolate angle CCW at *time*."""
    if v_min > v_max:
        v_max = v_max + turn
    val = func(v_min, v_max, time)
    return val


def interp_angle_min(v_min, v_max, time, turn=1.0, func=lerp):
    """Interpolate widest angle at *time* (can be CW or CCW)."""
    delta = v_max - v_min
    if abs(delta) < turn / 2.0:
        _interp = interp_angle_cw if delta <= 0 else interp_angle_ccw
    else:
        _interp = interp_angle_ccw if delta <= 0 else interp_angle_cw
    return _interp(v_min, v_max, time, turn, func)


def interp_angle_max(v_min, v_max, time, turn=1.0, func=lerp):
    """Interpolate widest angle at *time* (can be CW or CCW)."""
    delta = v_max - v_min
    if abs(delta) < turn / 2.0:
        _interp = interp_angle_ccw if delta <= 0 else interp_angle_cw
    else:
        _interp = interp_angle_cw if delta <= 0 else interp_angle_ccw
    return _interp(v_min, v_max, time, turn, func)


def interp_angle(v_min, v_max, time, turn=1.0, sweep='min', func=lerp):
    """Interpolate angle covering *sweep*."""
    # pylint: disable=too-many-arguments
    if sweep.lower() == 'ccw':
        return interp_angle_ccw(v_min, v_max, time, turn, func)
    elif sweep.lower() == 'cw':
        return interp_angle_cw(v_min, v_max, time, turn, func)
    elif sweep.lower() == 'min':
        return interp_angle_min(v_min, v_max, time, turn, func)
    elif sweep.lower() == 'max':
        return interp_angle_max(v_min, v_max, time, turn, func)


# ----- trigonometric interpolation methods
#
#       ref: http://jonmacey.blogspot.com/2010/11/interpolation.html

def coip(val_min, val_max, time):
    """Trigonometric interpolation at *time* with 0.0 <= time <= 1.0.

    ref: http://jonmacey.blogspot.com/2010/11/interpolation.html
    """
    angle = math.radians(lerp(0.0, 90.0, time))
    return ((val_min * math.pow(math.cos(angle), 2)) +
            (val_max * math.pow(math.sin(angle), 2)))


def coip_angle(v_min, v_max, time, turn=1.0, sweep='min'):
    """Lerp angle covering *sweep*."""
    func = coip
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def cosine(v_min, v_max, time):
    """Wrapper for coip()."""
    return coip(v_min, v_max, time)


def cosine_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Wrapper for coip_angle()."""
    return coip_angle(v_min, v_max, time, turn, sweep)


def cuip(val_min, val_max, time):
    """Cubic interpolation at *time* with 0.0 <= time <= 1.0.

    ref: http://jonmacey.blogspot.com/2010/11/interpolation.html
    """
    v1 = 1.0 + (2.0 * math.pow(time, 3)) - (3.0 * math.pow(time, 2))
    v2 = 0.0 - (2.0 * math.pow(time, 3)) + (3.0 * math.pow(time, 2))
    return (val_min * v1) + (val_max * v2)


def cuip_angle(v_min, v_max, time, turn=1.0, sweep='min'):
    """Lerp angle covering *sweep*."""
    func = cuip
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def cubic(v_min, v_max, time):
    """Wrapper for cuip()."""
    return cuip(v_min, v_max, time)


def cubic_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Wrapper for cuip_angle()."""
    return cuip_angle(v_min, v_max, time, turn, sweep)


# ----- trigonometric interpolation methods
#
#       ref: http://paulbourke.net/miscellaneous/interpolation/

def coip2(y1, y2, mu):
    """Cosine interpolation at *mu* with 0.0 <= mu <= 1.0.

    ref: http://paulbourke.net/miscellaneous/interpolation/
    """
    mu2 = (1.0 - math.cos(mu * math.pi)) / 2.0
    return (y1 * (1.0 - mu2)) + (y2 * mu2)


def coip2_angle(v_min, v_max, time, turn=1.0, sweep='min'):
    """Lerp angle covering *sweep*."""
    func = coip2
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def cosine2(v_min, v_max, time):
    """Wrapper for coip2()."""
    return coip2(v_min, v_max, time)


def cosine2_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Wrapper for coip2_angle()."""
    return coip2_angle(v_min, v_max, time, turn, sweep)


# ----- trigonometric interpolation methods
#
#       ref: http://codeplea.com/simple-interpolation

def step(v_min, v_max, time):
    """Step interpolation."""
    return v_min if time < 0.5 else v_max


def step_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Step interpolation for angle."""
    func = step
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def linear(v_min, v_max, time):
    """Linear interpolation."""
    # return v_min + time * (v_max - v_min)
    return lerp(v_min, v_max, time)


def linear_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Linear interpolation for angle."""
    func = linear
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def cosine3(v_min, v_max, time):
    """Cosine interpolation."""
    timed = ((-1 * math.cos(math.pi * time)) / 2) + 0.5
    return linear(v_min, v_max, timed)


def cosine3_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Cosine interpolation for angle."""
    func = cosine3
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def smooth_step(v_min, v_max, time):
    """Smooth Step interpolation."""
    timed = math.pow(time, 2) * (3 - (2 * time))
    return linear(v_min, v_max, timed)


def smooth_step_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Smooth Step interpolation for angle."""
    func = smooth_step
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def acceleration(v_min, v_max, time):
    """Accelerated interpolation."""
    timed = 1 - math.pow(1 - time, 2)
    return linear(v_min, v_max, timed)


def acceleration_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Accelerated interpolation for angle."""
    func = acceleration
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def accel(v_min, v_max, time):
    """Wrapper for Accelerated interpolation."""
    return acceleration(v_min, v_max, time)


def accel_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Wrapper for Accelerated interpolation for angle."""
    func = accel
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def deceleration(v_min, v_max, time):
    """Decelerated interpolation."""
    timed = math.pow(time, 2)
    return linear(v_min, v_max, timed)


def deceleration_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Accelerated interpolation for angle."""
    func = deceleration
    return interp_angle(v_min, v_max, time, turn, sweep, func)


def decel(v_min, v_max, time):
    """Wrapper for Decelerated interpolation."""
    return deceleration(v_min, v_max, time)


def decel_polar(v_min, v_max, time, turn=1.0, sweep='min'):
    """Wrapper for Accelerated interpolation for angle."""
    func = decel
    return interp_angle(v_min, v_max, time, turn, sweep, func)


# ----- Cubic spline interpolation methods (untested)
#
#       ref: http://paulbourke.net/miscellaneous/interpolation/

def cuip2(y0, y1, y2, y3, mu, mode='default'):
    """Cubic interpolation at *mu* with 0.0 <= mu <= 1.0.

    ref: http://paulbourke.net/miscellaneous/interpolation/
    """
    # pylint: disable=too-many-arguments
    mu2 = math.pow(mu, 2)
    if mode == 'catmull-rom':
        a0 = 0.5*y3 - 1.5*y2 - 0.5*y0 + 1.5*y1
        a1 = y0 - 2.5*y1 + 2*y2 - 0.5*y3
        a2 = 0.5*y2 - 0.5*y0
        a3 = y1
    else:
        a0 = y3 - y2 - y0 + y1
        a1 = y0 - y1 - a0
        a2 = y2 - y0
        a3 = y1
    return (a0 * mu * mu2) + (a1 * mu2) + (a2 * mu) + a3


# ----- bezier interpolation
#
#       ref: http://bsou.io/posts/color-gradients-with-python

def binomial_coefficient(n, k):
    """A direct implementation of the multiplicative formula ...

    ref: https://en.wikipedia.org/wiki/Binomial_coefficient
    """
    # inefficient:
    # return factorial(n) // (factorial(k) * factorial(n - k))

    if k < 0 or k > n:
        return 0
    if k == 0 or k == n:
        return 1
    k = min(k, n - k)  # take advantage of symmetry
    c = 1
    for i in range(k):
        c = c * (n - i) / (i + 1)
    return c


def bernstein(t, n, i):
    """Bernstein coefficient."""
    binom = binomial_coefficient(n, i)
    return binom * ((1 - t)**(n-i)) * (t**i)


def bezier_gradient(color_list, n_out=100):
    """Return a "bezier gradient" list of colors (component triplets)
    using a given list of colors as control points.

    ref: http://bsou.io/posts/color-gradients-with-python
    """

    def bezier_interp(t):
        """Define an interpolation function for this specific curve."""
        # List of all summands
        summands = [[(bernstein(t, n, i) * x) for x in c]
                    for i, c in enumerate(color_list)]
        # Output color
        out = 3 * [0.0]
        # Add components of each summand together
        for vector in summands:
            for c in range(3):
                out[c] += vector[c]
        # return list with interpolated values
        return out

    # color coordinate vectors for each color, use as control points
    n = len(color_list) - 1

    # interpolate
    return [bezier_interp(float(t) / (n_out - 1)) for t in range(n_out)]


# ----- color interpolation: dispatch min/max interpolators

def linear_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min'):
    """Cosine interpolation of two InkColor objects in *colorspace*."""
    func = linear
    func_angle = linear_polar
    return interp_ic(ic_min, ic_max, time, colorspace, sweep, func, func_angle)


def cosine_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min'):
    """Cosine interpolation of two InkColor objects in *colorspace*."""
    func = cosine
    func_angle = cosine_polar
    return interp_ic(ic_min, ic_max, time, colorspace, sweep, func, func_angle)


def cubic_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min'):
    """Cubic interpolation of two InkColor objects in *colorspace*."""
    func = cubic
    func_angle = cubic_polar
    return interp_ic(ic_min, ic_max, time, colorspace, sweep, func, func_angle)


def accel_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min'):
    """Accelerated interpolation of two InkColor objects in *colorspace*."""
    func = accel
    func_angle = accel_polar
    return interp_ic(ic_min, ic_max, time, colorspace, sweep, func, func_angle)


def decel_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min'):
    """Accelerated interpolation of two InkColor objects in *colorspace*."""
    func = decel
    func_angle = decel_polar
    return interp_ic(ic_min, ic_max, time, colorspace, sweep, func, func_angle)


def interp_ic(ic_min, ic_max, time, colorspace='sRGB', sweep='min',
              func=linear, func_angle=linear_polar):
    """Dispatch interpolation of two InkColor objects in *colorspace*."""
    # pylint: disable=too-many-arguments

    def _interp_coords(start, end):
        """Apply func to all 3 coordinate pairs."""
        return [func(start[0], end[0], time),
                func(start[1], end[1], time),
                func(start[2], end[2], time)]

    def _interp_with_hue(start, end, turn=1.0):
        """Apply func to last 2 coord pairs, func_angle to first."""
        return [func_angle(start[0], end[0], time, turn, sweep),
                func(start[1], end[1], time),
                func(start[2], end[2], time)]

    new_color = ic_min.copy()
    if colorspace == 'sRGB':
        start, end = [c.rgb_float() for c in (ic_min, ic_max)]
        new_color.from_float(*_interp_coords(start, end))
    elif colorspace == 'linearRGB':
        start, end = [c.rgb_linear() for c in (ic_min, ic_max)]
        new_color.from_linear(*_interp_coords(start, end))
    elif colorspace == 'LUV':
        start, end = [cv.rgb_to_luv(*c.rgb_float()) for c in (ic_min, ic_max)]
        new_color.from_float(*cv.luv_to_rgb(*_interp_coords(start, end)))
    elif colorspace == 'LAB':
        start, end = [cv.rgb_to_lab(*c.rgb_float()) for c in (ic_min, ic_max)]
        new_color.from_float(*cv.lab_to_rgb(*_interp_coords(start, end)))
    elif colorspace == 'HSL':
        start, end = [c.rgb_to_hsl() for c in (ic_min, ic_max)]
        new_color.from_hsl(*_interp_with_hue(start, end, turn=1.0))
    elif colorspace == 'HSV':
        start, end = [c.rgb_to_hsv() for c in (ic_min, ic_max)]
        new_color.from_hsv(*_interp_with_hue(start, end, turn=1.0))
    elif colorspace == 'LCH':
        start, end = [cv.rgb_to_lch(*c.rgb_float()) for c in (ic_min, ic_max)]
        # reorder components: hue first
        start.insert(0, start.pop())
        end.insert(0, end.pop())
        new = _interp_with_hue(start, end, turn=360.0)
        # reorder components back: hue last
        new.append(new.pop(0))
        new_color.from_float(*cv.lch_to_rgb(*new))
    return new_color


# ----- color interpolation: cubic spline (catmull-rom)


# ----- color interpolation: spline


# ----- color interpolation: bezier curve

def bezier_srgb(ic_list, steps=8):
    """Bezier interpolation of RGB-based InkColor objects in sRGB."""
    # create bezier gradient
    gradient = bezier_gradient([c.rgb_int() for c in ic_list], n_out=steps)
    # convert gradient color lists back to list of InkColor objects
    if gradient is not None and len(gradient):
        ic_temp = ic_list[0].copy()
        ic_out_list = []
        for color in gradient:
            ic_temp.from_int(*[int(v) for v in color])
            ic_out_list.append(ic_temp.copy())
        return ic_out_list


def bezier_lrgb(ic_list, steps=8):
    """Bezier interpolation of RGB-based InkColor objects in linearRGB."""
    # create bezier gradient
    gradient = bezier_gradient([c.rgb_linear() for c in ic_list], n_out=steps)
    # convert gradient color lists back to list of InkColor objects
    if gradient is not None and len(gradient):
        ic_temp = ic_list[0].copy()
        ic_out_list = []
        for color in gradient:
            ic_temp.from_linear(*color)
            ic_out_list.append(ic_temp.copy())
        return ic_out_list


def bezier_luv(ic_list, steps=8):
    """Bezier interpolation of RGB-based InkColor objects in CIELUV."""
    # create new list with color coordinates based on CIE Luv values
    in_list = [cv.rgb_to_luv(*c.rgb_float()) for c in ic_list]
    # create bezier gradient
    gradient = bezier_gradient(in_list, n_out=steps)
    # convert gradient color lists back to list of InkColor objects
    if gradient is not None and len(gradient):
        ic_temp = ic_list[0].copy()
        ic_out_list = []
        for color in gradient:
            ic_temp.from_float(*cv.luv_to_rgb(*color))
            ic_out_list.append(ic_temp.copy())
        return ic_out_list


def bezier_lab(ic_list, steps=8):
    """Bezier interpolation of RGB-based InkColor objects in CIELUV."""
    # create new list with color coordinates based on CIE Lab values
    in_list = [cv.rgb_to_lab(*c.rgb_float()) for c in ic_list]
    # create bezier gradient
    gradient = bezier_gradient(in_list, n_out=steps)
    # convert gradient color lists back to list of InkColor objects
    if gradient is not None and len(gradient):
        ic_temp = ic_list[0].copy()
        ic_out_list = []
        for color in gradient:
            ic_temp.from_float(*cv.lab_to_rgb(*color))
            ic_out_list.append(ic_temp.copy())
        return ic_out_list


def bezier_ic(ic_list, steps, colorspace='sRGB'):
    """Linear interp of two RGB-based InkColor objects in *colorspace*."""
    new_colors = None
    if colorspace == 'sRGB':
        new_colors = bezier_srgb(ic_list, steps)
    elif colorspace == 'linearRGB':
        new_colors = bezier_lrgb(ic_list, steps)
    elif colorspace == 'HSL':
        return ic_list  # not supported
    elif colorspace == 'HSV':
        return ic_list  # not supported
    elif colorspace == 'LUV':
        new_colors = bezier_luv(ic_list, steps)
    elif colorspace == 'LAB':
        new_colors = bezier_lab(ic_list, steps)
    elif colorspace == 'LCH':
        return ic_list  # not supported
    else:
        new_colors = None
    return new_colors


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
