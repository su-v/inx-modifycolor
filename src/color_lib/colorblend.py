#!/usr/bin/env python
"""
color_lib.colorblend    - module with functions for color blending
                          (blend modes for two SVG paints in RGBA)

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math  # sqrt

# local library
from color_lib.util import min_mid_max
import inkex


# globals
DEBUG = False
MODINPLACE = True


def showme(msg):
    """Verbose debug messages to stderr."""
    if DEBUG:
        inkex.debug(msg)


def premul(ic_color):
    """Pre-multiply InkColor *ic_color* with alpha.

    Return list with r, g, b, alpha.
    """
    r, g, b = [c * ic_color.alpha for c in ic_color.rgb_float()]
    return r, g, b, ic_color.alpha


def premull(r, g, b, a):
    """Pre-multiply *r*, *g*, *b* with alpha *a*.

    Return list with r, g, b, a.
    """
    r, g, b = [c * a for c in (r, g, b)]
    return r, g, b, a


def unpremul(r, g, b, a):
    """Un-pre-multiply *r*, *g*, *b* with alpha *a*.

    Return list with r, g, b, a
    """
    if a > 0:
        r, g, b = [c / a for c in (r, g, b)]
    return r, g, b, a


# ----- SVG 1.1: Filter primitive 'feBlend'
#       https://www.w3.org/TR/SVG11/filters.html#feBlendElement

def blend_normal(ic_this, ic_other, linear=False):
    """Blend InkColor *ic_this* over InkColor *ic_other*.

    Return new InkColor object.
    """
    if linear:
        a = premull(*ic_this.rgba_linear())
        b = premull(*ic_other.rgba_linear())
    else:
        a = premul(ic_this)
        b = premul(ic_other)
    qa = ic_this.alpha
    qb = ic_other.alpha
    # cr = (1 - qa) * cb + ca
    r = ((1 - qa) * b[0]) + a[0]
    g = ((1 - qa) * b[1]) + a[1]
    b = ((1 - qa) * b[2]) + a[2]
    # qr = 1 - (1-qa)*(1-qb)
    a = 1 - ((1 - qa) * (1 - qb))
    # return new InkColor object
    new_color = ic_this.copy()
    if linear:
        new_color.from_linear(*unpremul(r, g, b, a))
    else:
        new_color.from_float(*unpremul(r, g, b, a))
    return new_color


def blend_multiply(ic_this, ic_other, linear=False):
    """Blend InkColor *ic_this*, *ic_other* with 'multiply'.

    Return new InkColor object.
    """
    if linear:
        a = premull(*ic_this.rgba_linear())
        b = premull(*ic_other.rgba_linear())
    else:
        a = premul(ic_this)
        b = premul(ic_other)
    qa = ic_this.alpha
    qb = ic_other.alpha
    # cr = (1 - qa) * cb + (1 - qb) * ca + ca * cb
    r = ((1 - qa) * b[0]) + ((1 - qb) * a[0]) + (a[0] * b[0])
    g = ((1 - qa) * b[1]) + ((1 - qb) * a[1]) + (a[1] * b[1])
    b = ((1 - qa) * b[2]) + ((1 - qb) * a[2]) + (a[2] * b[2])
    # qr = 1 - (1-qa)*(1-qb)
    a = 1 - ((1 - qa) * (1 - qb))
    # return new InkColor object
    new_color = ic_this.copy()
    if linear:
        new_color.from_linear(*unpremul(r, g, b, a))
    else:
        new_color.from_float(*unpremul(r, g, b, a))
    return new_color


def blend_screen(ic_this, ic_other, linear=False):
    """Blend InkColor *ic_this*, *ic_other* with 'screen'.

    Return new InkColor object.
    """
    if linear:
        a = premull(*ic_this.rgba_linear())
        b = premull(*ic_other.rgba_linear())
    else:
        a = premul(ic_this)
        b = premul(ic_other)
    qa = ic_this.alpha
    qb = ic_other.alpha
    # cr = cb + ca - ca * cb
    r = b[0] + a[0] - (a[0] * b[0])
    g = b[1] + a[1] - (a[1] * b[1])
    b = b[2] + a[2] - (a[2] * b[2])
    # qr = 1 - (1-qa)*(1-qb)
    a = 1 - ((1 - qa) * (1 - qb))
    # return new InkColor object
    new_color = ic_this.copy()
    if linear:
        new_color.from_linear(*unpremul(r, g, b, a))
    else:
        new_color.from_float(*unpremul(r, g, b, a))
    return new_color


def blend_darken(ic_this, ic_other, linear=False):
    """Blend InkColor *ic_this*, *ic_other* with 'darken'.

    Return new InkColor object.
    """
    if linear:
        a = premull(*ic_this.rgba_linear())
        b = premull(*ic_other.rgba_linear())
    else:
        a = premul(ic_this)
        b = premul(ic_other)
    qa = ic_this.alpha
    qb = ic_other.alpha
    # cr = Minimum ( (1 - qa) * cb + ca, (1 - qb) * ca + cb )
    r = min(((1 - qa) * b[0]) + a[0], ((1 - qb) * a[0]) + b[0])
    g = min(((1 - qa) * b[1]) + a[1], ((1 - qb) * a[1]) + b[1])
    b = min(((1 - qa) * b[2]) + a[2], ((1 - qb) * a[2]) + b[2])
    # qr = 1 - (1-qa)*(1-qb)
    a = 1 - ((1 - qa) * (1 - qb))
    # return new InkColor object
    new_color = ic_this.copy()
    if linear:
        new_color.from_linear(*unpremul(r, g, b, a))
    else:
        new_color.from_float(*unpremul(r, g, b, a))
    return new_color


def blend_lighten(ic_this, ic_other, linear=False):
    """Blend InkColor *ic_this*, *ic_other* with 'lighten'.

    Return new InkColor object.
    """
    if linear:
        a = premull(*ic_this.rgba_linear())
        b = premull(*ic_other.rgba_linear())
    else:
        a = premul(ic_this)
        b = premul(ic_other)
    qa = ic_this.alpha
    qb = ic_other.alpha
    # cr = Maximum ( (1 - qa) * cb + ca, (1 - qb) * ca + cb )
    r = max(((1 - qa) * b[0]) + a[0], ((1 - qb) * a[0]) + b[0])
    g = max(((1 - qa) * b[1]) + a[1], ((1 - qb) * a[1]) + b[1])
    b = max(((1 - qa) * b[2]) + a[2], ((1 - qb) * a[2]) + b[2])
    # qr = 1 - (1-qa)*(1-qb)
    a = 1 - ((1 - qa) * (1 - qb))
    # return new InkColor object
    new_color = ic_this.copy()
    if linear:
        new_color.from_linear(*unpremul(r, g, b, a))
    else:
        new_color.from_float(*unpremul(r, g, b, a))
    return new_color


def blend(ic_other, ic_this, mode=blend_normal, linear=False):
    """Dispatcher for "old" blend modes passed in as function ref."""
    return mode(ic_this, ic_other, linear)


def color_blend(ic_this, ic_other, mode="normal", linear=False):
    """Blend InkColor *ic_this* over *ic_other* with blend *mode*.

    Return new InkColor object.
    """
    if mode.endswith('normal'):
        return blend_normal(ic_this, ic_other, linear)
    elif mode.endswith('multiply'):
        return blend_multiply(ic_this, ic_other, linear)
    elif mode.endswith('screen'):
        return blend_screen(ic_this, ic_other, linear)
    elif mode.endswith('darken'):
        return blend_darken(ic_this, ic_other, linear)
    elif mode.endswith('lighten'):
        return blend_lighten(ic_this, ic_other, linear)


def blend_over_white(ic_this):
    """Blend *ic_this* over solid white background.

    Update InkColor object *ic_this*.
    """
    ic_other = ic_this.copy()
    ic_other.from_css('white')
    ic_other.alpha = 1.0
    ic_blend = blend_normal(ic_this, ic_other)
    ic_this.__dict__.update(ic_blend.__dict__)


# ----- Compositing and Blending Level 1
#       http://www.w3.org/TR/compositing-1

# ----- 1. Separable blend modes
#
# A blend mode is termed separable if each component of the result color is
# completely determined by the corresponding components of the constituent
# backdrop and source colors - that is, if the mixing formula is applied
# separately to each set of corresponding components.

def css_blend_normal(cb, cs):
    """CSS blend mode 'normal'."""
    #
    # B(Cb, Cs) = Cs
    #
    # pylint: disable=unused-argument
    return cs


def css_blend_multiply(cb, cs):
    """CSS blend mode 'multiply'."""
    #
    # B(Cb, Cs) = Cb x Cs
    #
    return cb * cs


def css_blend_screen(cb, cs):
    """CSS blend mode 'screen'."""
    #
    # B(Cb, Cs) = 1 - [(1 - Cb) x (1 - Cs)]
    #           = Cb + Cs -(Cb x Cs)
    #
    return cb + cs - (cb * cs)


def css_blend_overlay(cb, cs):
    """CSS blend mode 'overlay'."""
    #
    # B(Cb, Cs) = HardLight(Cs, Cb)
    #
    return css_blend_hardlight(cs, cb)


def css_blend_darken(cb, cs):
    """CSS blend mode 'darken'."""
    #
    # B(Cb, Cs) = min(Cb, Cs)
    #
    return min(cb, cs)


def css_blend_lighten(cb, cs):
    """CSS blend mode 'lighten'."""
    #
    # B(Cb, Cs) = max(Cb, Cs)
    #
    return max(cb, cs)


def css_blend_colordodge(cb, cs):
    """CSS blend mode 'color-dodge'."""
    #
    # if(Cb == 0)
    #     B(Cb, Cs) = 0
    # else if(Cs == 1)
    #     B(Cb, Cs) = 1
    # else
    #     B(Cb, Cs) = min(1, Cb / (1 - Cs))
    #
    if cb == 0.0:
        return 0.0
    elif cs == 1.0:
        return 1.0
    else:
        return min(1, cb / (1 - cs))


def css_blend_colorburn(cb, cs):
    """CSS blend mode 'color-burn'."""
    #
    # if(Cb == 1)
    #     B(Cb, Cs) = 1
    # else if(Cs == 0)
    #     B(Cb, Cs) = 0
    # else
    #     B(Cb, Cs) = 1 - min(1, (1 - Cb) / Cs
    #
    if cb == 1.0:
        return 1.0
    elif cs == 0.0:
        return 0.0
    else:
        return 1 - min(1, (1 - cb) / cs)


def css_blend_hardlight(cb, cs):
    """CSS blend mode 'hard-light'."""
    #
    # if(Cs <= 0.5)
    #     B(Cb, Cs) = Multiply(Cb, 2 x Cs)
    # else
    #     B(Cb, Cs) = Screen(Cb, 2 x Cs -1)
    #
    cb *= 2
    if cb <= 1:
        return css_blend_multiply(cb, cs)
    else:
        return css_blend_screen(cb - 1, cs)


def css_blend_softlight(cb, cs):
    """CSS blend mode 'soft-light'."""
    #
    #     if(Cs <= 0.5)
    #         B(Cb, Cs) = Cb - (1 - 2 x Cs) x Cb x (1 - Cb)
    #     else
    #         B(Cb, Cs) = Cb + (2 x Cs - 1) x (D(Cb) - Cb)
    # with
    #     if(Cb <= 0.25)
    #         D(Cb) = ((16 * Cb - 12) x Cb + 4) x Cb
    #     else
    #         D(Cb) = sqrt(Cb)
    #
    d = 1
    e = cb
    if cs > 0.5:
        e = 1
        d = math.sqrt(cb) if cb > 0.25 else ((16 * cb - 12) * cb + 4) * cb
    return cb - ((1 - 2 * cs) * e * (d - cb))


def css_blend_difference(cb, cs):
    """CSS blend mode 'difference'."""
    #
    # B(Cb, Cs) = | Cb - Cs |
    #
    return abs(cb - cs)


def css_blend_exclusion(cb, cs):
    """CSS blend mode 'exclusion'."""
    #
    # B(Cb, Cs) = Cb + Cs - 2 x Cb x Cs
    #
    return cb + cs - (2 * cb * cs)


def css_blend_average(cb, cs):
    """LESS blend mode 'average'.

    Compute the average of two colors on a per-channel (RGB) basis.

    ref: http://lesscss.org/functions/#color-blending-average
    """
    return (cb + cs) / 2.0


def css_blend_negation(cb, cs):
    """LESS blend mode 'negation'.

    Do the opposite effect to difference. The result is a brighter color.
    Note: The opposite effect doesn't mean the inverted effect as resulting
    from an addition operation.

    ref: http://lesscss.org/functions/#color-blending-negation
    """
    return 1 - abs(cb + cs - 1)


def css_blend_divide(cb, cs):
    """Divison (additive, custom).
    This blend mode simply divides pixel values of one layer with the other,
    but it's useful for brightening photos if the color is on grey or less. It
    is also useful for removing a color tint from a photo. If you create a
    layer that is the color of the tint you wish to remove - such as a pale
    blue, for scenes that are too cool in color temperature - Divide mode will
    return that color to white in the resulting composite, as any value divided
    by itself equals 1.0 (white).

    ref: https://en.wikipedia.org/wiki/Blend_modes#Divide
    """
    return cb / cs if cs > 0 else 0


def css_blend_add(cb, cs):
    """Addition (additive, custom).
    This blend mode simply adds pixel values of one layer with the other. In
    case of values above 1 (in the case of RGB), white is displayed. "Linear
    Dodge" produces the same visual result. Since this always produces the same
    or lighter colors than the input it is also known as 'plus lighter'. A
    variant subtracts 1 from all end values, with values below 0 becoming
    black; this mode is known as 'plus darker'.

    ref: https://en.wikipedia.org/wiki/Blend_modes#Addition
    """
    return cb + cs


def css_blend_addlighter(cb, cs):
    """Addition (additive, custom) - 'plus lighter'."""
    return css_blend_add(cb, cs)


def css_blend_adddarker(cb, cs):
    """Addition (additive, custom) - 'plus darker'."""
    return css_blend_add(cb, cs) - 1


def css_blend_subtract(cb, cs):
    """Subtraction (additive, custom).
    This blend mode simply subtracts pixel values of one layer with the other.
    In case of negative values, black is displayed.

    ref: https://en.wikipedia.org/wiki/Blend_modes#Subtract
    """
    return cb - cs


def css_blend(ic_backdrop, ic_source, mode=css_blend_normal, linear=False):
    """Separable CSS blend modes over backdrop.

    Reference: http://www.w3.org/TR/compositing-1
    """

    # usage:
    # >>> from color_lib import inkcolor as ic
    # >>> bg = ic.InkColorRGBACSS('blue')
    # >>> red = ic.InkColorRGBACSS('red')
    # >>> red.alpha = 0.5
    # >>> from color_lib import colorblend as cb
    # >>> cb.css_blend(bg, red, mode=getattr(cb, 'css_blend_normal'))
    # >>> rgba(128, 0, 128, 1.0)

    a_b = ic_backdrop.alpha
    a_s = ic_source.alpha

    if linear:
        c_b = ic_backdrop.rgb_linear()
        c_s = ic_source.rgb_linear()
    else:
        c_b = ic_backdrop.rgb_float()
        c_s = ic_source.rgb_float()

    c_r = 3 * [0]

    # alpha
    a_r = a_s + (a_b * (1 - a_s))

    # blend and composite
    for i in range(3):
        cb = c_b[i]
        cs = c_s[i]
        cr = mode(cb, cs)
        if a_r:
            cr = (a_s*cs + a_b*(cb - a_s*(cb + cs - cr))) / a_r
        c_r[i] = cr

    # return result as new InkColor object
    new_color = ic_backdrop.copy()
    if linear:
        new_color.from_linear(*c_r, alpha=a_r)
    else:
        new_color.from_float(*c_r, alpha=a_r)
    return new_color


# ----- 2. Non-separable blend modes
#
# Nonseparable blend modes consider all color components in combination
# as opposed to the separable ones that look at each component
# individually. All of these blend modes conceptually entail the
# following steps:
#
# 1. Convert the backdrop and source colors from the blending color
#    space to an intermediate hue-saturation-luminosity representation.
# 2. Create a new color from some combination of hue, saturation, and
#    luminosity components selected from the backdrop and source colors.
# 3. Convert the result back to the original color space

def lum(color):
    """Return luminance of RGB *color*."""
    r, g, b = color
    return 0.3 * r + 0.59 * g + 0.11 * b


def clip_color(color):
    """Return clipped RGB *color*."""
    l = lum(color)
    c_min = min(*color)
    c_max = max(*color)
    if c_min < 0.0:
        color = [l + (((c - l) * l) / (l - c_min)) for c in color]
    if c_max > 1.0:
        color = [l + (((c - l) * (1 - l)) / (c_max - l)) for c in color]
    return color


def set_lum(color, l):
    """Set luminance *l* for RGB *color*."""
    d = l - lum(color)
    color = [c + d for c in color]
    return clip_color(color)


def sat(color):
    """Return saturation of RGB *color*."""
    return max(*color) - min(*color)


def set_sat(color, s):
    """Set saturation *s* for RGB *color*."""
    c_min, c_mid, c_max = min_mid_max(list(color))
    max_min = color[c_max] - color[c_min]
    mid_min = color[c_mid] - color[c_min]
    if max_min:
        color[c_mid] = (mid_min * s) / max_min
        color[c_max] = s
    else:
        color[c_mid] = 0.0
        color[c_max] = 0.0
    color[c_min] = 0.0
    return color


def css_blend_hsl_hue(c_b, c_s):
    """CSS blend mode 'hue'."""
    #
    # B(Cb, Cs) = SetLum(SetSat(Cs, Sat(Cb)), Lum(Cb))
    #
    return set_lum(set_sat(c_s, sat(c_b)), lum(c_b))


def css_blend_hsl_saturation(c_b, c_s):
    """CSS blend mode 'saturation'."""
    #
    # B(Cb, Cs) = SetLum(SetSat(Cb, Sat(Cs)), Lum(Cb))
    #
    # FIXME: desaturated colors in source or backdrop
    return set_lum(set_sat(c_b, sat(c_s)), lum(c_b))


def css_blend_hsl_color(c_b, c_s):
    """CSS blend mode 'color'."""
    #
    # B(Cb, Cs) = SetLum(Cs, Lum(Cb))
    #
    return set_lum(c_s, lum(c_b))


def css_blend_hsl_luminosity(c_b, c_s):
    """CSS blend mode 'luminosity'."""
    #
    # B(Cb, Cs) = SetLum(Cb, Lum(Cs))
    #
    return set_lum(c_b, lum(c_s))


def css_blend_hsl(ic_backdrop, ic_source, mode=css_blend_hsl_hue,
                  linear=False):
    """Non-separable CSS blend modes over backdrop."""

    a_b = ic_backdrop.alpha
    a_s = ic_source.alpha

    if linear:
        c_b = ic_backdrop.rgb_linear()
        c_s = ic_source.rgb_linear()
    else:
        c_b = ic_backdrop.rgb_float()
        c_s = ic_source.rgb_float()

    c_r = 3 * [0.0]

    # alpha
    a_r = a_s + (a_b * (1 - a_s))

    # blend
    c_r = mode(c_b, c_s)

    # composite
    for i in range(3):
        cb = c_b[i]
        cs = c_s[i]
        cr = c_r[i]
        if a_r:
            cr = (a_s*cs + a_b*(cb - a_s*(cb + cs - cr))) / a_r
        c_r[i] = cr

    # return result as new InkColor object
    new_color = ic_backdrop.copy()
    if linear:
        new_color.from_linear(*c_r, alpha=a_r)
    else:
        new_color.from_float(*c_r, alpha=a_r)
    return new_color


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
