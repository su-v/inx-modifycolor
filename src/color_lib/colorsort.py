#!/usr/bin/env python
"""
color_lib.colorsort     - module with functions for color sorting

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import inkcolor as ic
from color_lib import colorconvert as cv


def sort_colors_by_ic(colors, sort_prop, out_method, reverse=False):
    """Sort *colors* by InkColor attribute *sort_prop*."""

    def ic_key(color):
        """Sorting key for InkColor attribute."""
        ic_color.from_hex(color)
        ic_list = [ic_attrib.fget(ic_color)]
        ic_list.extend(ic_method(ic_color))
        return ic_list

    ic_attrib = getattr(ic.InkColor, sort_prop, None)
    ic_method = getattr(ic.InkColor, out_method, None)
    if ic_attrib is not None and ic_method is not None:
        ic_color = ic.InkColor()
        colors.sort(key=ic_key, reverse=reverse)


def sort_colors_by_cie(colors, sort_prop, reverse=False):
    """Sort *colors* by CIE color component in *sort_prop*."""

    def cie_key(color):
        """Sorting key for CIE color component."""
        ic_color.from_hex(color)
        cv_color = cv_method(*ic_color.rgb_float())
        cv_list = [cv_color[cv_attr]]
        cv_list.extend(cv_color)
        cv_list.extend([ic_color.alpha])
        return cv_list

    cv_props = ()
    cv_attr = None
    if sort_prop.startswith('lch'):
        cv_props = ('lch_l', 'lch_c', 'lch_h')
    elif sort_prop.startswith('lab'):
        cv_props = ('lab_l', 'lab_a', 'lab_b')
    elif sort_prop.startswith('luv'):
        cv_props = ('luv_l', 'luv_u', 'luv_v')
    if sort_prop in cv_props:
        cv_attr = cv_props.index(sort_prop)
    cv_method = getattr(cv, 'rgb_to_{}'.format(sort_prop[:3]), None)
    if cv_attr is not None and cv_method is not None:
        ic_color = ic.InkColor()
        colors.sort(key=cie_key, reverse=reverse)


def sort_colors_by_count(colors, rdict, cdict, reverse=False):
    """Sort *colors* in-place by object count in *cdict*.

    cdict = { color_id: count, ... }
    rdict = { color_id: hex_color, ... }
    """

    def count_key(color_id):
        """Sorting key for count."""
        if color_id in cdict:
            return (cdict[color_id], rdict[color_id])
        else:
            return 0

    sorted_colors = [rdict[key] for key in
                     sorted(rdict, key=count_key, reverse=not reverse)]
    for i in range(len(colors)):  # pylint: disable=consider-using-enumerate
        colors[i] = sorted_colors[i]


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
