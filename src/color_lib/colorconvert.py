#!/usr/bin/env python
"""
color_lib.colorconvert  - module with functions for color conversions

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math  # pow, sqrt

# local libary
from color_lib import colormatrix as cm
from color_lib import colorconst as cc
from color_lib.util import cbrt


# ----- sRGB <-> linearRGB

def srgb_to_linear(val):
    """Convert sRGB value to linearRGB."""
    # Based on:
    # http://www.4p8.com/eric.brasseur/gamma.html
    a = 0.055
    if val <= 0.04045:
        return val / 12.92
    else:
        return math.pow((val + a) / (1 + a), 2.4)


def linear_to_srgb(val):
    """Convert linearRGB value to sRGB."""
    # Based on:
    # http://www.4p8.com/eric.brasseur/gamma.html
    a = 0.055
    if val <= 0.0031308:
        return 12.92 * val
    else:
        return ((1 + a) * math.pow(val, 1 / 2.4)) - a


# ----- RGB <-> CIEXYZ

def rgb_to_xyz(r, g, b):
    """Convert RGB to XYZ."""
    r, g, b = [srgb_to_linear(c) for c in (r, g, b)]

    # Observer = 2 deg, Illuminant = D65
    cie_x = (r * 0.4124564) + (g * 0.3575761) + (b * 0.1804375)
    cie_y = (r * 0.2126729) + (g * 0.7151522) + (b * 0.0721750)
    cie_z = (r * 0.0193339) + (g * 0.1191920) + (b * 0.9503041)

    return [cie_x, cie_y, cie_z]


def xyz_to_rgb(x, y, z):
    """Convert RGB to XYZ."""
    x, y, z = [v for v in (x, y, z)]

    # Observer = 2 deg, Illuminant = D65
    rgb_r = (x * 3.2404542) + (y * -1.5371385) + (z * -0.4985314)
    rgb_g = (x * -0.9692660) + (y * 1.8760108) + (z * 0.0415560)
    rgb_b = (x * 0.0556434) + (y * -0.2040259) + (z * 1.0572252)

    return [linear_to_srgb(c) for c in (rgb_r, rgb_g, rgb_b)]


# ----- CIEXYZ chromatic adaptions

def d65_to_d50(x, y, z):
    """Bradford chromatic adaptation from D65 to D50.

    ref: https://drafts.csswg.org/css-color-4/#color-conversion-code
    """
    cie_x = (x * 1.0478112) + (y * 0.0228866) + (z * -0.0501270)
    cie_y = (x * 0.029542) + (y * 0.9904844) + (z * -0.0170491)
    cie_z = (x * -0.009234) + (y * 0.0150436) + (z * 0.7521316)

    return [cie_x, cie_y, cie_z]


def d50_to_d65(x, y, z):
    """Bradford chromatic adaptation from D50 to D65.

    ref: https://drafts.csswg.org/css-color-4/#color-conversion-code
    """
    cie_x = (x * 0.9555766) + (y * -0.0230393) + (z * 0.0631636)
    cie_y = (x * -0.0282895) + (y * 1.0099416) + (z * 0.0210077)
    cie_z = (x * 0.0122982) + (y * -0.0204830) + (z * 1.3299098)

    return [cie_x, cie_y, cie_z]


# ----- CIEXYZ helper funcs

def _func_from_xyz(val):
    """Helper function for XYZ->LUV/LAB."""
    if val > cc.CIEXYZ_E:
        return cbrt(val)
    else:
        return (cc.CIEXYZ_K * val + 16) / 116


def _func_to_xyz(val):
    """Helper function for LUV/LAB->XYZ."""
    result = math.pow(val, 3.0)
    if result > cc.CIEXYZ_E:
        return result
    else:
        return ((116 * val) - 16) / cc.CIEXYZ_K


# ----- CIEXYZ <-> CIELUV

def xyz_to_luv(x, y, z, ref='d50'):
    """Convert XYZ to CIELUV."""
    # reference white point
    ref_x, ref_y, ref_z = getattr(cc, 'CIEXYZ_O2_{}_REF'.format(ref.upper()))

    try:
        u = (4 * x) / (x + (15 * y) + (3 * z))
        v = (9 * y) / (x + (15 * y) + (3 * z))
    except ZeroDivisionError:
        # black causes ZeroDivisionError
        u = v = 0.0

    y = _func_from_xyz(y)

    ref_u = (4 * ref_x) / (ref_x + (15 * ref_y) + (3 * ref_z))
    ref_v = (9 * ref_y) / (ref_x + (15 * ref_y) + (3 * ref_z))

    cie_l = (116.0 * y) - 16
    cie_u = 13 * cie_l * (u - ref_u)
    cie_v = 13 * cie_l * (v - ref_v)

    return [cie_l, cie_u, cie_v]


def luv_to_xyz(l, u, v, ref='d50'):
    """Convert CIELUV to XYZ."""
    # reference white point
    ref_x, ref_y, ref_z = getattr(cc, 'CIEXYZ_O2_{}_REF'.format(ref.upper()))

    if l <= 0.0:
        # black (L = 0): u, v are undefined
        cie_x = cie_y = cie_z = 0.0
        return [cie_x, cie_y, cie_z]

    if l > cc.CIEXYZ_K * cc.CIEXYZ_E:
        y = math.pow((l + 16) / 116.0, 3)
    else:
        y = l / cc.CIEXYZ_K

    ref_u = (4 * ref_x) / (ref_x + (15 * ref_y) + (3 * ref_z))
    ref_v = (9 * ref_y) / (ref_x + (15 * ref_y) + (3 * ref_z))

    u = u / (13 * l) + ref_u
    v = v / (13 * l) + ref_v

    cie_y = y
    cie_x = - (9 * cie_y * u) / (((u - 4) * v) - (u * v))
    cie_z = ((9 * cie_y) - (15 * v * cie_y) - (v * cie_x)) / (3 * v)

    return [cie_x, cie_y, cie_z]


# ----- CIEXYZ <-> CIELAB

def xyz_to_lab(x, y, z, ref='d50'):
    """Convert whitepoint-adapated XYZ to CIELAB."""
    # reference white point
    ref_x, ref_y, ref_z = getattr(cc, 'CIEXYZ_O2_{}_REF'.format(ref.upper()))

    x = _func_from_xyz(x / ref_x)
    y = _func_from_xyz(y / ref_y)
    z = _func_from_xyz(z / ref_z)

    cie_l = (116.0 * y) - 16.0
    cie_a = 500 * (x - y)
    cie_b = 200 * (y - z)

    return [cie_l, cie_a, cie_b]


def lab_to_xyz(l, a, b, ref='d50'):
    """Convert CIELAB to whitepoint-adapted XYZ."""
    # reference white point
    ref_x, ref_y, ref_z = getattr(cc, 'CIEXYZ_O2_{}_REF'.format(ref.upper()))

    y = (l + 16.0) / 116.0
    x = (a / 500.0) + y
    z = (y - (b / 200.0))

    cie_x = _func_to_xyz(x) * ref_x
    if l > cc.CIEXYZ_K * cc.CIEXYZ_E:
        cie_y = math.pow((l + 16) / 116.0, 3) * ref_y
    else:
        cie_y = (l / cc.CIEXYZ_K) * ref_y
    cie_z = _func_to_xyz(z) * ref_z

    return [cie_x, cie_y, cie_z]


# ----- LAB <-> LCH

def lab_to_lch(l, a, b):
    """Convert CIELAB to LCH."""
    # Based on:
    # https://github.com/gka/chroma.js
    cie_l = l
    cie_c = math.sqrt(math.pow(a, 2) + math.pow(b, 2))
    cie_h = (math.degrees(math.atan2(b, a)) + 360) % 360
    return [cie_l, cie_c, cie_h]


def lch_to_lab(l, c, h):
    """Convert LCH to CIELAB."""
    # Based on
    # https://github.com/gka/chroma.js
    cie_l = l
    cie_a = math.cos(math.radians(h)) * c
    cie_b = math.sin(math.radians(h)) * c
    return [cie_l, cie_a, cie_b]


# ----- RGB <-> YUV

def rgb2yuv(rgb_matrix, mode='rec601'):
    """Convert RGB to YUV, return color matrix."""

    # analog PAL and SECAM composite color video

    # Y [0 to 1.0]
    # U [-0.436 to 0.436]
    # V [-0.615 to 0.615]

    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    if mode == 'rec601':
        mat[0][0] = cc.LUMA_REC601_R
        mat[0][1] = cc.LUMA_REC601_G
        mat[0][2] = cc.LUMA_REC601_B
        mat[1][0] = -0.14713
        mat[1][1] = -0.28886
        mat[1][2] = 0.436
        mat[2][0] = 0.615
        mat[2][1] = -0.51499
        mat[2][2] = -0.10001
    elif mode == 'rec709':
        mat[0][0] = cc.LUMA_REC709_R
        mat[0][1] = cc.LUMA_REC709_G
        mat[0][2] = cc.LUMA_REC709_B
        mat[1][0] = -0.09991
        mat[1][1] = -0.33609
        mat[1][2] = 0.436
        mat[2][0] = 0.615
        mat[2][1] = -0.55861
        mat[2][2] = -0.05639

    # multiply the two matrices, return matrix
    return cm.matrixmult(mat, rgb_matrix)


def yuv2rgb(rgb_matrix, mode='rec601'):
    """Convert YUV to RGB, return color matrix."""

    # analog PAL and SECAM composite color video

    # Y [0 to 1.0]
    # U [-0.436 to 0.436]
    # V [-0.615 to 0.615]

    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    if mode == 'rec601':
        mat[0][0] = 1.0
        mat[0][1] = 0.0
        mat[0][2] = 1.13983
        mat[1][0] = 1.0
        mat[1][1] = -0.39465
        mat[1][2] = -0.58060
        mat[2][0] = 1.0
        mat[2][1] = 2.03211
        mat[2][2] = 0.0
    elif mode == 'rec709':
        mat[0][0] = 1.0
        mat[0][1] = 0.0
        mat[0][2] = 1.28033
        mat[1][0] = 1.0
        mat[1][1] = -0.21482
        mat[1][2] = -0.38059
        mat[2][0] = 1.0
        mat[2][1] = 2.12798
        mat[2][2] = 0.0

    # multiply the two matrices, return matrix
    return cm.matrixmult(mat, rgb_matrix)


# ----- RGB <-> YIQ

def rgb2yiq(rgb_matrix):
    """Convert RGB to YIQ, return color matrix."""

    # analog NTSC video

    # Y [0 to 1.0]
    # I [-0.595716 to 0.595716]
    # Q [-0.522591 to 0.522591]

    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    mat[0][0] = cc.LUMA_REC601_R
    mat[0][1] = cc.LUMA_REC601_G
    mat[0][2] = cc.LUMA_REC601_B
    mat[1][0] = 0.595716
    mat[1][1] = -0.274453
    mat[1][2] = -0.321263
    mat[2][0] = 0.211456
    mat[2][1] = -0.522591
    mat[2][2] = 0.311135

    # multiply the two matrices, return matrix
    return cm.matrixmult(mat, rgb_matrix)


def yiq2rgb(yiq_matrix):
    """Convert YIQ to RGB, return color matrix."""

    # analog NTSC video

    # Y [0 to 1.0]
    # I [-0.5957 to 0.5957]
    # Q [-0.5226 to 0.5226]

    mat = [[0.0 for _ in range(5)] for _ in range(5)]
    for i in (3, 4):
        mat[i][i] = 1.0

    mat[0][0] = 1.0
    mat[0][1] = 0.95568806036115671171
    mat[0][2] = 0.61985809445637075388
    mat[1][0] = 1.0
    mat[1][1] = -0.27158179694405859326
    mat[1][2] = -0.64687381613840131330
    mat[2][0] = 1.0
    mat[2][1] = -1.1081773266826619523
    mat[2][2] = 1.7050645599191817149

    # multiply the two matrices, return matrix
    return cm.matrixmult(mat, yiq_matrix)


# ----- Wrappers

# RGB <- CIEXYZ -> CIELUV

def rgb_to_luv(r, g, b, ref='d50'):
    """Convert RGB to CIELAB."""
    if ref == 'd50':
        return xyz_to_luv(*d65_to_d50(*rgb_to_xyz(r, g, b)), ref=ref)
    else:  # if ref == 'd65':
        ref = 'd65'
        return xyz_to_luv(*rgb_to_xyz(r, g, b), ref=ref)


def luv_to_rgb(l, u, v, ref='d50'):
    """Convert RGB to CIELAB."""
    if ref == 'd50':
        return xyz_to_rgb(*d50_to_d65(*luv_to_xyz(l, u, v, ref)))
    else:  # if ref == 'd65':
        ref = 'd65'
        return xyz_to_rgb(*luv_to_xyz(l, u, v, ref))


# RGB <- CIEXYZ -> CIELAB

def rgb_to_lab(r, g, b, ref='d50'):
    """Convert RGB to CIELAB."""
    if ref == 'd50':
        return xyz_to_lab(*d65_to_d50(*rgb_to_xyz(r, g, b)), ref=ref)
    else:  # if ref == 'd65':
        ref = 'd65'
        return xyz_to_lab(*rgb_to_xyz(r, g, b), ref=ref)


def lab_to_rgb(l, a, b, ref='d50'):
    """Convert RGB to CIELAB."""
    if ref == 'd50':
        return xyz_to_rgb(*d50_to_d65(*lab_to_xyz(l, a, b, ref)))
    else:  # if ref == 'd65':
        ref = 'd65'
        return xyz_to_rgb(*lab_to_xyz(l, a, b, ref))


# RGB <- CIELAB -> LCH

def rgb_to_lch(r, g, b, ref='d50'):
    """Convert RGB to LCH."""
    return lab_to_lch(*rgb_to_lab(r, g, b, ref=ref))


def lch_to_rgb(l, c, h, ref='d50'):
    """Convert LCH to RGB."""
    return lab_to_rgb(*lch_to_lab(l, c, h), ref=ref)


# RGB <- RGB2YUV -> YUV

def rgb_to_yuv(r, g, b, mode='rec601'):
    """Convert RGB to YUV."""
    return cm.matrix_to_list(rgb2yuv(cm.rgb2matrix(r, g, b), mode))[:3]


def yuv_to_rgb(y, u, v, mode='rec601'):
    """Convert YUV to RGB."""
    return cm.matrix2rgb(yuv2rgb(cm.list_to_matrix(y, u, v), mode))[:3]


# RGB <- RGB2YIQ -> YIQ

def rgb_to_yiq(r, g, b):
    """Convert RGB *r*, *g*, *b* to YIQ (y, i, q)."""
    return cm.matrix_to_list(rgb2yiq(cm.rgb2matrix(r, g, b)))[:3]


def yiq_to_rgb(y, i, q):
    """Convert YIQ *y*, *i*, *q* to RGB (r, g, b)."""
    return cm.matrix2rgb(yiq2rgb(cm.list_to_matrix(y, i, q)))[:3]


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
