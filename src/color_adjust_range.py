#!/usr/bin/env python
"""
color_adjust_range - adjust similar colors (within threshold) or
                     colors within ranges defined for color components

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colordiff as cd
from color_lib import colorblend as cb
from color_lib import colorops as co
from color_lib.util import randomize_float


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class ColorAdjustRange(modcol.ColorRGBAO):
    """ColorEffect-based class to adjust components of similar colors."""

    def __init__(self):
        """Init base class and ColorAdjustRange()."""
        # pylint: disable=too-many-statements
        modcol.ColorRGBAO.__init__(self)

        # instance attributes
        self.match = False

        # Range: common options
        self.OptionParser.add_option("--exclude_match",
                                     action="store", type="inkbool",
                                     dest="exclude_match", default=True,
                                     help="Exclude matching colors")
        # Range: RGBA
        self.OptionParser.add_option("--range_rgb_r_min",
                                     action="store", type="int",
                                     dest="range_rgb_r_min", default=0,
                                     help="RGB range: min red")
        self.OptionParser.add_option("--range_rgb_r_max",
                                     action="store", type="int",
                                     dest="range_rgb_r_max", default=255,
                                     help="RGB range: max red")
        self.OptionParser.add_option("--range_rgb_g_min",
                                     action="store", type="int",
                                     dest="range_rgb_g_min", default=0,
                                     help="RGB range: min green")
        self.OptionParser.add_option("--range_rgb_g_max",
                                     action="store", type="int",
                                     dest="range_rgb_g_max", default=255,
                                     help="RGB range: max green")
        self.OptionParser.add_option("--range_rgb_b_min",
                                     action="store", type="int",
                                     dest="range_rgb_b_min", default=0,
                                     help="RGB range: min blue")
        self.OptionParser.add_option("--range_rgb_b_max",
                                     action="store", type="int",
                                     dest="range_rgb_b_max", default=255,
                                     help="RGB range: max blue")
        self.OptionParser.add_option("--range_rgb_a_min",
                                     action="store", type="int",
                                     dest="range_rgb_a_min", default=0,
                                     help="RGB range: min alpha")
        self.OptionParser.add_option("--range_rgb_a_max",
                                     action="store", type="int",
                                     dest="range_rgb_a_max", default=100,
                                     help="RGB range: max alpha")
        # Range: HSLA
        self.OptionParser.add_option("--range_hsl_h_min",
                                     action="store", type="int",
                                     dest="range_hsl_h_min", default=0,
                                     help="HSL range: min hue")
        self.OptionParser.add_option("--range_hsl_h_max",
                                     action="store", type="int",
                                     dest="range_hsl_h_max", default=360,
                                     help="HSL range: max hue")
        self.OptionParser.add_option("--range_hsl_s_min",
                                     action="store", type="int",
                                     dest="range_hsl_s_min", default=0,
                                     help="HSL range: min saturation")
        self.OptionParser.add_option("--range_hsl_s_max",
                                     action="store", type="int",
                                     dest="range_hsl_s_max", default=100,
                                     help="HSL range: max saturation")
        self.OptionParser.add_option("--range_hsl_l_min",
                                     action="store", type="int",
                                     dest="range_hsl_l_min", default=0,
                                     help="HSL range: min lightness")
        self.OptionParser.add_option("--range_hsl_l_max",
                                     action="store", type="int",
                                     dest="range_hsl_l_max", default=100,
                                     help="HSL range: max lightness")
        self.OptionParser.add_option("--range_hsl_a_min",
                                     action="store", type="int",
                                     dest="range_hsl_a_min", default=0,
                                     help="HSL range: min alpha")
        self.OptionParser.add_option("--range_hsl_a_max",
                                     action="store", type="int",
                                     dest="range_hsl_a_max", default=100,
                                     help="HSL range: max alpha")
        # Range: HSVA
        self.OptionParser.add_option("--range_hsv_h_min",
                                     action="store", type="int",
                                     dest="range_hsv_h_min", default=0,
                                     help="HSV range: min hue")
        self.OptionParser.add_option("--range_hsv_h_max",
                                     action="store", type="int",
                                     dest="range_hsv_h_max", default=360,
                                     help="HSV range: max hue")
        self.OptionParser.add_option("--range_hsv_s_min",
                                     action="store", type="int",
                                     dest="range_hsv_s_min", default=0,
                                     help="HSV range: min saturation")
        self.OptionParser.add_option("--range_hsv_s_max",
                                     action="store", type="int",
                                     dest="range_hsv_s_max", default=100,
                                     help="HSV range: max saturation")
        self.OptionParser.add_option("--range_hsv_v_min",
                                     action="store", type="int",
                                     dest="range_hsv_v_min", default=0,
                                     help="HSV range: min value")
        self.OptionParser.add_option("--range_hsv_v_max",
                                     action="store", type="int",
                                     dest="range_hsv_v_max", default=100,
                                     help="HSV range: max value")
        self.OptionParser.add_option("--range_hsv_a_min",
                                     action="store", type="int",
                                     dest="range_hsv_a_min", default=0,
                                     help="HSV range: min alpha")
        self.OptionParser.add_option("--range_hsv_a_max",
                                     action="store", type="int",
                                     dest="range_hsv_a_max", default=100,
                                     help="HSV range: max alpha")
        # Range: threshold
        self.OptionParser.add_option("--source_color",
                                     action="store", type="int",
                                     dest="source_color", default="-1",
                                     help="Source color")
        self.OptionParser.add_option("--threshold",
                                     action="store", type="float",
                                     dest="threshold", default=0.0,
                                     help="Threshold (for source color)")
        self.OptionParser.add_option("--match_alpha",
                                     action="store", type="inkbool",
                                     dest="match_alpha", default=True,
                                     help="Match including alpha")
        # Adjust RGBA
        self.OptionParser.add_option("--rgb_r",
                                     action="store", type="int",
                                     dest="rgb_r", default=0,
                                     help="Adjust RGB red value")
        self.OptionParser.add_option("--rgb_g",
                                     action="store", type="int",
                                     dest="rgb_g", default=0,
                                     help="Adjust RGB green value")
        self.OptionParser.add_option("--rgb_b",
                                     action="store", type="int",
                                     dest="rgb_b", default=0,
                                     help="Adjust RGB blue value")
        self.OptionParser.add_option("--rgb_a",
                                     action="store", type="int",
                                     dest="rgb_a", default=0,
                                     help="Adjust RGB alpha value")
        # RGBA absolute switch
        self.OptionParser.add_option("--rgb_r_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_r_abs", default=False,
                                     help="Use absolute RGB red value")
        self.OptionParser.add_option("--rgb_g_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_g_abs", default=False,
                                     help="Use absolute RGB green value")
        self.OptionParser.add_option("--rgb_b_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_b_abs", default=False,
                                     help="Use absolute RGB blue value")
        self.OptionParser.add_option("--rgb_alpha_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_alpha_abs", default=False,
                                     help="Use absolute alpha value")
        # Adjust HSLA
        self.OptionParser.add_option("--hsl_h",
                                     action="store", type="int",
                                     dest="hsl_h", default="0",
                                     help="Adjust HSL hue")
        self.OptionParser.add_option("--hsl_l",
                                     action="store", type="int",
                                     dest="hsl_l", default=0,
                                     help="Adjust HSL lightness")
        self.OptionParser.add_option("--hsl_s",
                                     action="store", type="int",
                                     dest="hsl_s", default=0,
                                     help="Adjust HSL saturation")
        self.OptionParser.add_option("--hsl_a",
                                     action="store", type="int",
                                     dest="hsl_a", default=0,
                                     help="Adjust HSL alpha")
        # HSLA absolute switch
        self.OptionParser.add_option("--hsl_h_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_h_abs", default=False,
                                     help="Use absolute HSL hue value")
        self.OptionParser.add_option("--hsl_s_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_s_abs", default=False,
                                     help="Use absolute HSL saturation value")
        self.OptionParser.add_option("--hsl_l_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_l_abs", default=False,
                                     help="Use absolute HSL lightness value")
        self.OptionParser.add_option("--hsl_alpha_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_alpha_abs", default=False,
                                     help="Use absolute alpha value")
        # Adjust HSVA
        self.OptionParser.add_option("--hsv_h",
                                     action="store", type="int",
                                     dest="hsv_h", default=0,
                                     help="Adjust HSV hue")
        self.OptionParser.add_option("--hsv_s",
                                     action="store", type="int",
                                     dest="hsv_s", default=0,
                                     help="Adjust HSV saturation")
        self.OptionParser.add_option("--hsv_v",
                                     action="store", type="int",
                                     dest="hsv_v", default="0",
                                     help="Adjust HSV value")
        self.OptionParser.add_option("--hsv_a",
                                     action="store", type="int",
                                     dest="hsv_a", default="0",
                                     help="Adjust HSV alpha")
        # HSVA absolute switch
        self.OptionParser.add_option("--hsv_h_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_h_abs", default=False,
                                     help="Use absolute HSL hue value")
        self.OptionParser.add_option("--hsv_s_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_s_abs", default=False,
                                     help="Use absolute HSL saturation value")
        self.OptionParser.add_option("--hsv_v_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_v_abs", default=False,
                                     help="Use absolute HSL lightness value")
        self.OptionParser.add_option("--hsv_alpha_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_alpha_abs", default=False,
                                     help="Use absolute alpha value")
        # Adjust Blend
        self.OptionParser.add_option("--blend_mode",
                                     action="store", type="string",
                                     dest="blend_mode", default="blend_normal",
                                     help="Blend mode")
        self.OptionParser.add_option("--blend_order",
                                     action="store", type="string",
                                     dest="blend_order",
                                     default="custom_over_current",
                                     help="Blend order")
        self.OptionParser.add_option("--blend_colorspace",
                                     action="store", type="string",
                                     dest="blend_colorspace", default="sRGB",
                                     help="Blend color space")
        self.OptionParser.add_option("--blend_alpha",
                                     action="store", type="inkbool",
                                     dest="blend_alpha", default=True,
                                     help="Update blended alpha")
        self.OptionParser.add_option("--blend_color",
                                     action="store", type="int",
                                     dest="blend_color", default=-16777088,
                                     help="Source blend color")
        # Adjust Randomize
        self.OptionParser.add_option("--random_h",
                                     action="store", type="int",
                                     dest="random_h", default=0,
                                     help="Random Hue range")
        self.OptionParser.add_option("--random_s",
                                     action="store", type="int",
                                     dest="random_s", default=0,
                                     help="Random Saturation range")
        self.OptionParser.add_option("--random_l",
                                     action="store", type="int",
                                     dest="random_l", default=0,
                                     help="Random Lightness range")
        self.OptionParser.add_option("--random_a",
                                     action="store", type="int",
                                     dest="random_a", default=0,
                                     help="Random Alpha range")
        # Adjust greyscale
        self.OptionParser.add_option("--greyscale_preset",
                                     action="store", type="string",
                                     dest="greyscale_preset",
                                     default="luma_rec601",
                                     help="Greyscale preset")
        self.OptionParser.add_option("--greyscale_rgb_posterize_count",
                                     action="store", type="int",
                                     dest="greyscale_rgb_posterize_count",
                                     default=5,
                                     help="Greyscale RGB posterize levels")
        # Adjust Back and White
        self.OptionParser.add_option("--blackwhite_threshold",
                                     action="store", type="int",
                                     dest="blackwhite_threshold",
                                     default=50,
                                     help="Black-White threshold")
        self.OptionParser.add_option("--blackwhite_grey_mode",
                                     action="store", type="string",
                                     dest="blackwhite_grey_mode",
                                     default="luma_rec601",
                                     help="Black-White greyscale mode")
        # notebooks
        self.OptionParser.add_option("--range",
                                     action="store", type="string",
                                     dest="range",
                                     help="The selected range tab")
        self.OptionParser.add_option("--adjust",
                                     action="store", type="string",
                                     dest="adjust",
                                     help="The selected adjust tab")

    def match_rgb_range(self, ink_color):
        """Match current color to RGBA range."""
        match_c = (ink_color.red >= self.options.range_rgb_r_min and
                   ink_color.red <= self.options.range_rgb_r_max and
                   ink_color.green >= self.options.range_rgb_g_min and
                   ink_color.green <= self.options.range_rgb_g_max and
                   ink_color.blue >= self.options.range_rgb_b_min and
                   ink_color.blue <= self.options.range_rgb_b_max)
        match_a = (ink_color.alpha >= self.options.range_rgb_a_min / 100.0 and
                   ink_color.alpha <= self.options.range_rgb_a_max / 100.0)
        return match_c and match_a

    def match_hsl_range(self, ink_color):
        """Match current color to HSLA range."""
        match_c = (ink_color.hsl_h_deg >= self.options.range_hsl_h_min and
                   ink_color.hsl_h_deg <= self.options.range_hsl_h_max and
                   ink_color.hsl_s >= self.options.range_hsl_s_min / 100.0 and
                   ink_color.hsl_s <= self.options.range_hsl_s_max / 100.0 and
                   ink_color.hsl_l >= self.options.range_hsl_l_min / 100.0 and
                   ink_color.hsl_l <= self.options.range_hsl_l_max / 100.0)
        match_a = (ink_color.alpha >= self.options.range_hsl_a_min / 100.0 and
                   ink_color.alpha <= self.options.range_hsl_a_max / 100.0)
        return match_c and match_a

    def match_hsv_range(self, ink_color):
        """Match current color to HSVA range."""
        match_c = (ink_color.hsv_h_deg >= self.options.range_hsv_h_min and
                   ink_color.hsv_h_deg <= self.options.range_hsv_h_max and
                   ink_color.hsv_s >= self.options.range_hsv_s_min / 100.0 and
                   ink_color.hsv_s <= self.options.range_hsv_s_max / 100.0 and
                   ink_color.hsv_v >= self.options.range_hsv_v_min / 100.0 and
                   ink_color.hsv_v <= self.options.range_hsv_v_max / 100.0)
        match_a = (ink_color.alpha >= self.options.range_hsv_a_min / 100.0 and
                   ink_color.alpha <= self.options.range_hsv_a_max / 100.0)
        return match_c and match_a

    def match_threshold(self, ink_color):
        """Match two InkColor objects based on threshold."""
        source_color = ic.InkColorRGBALong(self.options.source_color)
        match = False
        if self.options.threshold == 0.0:
            # Compare exact match without or with alpha
            if not self.options.match_alpha:
                if ink_color.rgb_int() == source_color.rgb_int():
                    match = True
            else:
                if ink_color == source_color:
                    match = True
        elif self.options.threshold > 0:
            # Compare source color with threshold, alpha separately
            delta = {}
            delta['c'] = cd.color_diff(source_color, ink_color)
            delta['a'] = abs(source_color.alpha - ink_color.alpha) * 100
            if delta['c'] <= self.options.threshold:
                if not self.options.match_alpha:
                    match = True
                elif delta['a'] <= self.options.threshold:
                    match = True
        return match

    def match_color(self, ink_color):
        """Match current color to range, threshold."""
        # dispatcher for Range tabs
        if self.options.range == '"range_rgb_tab"':
            self.match = self.match_rgb_range(ink_color)
        elif self.options.range == '"range_hsl_tab"':
            self.match = self.match_hsl_range(ink_color)
        elif self.options.range == '"range_hsv_tab"':
            self.match = self.match_hsv_range(ink_color)
        elif self.options.range == '"range_threshold_tab"':
            self.match = self.match_threshold(ink_color)
        # include or exclude matching colors
        if self.options.exclude_match:
            self.match = not self.match

    def adjust_rgb(self, ink_color):
        """Adjust each color channel of RGB color representation."""
        rgb_r = self.options.rgb_r
        rgb_g = self.options.rgb_g
        rgb_b = self.options.rgb_b
        rgb_a = self.options.rgb_a / 100.0
        r, g, b = ink_color.rgb_int()
        a = ink_color.alpha
        # color
        r = abs(rgb_r) if self.options.rgb_r_abs else r + rgb_r
        g = abs(rgb_g) if self.options.rgb_g_abs else g + rgb_g
        b = abs(rgb_b) if self.options.rgb_b_abs else b + rgb_b
        # alpha
        if not self.options.object_opacity:
            a = abs(rgb_a) if self.options.rgb_alpha_abs else a + rgb_a
        # update ink_color
        ink_color.from_int(r, g, b, a)

    def adjust_hsl(self, ink_color):
        """Adjust RGB color based on HSL color representation."""
        hsl_h = self.options.hsl_h / 360.0
        hsl_s = self.options.hsl_s / 100.0
        hsl_l = self.options.hsl_l / 100.0
        hsl_a = self.options.hsl_a / 100.0
        h, s, l, a = ink_color.rgba_to_hsla()
        # color
        h = abs(hsl_h) if self.options.hsl_h_abs else h + hsl_h
        s = abs(hsl_s) if self.options.hsl_s_abs else s + hsl_s
        l = abs(hsl_l) if self.options.hsl_l_abs else l + hsl_l
        # alpha
        if not self.options.object_opacity:
            a = abs(hsl_a) if self.options.hsl_alpha_abs else a + hsl_a
        # update ink_color
        ink_color.from_hsla(h, s, l, a)

    def adjust_hsv(self, ink_color):
        """Adjust RGB color based on HSV color representation."""
        # parameters
        hsv_h = self.options.hsv_h / 360.0
        hsv_s = self.options.hsv_s / 100.0
        hsv_v = self.options.hsv_v / 100.0
        hsv_a = self.options.hsv_a / 100.0
        h, s, v, a = ink_color.rgba_to_hsva()
        # color
        h = abs(hsv_h) if self.options.hsv_h_abs else h + hsv_h
        s = abs(hsv_s) if self.options.hsv_s_abs else s + hsv_s
        v = abs(hsv_v) if self.options.hsv_v_abs else v + hsv_v
        # alpha
        if not self.options.object_opacity:
            a = abs(hsv_a) if self.options.hsv_alpha_abs else a + hsv_a
        # update ink_color
        ink_color.from_hsva(h, s, v, a)

    def adjust_blend(self, ink_color):
        """Blend current color with custom color."""
        # parameters
        mode = self.options.blend_mode
        source = self.options.blend_color
        order = self.options.blend_order
        linear = self.options.blend_colorspace == 'linearRGB'
        alpha = self.options.blend_alpha
        # prepare color, funcs
        blend_color = ic.InkColorRGBALong(source)
        blend = getattr(cb, mode[:mode.rfind('_')], None)
        func = getattr(cb, mode, None)
        # blend custom with current color
        if blend is not None and func is not None:
            if order == 'custom_over_current':
                ic_out = blend(ink_color, blend_color, func, linear)
            else:
                ic_out = blend(blend_color, ink_color, func, linear)
        else:
            ic_out = ink_color
        # update ink_color
        if alpha:
            # TODO: support self.options.object_opacity for blended alpha?
            ink_color.from_float(*ic_out.rgba_float())
        else:
            ink_color.from_float(*ic_out.rgb_float())

    def adjust_random(self, ink_color):
        """Randomize color and alpha components of InkColor."""
        h_range = self.options.random_h
        s_range = self.options.random_s
        l_range = self.options.random_l
        a_range = self.options.random_a
        if h_range or s_range or l_range or a_range:
            h, s, l, a = ink_color.rgba_to_hsla()
            if h_range > 0:
                h = randomize_float(h_range, h)
            if s_range > 0:
                s = randomize_float(s_range, s)
            if l_range > 0:
                l = randomize_float(l_range, l)
            if a_range > 0:
                if not self.options.object_opacity:
                    a = randomize_float(a_range, a)
            ink_color.from_hsla(h, s, l, a)

    def greyscale_preset_rgb(self, ink_color, preset):
        """Check and apply greyscale preset based on RGB channels."""
        new_color = None
        if preset.startswith('lightness'):
            new_color = co.greyscale_rgb_lightness(ink_color)
        elif preset.startswith('average'):
            new_color = co.greyscale_rgb_average(ink_color)
        elif preset.startswith('decompose'):
            if preset.endswith('max'):
                new_color = co.greyscale_rgb_decompose(ink_color, 'max')
            elif preset.endswith('min'):
                new_color = co.greyscale_rgb_decompose(ink_color, 'min')
        elif preset.startswith('channel'):
            if preset.endswith('red'):
                new_color = co.greyscale_rgb_channel(ink_color, 'r')
            elif preset.endswith('green'):
                new_color = co.greyscale_rgb_channel(ink_color, 'g')
            elif preset.endswith('blue'):
                new_color = co.greyscale_rgb_channel(ink_color, 'b')
        elif preset.startswith('posterize'):
            count = self.options.greyscale_rgb_posterize_count
            new_color = co.greyscale_rgb_posterize(ink_color, count)
        return new_color

    def adjust_greyscale(self, ink_color):
        """Adjust InkColor in range to selected greyscale preset."""
        new_color = None
        preset = self.options.greyscale_preset
        if preset.startswith('rgb_'):
            new_color = self.greyscale_preset_rgb(ink_color, preset[4:])
        elif preset.startswith('luma_'):
            new_color = co.greyscale_luma(ink_color, preset[5:])
        elif preset.startswith('luminance_'):
            new_color = co.greyscale_luminance(ink_color, preset[10:])
        elif preset == 'blend_luminosity':
            new_color = co.greyscale_blend_luminosity(ink_color)
        elif preset == 'hsl_lightness':
            new_color = co.greyscale_hsl_lightness(ink_color)
        elif preset == 'hsv_value':
            new_color = co.greyscale_hsv_value(ink_color)
        elif preset == 'lab_lightness':
            new_color = co.greyscale_lab_lightness(ink_color)
        elif preset == 'hsp_brightness':
            new_color = co.greyscale_hsp_brightness(ink_color)
        # update ink_color
        if new_color is not None:
            ink_color.from_float(*new_color.rgb_float())

    def adjust_blackwhite(self, ink_color):
        """Adjust InkColor in range to B&W based on threshold."""
        # parameters
        threshold = self.options.blackwhite_threshold / 100.0
        greymode = self.options.blackwhite_grey_mode
        # luma, luminance, or fallback
        if greymode.startswith('luma'):
            lum = co.luma(*ink_color.rgb_float(), mode=greymode[5:])
        elif greymode.startswith('luminance'):
            lum = co.luminance(*ink_color.rgb_linear())
        else:  # fall back to RGB lightness
            r, g, b = ink_color.rgb_float()
            lum = (r + g + b) / 3.0
        # Compare luminance to threshold
        result = 1.0 if lum > threshold else 0.0
        # Update ink_color
        ink_color.from_float(result, result, result)

    def adjust_color(self, ink_color):
        """Adjust current color *ink_color*."""
        # dispatcher for Adjust tabs
        if self.options.adjust == '"rgb_tab"':
            self.adjust_rgb(ink_color)
        elif self.options.adjust == '"hsl_tab"':
            self.adjust_hsl(ink_color)
        elif self.options.adjust == '"hsv_tab"':
            self.adjust_hsv(ink_color)
        elif self.options.adjust == '"blend_tab"':
            self.adjust_blend(ink_color)
        elif self.options.adjust == '"random_tab"':
            self.adjust_random(ink_color)
        elif self.options.adjust == '"greyscale_tab"':
            self.adjust_greyscale(ink_color)
        elif self.options.adjust == '"blackwhite_tab"':
            self.adjust_blackwhite(ink_color)

    def adjust_object_opacity(self, opacity):
        """Adjust object *opacity* based on alpha adjust options."""
        object_o = float(opacity)
        if self.options.adjust == '"rgb_tab"':
            if self.options.rgb_alpha_abs:
                object_o = abs(self.options.rgb_a) / 100.0
            else:
                object_o += self.options.rgb_a / 100.0
        elif self.options.adjust == '"hsl_tab"':
            if self.options.hsl_alpha_abs:
                object_o = abs(self.options.hsl_a) / 100.0
            else:
                object_o += self.options.hsl_a / 100.0
        elif self.options.adjust == '"hsv_tab"':
            if self.options.hsv_alpha_abs:
                object_o = abs(self.options.hsv_a) / 100.0
            else:
                object_o += self.options.hsv_a / 100.0
        elif self.options.adjust == '"blend_tab"':
            # TODO: Blended alpha may differ for fill and stroke paint.
            # Extensions based on color_lib only know about the color,
            # not the paint itself: what criteria allow to choose one
            # computed blended alpha value for object opacity, and how?
            pass
        elif self.options.adjust == '"random_tab"':
            o_range = self.options.random_a
            if o_range and o_range > 0:
                return randomize_float(o_range, object_o)
        # update opacity
        return object_o

    def modify_opacity(self, opacity):
        """Modify object *opacity*."""
        # modify object opacity if match
        if self.match:
            if self.options.object_opacity:
                return self.adjust_object_opacity(opacity)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        # match current color with color range
        self.match_color(ink_color)
        # modify color if match
        if self.match:
            self.adjust_color(ink_color)


if __name__ == '__main__':
    ME = ColorAdjustRange()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
