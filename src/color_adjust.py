#!/usr/bin/env python
"""
color_adjust - adjust RGB, HSL, HSV or HLS of SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


class ColorAdjust(modcol.ColorRGBAO):
    """ColorEffect-based class to adjust color components of SVG paints."""

    def __init__(self):
        """Init base class and ColorAdjust()."""
        modcol.ColorRGBAO.__init__(self)

        # RGBA
        self.OptionParser.add_option("--rgb_r",
                                     action="store", type="int",
                                     dest="rgb_r", default=0,
                                     help="Adjust RGB red value")
        self.OptionParser.add_option("--rgb_g",
                                     action="store", type="int",
                                     dest="rgb_g", default=0,
                                     help="Adjust RGB green value")
        self.OptionParser.add_option("--rgb_b",
                                     action="store", type="int",
                                     dest="rgb_b", default=0,
                                     help="Adjust RGB blue value")
        self.OptionParser.add_option("--rgb_a",
                                     action="store", type="int",
                                     dest="rgb_a", default=0,
                                     help="Adjust RGB alpha value")
        # RGBA absolute switch
        self.OptionParser.add_option("--rgb_r_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_r_abs", default=False,
                                     help="Use absolute RGB red value")
        self.OptionParser.add_option("--rgb_g_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_g_abs", default=False,
                                     help="Use absolute RGB green value")
        self.OptionParser.add_option("--rgb_b_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_b_abs", default=False,
                                     help="Use absolute RGB blue value")
        self.OptionParser.add_option("--rgb_a_abs",
                                     action="store", type="inkbool",
                                     dest="rgb_a_abs", default=False,
                                     help="Use absolute alpha value")
        # HSLA
        self.OptionParser.add_option("--hsl_h",
                                     action="store", type="int",
                                     dest="hsl_h", default="0",
                                     help="Adjust HSL hue")
        self.OptionParser.add_option("--hsl_l",
                                     action="store", type="int",
                                     dest="hsl_l", default=0,
                                     help="Adjust HSL lightness")
        self.OptionParser.add_option("--hsl_s",
                                     action="store", type="int",
                                     dest="hsl_s", default=0,
                                     help="Adjust HSL saturation")
        self.OptionParser.add_option("--hsl_a",
                                     action="store", type="int",
                                     dest="hsl_a", default=0,
                                     help="Adjust HSL alpha")
        # HSLA absolute switch
        self.OptionParser.add_option("--hsl_h_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_h_abs", default=False,
                                     help="Use absolute HSL hue value")
        self.OptionParser.add_option("--hsl_s_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_s_abs", default=False,
                                     help="Use absolute HSL saturation value")
        self.OptionParser.add_option("--hsl_l_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_l_abs", default=False,
                                     help="Use absolute HSL lightness value")
        self.OptionParser.add_option("--hsl_a_abs",
                                     action="store", type="inkbool",
                                     dest="hsl_a_abs", default=False,
                                     help="Use absolute alpha value")
        # HSVA
        self.OptionParser.add_option("--hsv_h",
                                     action="store", type="int",
                                     dest="hsv_h", default=0,
                                     help="Adjust HSV hue")
        self.OptionParser.add_option("--hsv_s",
                                     action="store", type="int",
                                     dest="hsv_s", default=0,
                                     help="Adjust HSV saturation")
        self.OptionParser.add_option("--hsv_v",
                                     action="store", type="int",
                                     dest="hsv_v", default="0",
                                     help="Adjust HSV value")
        self.OptionParser.add_option("--hsv_a",
                                     action="store", type="int",
                                     dest="hsv_a", default="0",
                                     help="Adjust HSV alpha")
        # HSVA absolute switch
        self.OptionParser.add_option("--hsv_h_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_h_abs", default=False,
                                     help="Use absolute HSL hue value")
        self.OptionParser.add_option("--hsv_s_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_s_abs", default=False,
                                     help="Use absolute HSL saturation value")
        self.OptionParser.add_option("--hsv_v_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_v_abs", default=False,
                                     help="Use absolute HSL lightness value")
        self.OptionParser.add_option("--hsv_a_abs",
                                     action="store", type="inkbool",
                                     dest="hsv_a_abs", default=False,
                                     help="Use absolute alpha value")
        # common
        self.OptionParser.add_option("--linear",
                                     action="store", type="inkbool",
                                     dest="linear", default=False,
                                     help="Adjust in linearRGB")

    def adjust_rgb(self, ink_color):
        """Adjust each color channel of RGB color representation."""
        # parameters
        rgb_r = self.options.rgb_r
        rgb_g = self.options.rgb_g
        rgb_b = self.options.rgb_b
        rgb_a = self.options.rgb_a / 100.0
        r, g, b = ink_color.rgb_int()
        a = ink_color.alpha
        # color
        r = abs(rgb_r) if self.options.rgb_r_abs else r + rgb_r
        g = abs(rgb_g) if self.options.rgb_g_abs else g + rgb_g
        b = abs(rgb_b) if self.options.rgb_b_abs else b + rgb_b
        # alpha
        if not self.options.object_opacity:
            a = abs(rgb_a) if self.options.rgb_a_abs else a + rgb_a
        # update ink_color
        ink_color.from_int(r, g, b, a)

    def adjust_hsl(self, ink_color):
        """Adjust RGB color based on HSL color representation."""
        # parameters
        hsl_h = self.options.hsl_h / 360.0
        hsl_s = self.options.hsl_s / 100.0
        hsl_l = self.options.hsl_l / 100.0
        hsl_a = self.options.hsl_a / 100.0
        h, s, l, a = ink_color.rgba_to_hsla()
        # color
        h = abs(hsl_h) if self.options.hsl_h_abs else h + hsl_h
        s = abs(hsl_s) if self.options.hsl_s_abs else s + hsl_s
        l = abs(hsl_l) if self.options.hsl_l_abs else l + hsl_l
        # alpha
        if not self.options.object_opacity:
            a = abs(hsl_a) if self.options.hsl_a_abs else a + hsl_a
        # update ink_color
        ink_color.from_hsla(h, s, l, a)

    def adjust_hsv(self, ink_color):
        """Adjust RGB color based on HSV color representation."""
        # parameters
        hsv_h = self.options.hsv_h / 360.0
        hsv_s = self.options.hsv_s / 100.0
        hsv_v = self.options.hsv_v / 100.0
        hsv_a = self.options.hsv_a / 100.0
        h, s, v, a = ink_color.rgba_to_hsva()
        # color
        h = abs(hsv_h) if self.options.hsv_h_abs else h + hsv_h
        s = abs(hsv_s) if self.options.hsv_s_abs else s + hsv_s
        v = abs(hsv_v) if self.options.hsv_v_abs else v + hsv_v
        # alpha
        if not self.options.object_opacity:
            a = abs(hsv_a) if self.options.hsv_a_abs else a + hsv_a
        # update ink_color
        ink_color.from_hsva(h, s, v, a)

    def modify_opacity(self, opacity):
        """Modify object *opacity*."""
        if self.options.object_opacity:
            object_o = float(opacity)
            if self.options.tab == '"rgb_tab"':
                if self.options.rgb_a_abs:
                    object_o = abs(self.options.rgb_a) / 100.0
                else:
                    object_o += self.options.rgb_a / 100.0
            elif self.options.tab == '"hsl_tab"':
                if self.options.hsl_a_abs:
                    object_o = abs(self.options.hsl_a) / 100.0
                else:
                    object_o += self.options.hsl_a / 100.0
            elif self.options.tab == '"hsv_tab"':
                if self.options.hsv_a_abs:
                    object_o = abs(self.options.hsv_a) / 100.0
                else:
                    object_o += self.options.hsv_a / 100.0
            # update object opacity
            return object_o

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        # experimental: convert to linearRGB
        if self.options.linear:
            ink_color.from_float(*ink_color.rgb_linear())
        # adjust color
        if self.options.tab == '"rgb_tab"':
            self.adjust_rgb(ink_color)
        elif self.options.tab == '"hsl_tab"':
            self.adjust_hsl(ink_color)
        elif self.options.tab == '"hsv_tab"':
            self.adjust_hsv(ink_color)
        # exerimental: convert back to sRGB
        if self.options.linear:
            ink_color.from_linear(*ink_color.rgb_float())


if __name__ == '__main__':
    ME = ColorAdjust()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
