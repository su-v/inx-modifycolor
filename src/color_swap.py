#!/usr/bin/env python
"""
color_swap           - swap or copy SVG paints

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class PaintCopy(modcol.ColorProps):
    """ColorEffect-based class to swap SVG paints."""

    def __init__(self):
        """Init base class and PaintCopy()."""
        modcol.ColorProps.__init__(self)

        # fill and stroke
        self.OptionParser.add_option("--fill_stroke_mode",
                                     action="store",
                                     type="string",
                                     dest="fill_stroke_mode",
                                     default="fillstroke",
                                     help="Exchange mode fill, stroke")
        self.OptionParser.add_option("--fill_stroke_copy_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="fill_stroke_copy_alpha",
                                     default=True,
                                     help="Copy alpha")
        self.OptionParser.add_option("--fill_stroke_copy_none",
                                     action="store",
                                     type="inkbool",
                                     dest="fill_stroke_copy_none",
                                     default=True,
                                     help="Copy 'None' property")
        self.OptionParser.add_option("--fill_stroke_copy_unset",
                                     action="store",
                                     type="inkbool",
                                     dest="fill_stroke_copy_unset",
                                     default=True,
                                     help="Copy 'Unset' property")
        self.OptionParser.add_option("--fill_stroke_convert_unset",
                                     action="store",
                                     type="inkbool",
                                     dest="fill_stroke_convert_unset",
                                     default=True,
                                     help="Convert 'Unset' property")

    def copy_paint(self, odict, sdict, source='fill', target='stroke'):
        """Copy *source* properties to *target*."""
        # set target
        if odict.get(source, False):
            if odict[source] != 'none':
                # source is color or paint server
                sdict[target] = odict[source]
            else:
                # source is none
                if self.options.fill_stroke_copy_none:
                    sdict[target] = odict[source]
        else:
            # source is unset
            if self.options.fill_stroke_copy_unset:
                if self.options.fill_stroke_convert_unset:
                    if source == 'fill':
                        sdict[target] = 'black'
                    elif source == 'stroke':
                        sdict[target] = 'none'
                elif target in odict:
                    sdict.pop(target, None)

        # set target opacity
        if self.options.fill_stroke_copy_alpha:
            source_a = source + '-opacity'
            target_a = target + '-opacity'
            if odict.get(source_a, False):
                # source opacity is set
                sdict[target_a] = odict[source_a]
            else:
                # source opacity is unset
                if target_a in odict:
                    sdict[target_a] = "1"

    def copy_props(self, sdict):
        """Copy fill, stroke properties."""
        odict = dict(sdict)
        if 'fill' in self.options.fill_stroke_mode:
            self.copy_paint(odict, sdict, source='fill', target='stroke')
        if 'stroke' in self.options.fill_stroke_mode:
            self.copy_paint(odict, sdict, source='stroke', target='fill')

    def modify_props(self, sdict, has_opacity):
        """Modify color properties in style dict *sdict*."""
        # ignore object opacity
        has_opacity = False
        # check for custom scope
        if self.options.scope == 'fill_stroke':
            if 'fill' in sdict or 'stroke' in sdict:
                # showme(sdict)
                self.copy_props(sdict)
                # showme(sdict)

    def custom_scope(self, ic_props):
        """Adjust scope via IC_PROPS dict *ic_props*."""
        # set custom scope
        if self.options.scope == 'fill_stroke':
            ic_props.pop('stop-color', None)
            ic_props.pop('flood-color', None)
            ic_props.pop('lightning-color', None)


if __name__ == '__main__':
    ME = PaintCopy()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
