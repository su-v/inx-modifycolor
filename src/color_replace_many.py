#!/usr/bin/env python
"""
color_replace_many - replace SVG paint color with other

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class ColorReplaceMany(modcol.ColorRGBA):
    """ColorEffect-based class to replace SVG paint colors."""

    def __init__(self):
        """Init base class and ColorReplaceMany()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.replace_dict = {}

        # replace
        self.OptionParser.add_option("--match_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="match_alpha",
                                     default=False,
                                     help="Match alpha")
        self.OptionParser.add_option("--update_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="update_alpha",
                                     default=False,
                                     help="Update alpha")
        self.OptionParser.add_option("--reverse",
                                     action="store",
                                     type="inkbool",
                                     dest="reverse",
                                     default=False,
                                     help="Reverse replacement order")

    # ----- modify current color

    def replace_color(self, ink_color, rdict):
        """Replace current color if match."""
        if self.options.match_alpha:
            current_hex = ink_color.rgba_hex()
        else:
            current_hex = ink_color.rgb_hex()
        # update ink_color
        if current_hex in rdict:
            ink_color.from_hex(rdict[current_hex])

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"replace_many_tab"':
            self.replace_color(ink_color, self.replace_dict)

    # ----- index color pairs

    def record_colors(self, node, rdict):
        """Record colors pairs in instance attribute *rdict*."""
        if len(node) < 2:
            return False
        objects = reversed(node[:2]) if self.options.reverse else node[:2]
        source_hex, target_hex = [modcol.hexcolor(obj) for obj in objects]
        if source_hex is not None and target_hex is not None:
            if not self.options.match_alpha:
                source_hex = source_hex[:-2]
            if not self.options.update_alpha:
                target_hex = target_hex[:-2]
            rdict[source_hex] = target_hex
            return True
        else:
            return False

    def record_selected(self, rdict):
        """Iterate selection for groups with 2 elements (color pairs)."""
        if len(self.selected):
            deselect_list = []
            # record color pairs with RGB(A) as key
            for node in self.selected.values():
                if modcol.is_group(node) and len(node) == 2:
                    if self.record_colors(node, rdict):
                        old_id = node.get('id', "")
                        deselect_list.append(old_id)
                        if not old_id.startswith(modcol.BASE_ID):
                            node.set('id', self.uniqueId(modcol.BASE_ID))
            # deselect processed groups
            for id_ in deselect_list:
                self.selected.pop(id_, None)

    # ----- prepare current document

    def prepare_doc(self):
        """Pre-process document to index color pairs."""
        # process selection for groups with 2 children
        if self.options.tab == '"replace_many_tab"':
            self.record_selected(self.replace_dict)


if __name__ == '__main__':
    ME = ColorReplaceMany()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
