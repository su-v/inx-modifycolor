#!/usr/bin/env python
"""
color_remap     - Substitue black and white, remap the rest

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorinterp as ci


class ColorRemap(modcol.ColorRGBA):
    """ColorEffect-based class to remap SVG paints."""

    def __init__(self):
        """Init base class and ColorRemap()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.rgb1 = []
        self.rgb2 = []

        # substitute color for black
        self.OptionParser.add_option("--color1",
                                     action="store", type="int",
                                     dest="color1", default=1364325887,
                                     help="Substitute color (black)")
        # substitute color for white
        self.OptionParser.add_option("--color2",
                                     action="store", type="int",
                                     dest="color2", default=-65281,
                                     help="Substitute color (white)")
        # common
        self.OptionParser.add_option("--reverse",
                                     action="store", type="inkbool",
                                     dest="reverse", default=False,
                                     help="Swap colors")
        self.OptionParser.add_option("--color_space",
                                     action="store", type="string",
                                     dest="color_space", default="sRGB",
                                     help="Color mode")

    def remap_black_and_white(self, ink_color):
        """Substitute black, white, remap the rest."""

        # get current RGB values
        if self.options.color_space == 'linearRGB':
            rgb_current = ink_color.rgb_linear()
        else:
            rgb_current = ink_color.rgb_float()

        # for each channel, remap to new range
        rgb_new = 3 * [0.0]
        for i in range(3):
            rgb_new[i] = ci.lerp(self.rgb1[i], self.rgb2[i], rgb_current[i])

        # update ink_color
        if self.options.color_space == 'linearRGB':
            ink_color.from_linear(*rgb_new)
        else:
            ink_color.from_float(*rgb_new)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if (self.options.tab == '"color1_tab"' or
                self.options.tab == '"color2_tab"'):
            self.remap_black_and_white(ink_color)

    def prepare_doc(self):
        """Prepare common stuff used later on to modify current color."""
        if (self.options.tab == '"color1_tab"' or
                self.options.tab == '"color2_tab"'):
            # modcol.inkex.debug(self.options.color1)
            ic_color1 = ic.InkColorRGBALong(self.options.color1)
            # modcol.inkex.debug(self.options.color2)
            ic_color2 = ic.InkColorRGBALong(self.options.color2)
            # store substitute colors as instance attributes
            if self.options.reverse:
                ic_color1, ic_color2 = ic_color2, ic_color1
            if self.options.color_space == 'linearRGB':
                self.rgb1 = ic_color1.rgb_linear()
                self.rgb2 = ic_color2.rgb_linear()
            else:
                self.rgb1 = ic_color1.rgb_float()
                self.rgb2 = ic_color2.rgb_float()


if __name__ == '__main__':
    ME = ColorRemap()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
