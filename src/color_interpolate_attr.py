#!/usr/bin/env python
"""
Copyright (C) 2009 Aurelio A. Heckert, aurium (a) gmail dot com
Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
# standard library
import math  # radians

# local library
import inkex
import simplestyle
from pathmodifier import zSort

# color interpolation
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorinterp as ci


def apply_style(node, val, attr_name):
    """Update node 'style' attribute with *val*."""
    sdict = simplestyle.parseStyle(node.get('style', None))
    sdict[attr_name] = str(val)
    node.set('style', simplestyle.formatStyle(sdict))


def apply_transform(node, val, attr_name):
    """Update node 'transform' attribute with *val*."""
    transform = node.get('transform', "")
    if attr_name == 'trans-x':
        val = 'translate({},0)'.format(val)
    elif attr_name == 'trans-y':
        val = 'translate(0,{})'.format(val)
    else:
        val = '{}({})'.format(attr_name, val)
    node.set('transform', '{} {}'.format(transform, val))


def apply_other(node, val, attr_name, attr_ns):
    """Update other node attribute *attr_name* with *val*."""
    if attr_ns == 'svg':
        node.set(attr_name, str(val))
    else:
        node.set(inkex.addNS(attr_name, attr_ns), str(val))


def apply_step(node, val, params):
    """Update *node* based on interpolated value *val*."""
    # prepare attribute value
    if params['type'] == 'color':
        val = 'rgb({},{},{})'.format(*val.rgb_int())
    else:
        if params['type'] == 'float':
            val = '{:.10f}'.format(val)
        elif params['type'] == 'int':
            val = str(int(round(val)))
        elif params['type'] == 'angle':
            # convert normalized angle to radians
            val = str(math.radians(float(val) * 360.0))
        else:
            val = str(val)
    # update node attribute with new value
    if params['loc'] == 'style':
        apply_style(node, val, params['name'])
    elif params['loc'] == 'transform':
        apply_transform(node, val, params['name'])
    elif params['loc'] == 'tag':
        apply_other(node, val, params['name'], params['ns'])


class InterpolateAttribute(inkex.Effect):
    """Class for interpolating an attribute of objects in selection."""

    def __init__(self):
        """Init base class and InterpolateAttribute()."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("-a", "--att",
                                     action="store", type="string",
                                     dest="att", default="fill",
                                     help="Attribute to be interpolated.")
        self.OptionParser.add_option("-o", "--att-other-name",
                                     action="store", type="string",
                                     dest="att_other_name",
                                     help="Other attribute.")
        self.OptionParser.add_option("-t", "--att-other-type",
                                     action="store", type="string",
                                     dest="att_other_type",
                                     help="The other attribute type.")
        self.OptionParser.add_option("-w", "--att-other-where",
                                     action="store", type="string",
                                     dest="att_other_where",
                                     help="tag attribute or style attribute?")
        self.OptionParser.add_option("--att-other-namespace",
                                     action="store", type="string",
                                     dest="att_other_namespace", default="svg",
                                     help="tag namespace")
        self.OptionParser.add_option("-s", "--start-val",
                                     action="store", type="string",
                                     dest="start_val", default="red",
                                     help="Initial interpolation value.")
        self.OptionParser.add_option("-e", "--end-val",
                                     action="store", type="string",
                                     dest="end_val", default="rgb(255,0,0)",
                                     help="End interpolation value.")
        self.OptionParser.add_option("-u", "--unit",
                                     action="store", type="string",
                                     dest="unit", default="none",
                                     help="Values unit.")
        self.OptionParser.add_option("--interp_mode",
                                     action="store", type="string",
                                     dest="interp_mode", default="linear",
                                     help="Interpolation mode")
        self.OptionParser.add_option("--color_mode",
                                     action="store", type="string",
                                     dest="color_mode", default="sRGB",
                                     help="Color space for interpolation")
        self.OptionParser.add_option("--hue_sweep",
                                     action="store", type="string",
                                     dest="hue_sweep", default="nearest",
                                     help="Interpolation angle for hue")
        self.OptionParser.add_option("--reverse",
                                     action="store", type="inkbool",
                                     dest="reverse", default=True,
                                     help="reverse start, end values")
        self.OptionParser.add_option("--zsort",
                                     action="store", type="inkbool",
                                     dest="zsort", default=True,
                                     help="use z- instead of selection order")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab")
        self.OptionParser.add_option("--att_select",
                                     action="store", type="string",
                                     dest="att_select",
                                     help="The selected UI-tab")

    def get_attribute_params(self):
        """Get attribute parameters based on preset or custom input."""
        params = {}
        presets = {
            'width': ['float', 'tag', 'svg'],
            'height': ['float', 'tag', 'svg'],
            'scale': ['float', 'transform', 'svg'],
            'trans-x': ['float', 'transform', 'svg'],
            'trans-y': ['float', 'transform', 'svg'],
            'fill': ['color', 'style', 'svg'],
            'fill-opacity': ['float', 'style', 'svg'],
            'stroke': ['color', 'style', 'svg'],
            'stroke-opacity': ['float', 'style', 'svg'],
            'opacity': ['float', 'style', 'svg'],
        }
        if self.options.att_select == '"att_other"':
            # custom attribute config
            params['name'] = self.options.att_other_name
            params['type'] = self.options.att_other_type
            params['loc'] = self.options.att_other_where
            params['ns'] = self.options.att_other_namespace
        else:
            # attribute presets
            params['name'] = self.options.att
            for i, key in enumerate(['type', 'loc', 'ns']):
                params[key] = presets[params['name']][i]
        # common parameter
        params['unit'] = self.options.unit
        # return selected parameters
        return params

    def get_elements(self):
        """Get and check selection, return group or sorted list."""
        collection = []
        if len(self.selected) == 0:
            pass
        elif len(self.selected) == 1:
            # must be a group
            node = self.selected[self.options.ids[0]]
            if modcol.is_group(node):
                collection = node
        else:
            # multiple selection
            if self.options.zsort:
                sorted_ids = zSort(self.document.getroot(),
                                   self.selected.keys())
            else:
                sorted_ids = self.options.ids
            collection = list(sorted_ids)
            for i, val in enumerate(sorted_ids):
                collection[i] = self.getElementById(val)
        return collection

    def get_color_values(self):
        """Get start, end values for attribute 'color'."""
        val_ini = self.options.start_val.strip()
        val_end = self.options.end_val.strip()
        if self.options.reverse:
            val_ini, val_end = val_end, val_ini
        ic_color = ic.InkColor()
        if ic.is_color(val_ini):
            ic_color.from_css(val_ini)
        ic_ini = ic_color.copy()
        if ic.is_color(val_end):
            ic_color.from_css(val_end)
        ic_end = ic_color.copy()
        return ic_ini, ic_end

    def get_numeric_values(self, attr_unit):
        """Get start, end values for numeric attributes."""
        val_ini = self.options.start_val.strip()
        val_end = self.options.end_val.strip()
        if self.options.reverse:
            val_ini, val_end = val_end, val_ini
        if attr_unit == 'deg':  # normalize to 0.0 - 1.0
            val_ini = float(val_ini) / 360.0
            val_end = float(val_end) / 360.0
        elif attr_unit == 'rad':  # normalize to 0.0 - 1.0
            val_ini = float(val_ini) / (2 * math.pi)
            val_end = float(val_end) / (2 * math.pi)
        elif attr_unit != 'none':
            val_ini = self.unittouu(val_ini + attr_unit)
            val_end = self.unittouu(val_end + attr_unit)
        else:
            try:
                val_ini = float(val_ini)
                val_end = float(val_end)
            except ValueError:
                val_ini = val_end = 1.0  # fallback or assert?
        return val_ini, val_end

    def interpolate_color(self, values, time):
        """Interpolate attribute value based on *values* at *time*."""
        sweep = self.options.hue_sweep
        colorspace = self.options.color_mode
        interp_mode = self.options.interp_mode
        ic_ini, ic_end = values
        func = getattr(ci, '{}_ic'.format(interp_mode), None)
        if func is not None:
            ic_new = func(ic_ini, ic_end, time, colorspace, sweep)
        else:
            ic_new = ic_ini
        return ic_new

    def interpolate_angle(self, values, time):
        """Interpolate attribute value based on *values* at *time*."""
        turn = 1.0  # see get_numeric_values()
        sweep = 'ccw'
        interp_mode = self.options.interp_mode
        val_ini, val_end = values
        func = getattr(ci, '{}_polar'.format(interp_mode), None)
        if func is not None:
            val = func(val_ini, val_end, time, turn, sweep)
        else:
            val = val_ini
        return val

    def interpolate_numeric(self, values, time):
        """Interpolate attribute value based on *values* at *time*."""
        interp_mode = self.options.interp_mode
        val_ini, val_end = values
        func = getattr(ci, interp_mode, None)
        if func is not None:
            val = func(val_ini, val_end, time)
        else:
            val = val_ini
        return val

    def effect(self):
        """Interpolate attribute of objects in selection."""
        # get collection of objects
        collection = self.get_elements()
        # iterate through collection
        if not len(collection) or len(collection) < 2:
            inkex.errormsg('There is no selection to interpolate')
            return False
        else:
            # get attribute parameters
            attr_params = self.get_attribute_params()
            # get start, end values
            if attr_params['type'] == 'color':
                values = self.get_color_values()
            else:
                values = self.get_numeric_values(attr_params['unit'])
            # interate collection and apply interpolated value
            for i, node in enumerate(collection):
                # interpolate values
                time = float(i) / (len(collection) - 1)
                if attr_params['type'] == 'color':
                    val = self.interpolate_color(values, time)
                elif attr_params['type'] == 'angle':
                    val = self.interpolate_angle(values, time)
                else:
                    val = self.interpolate_numeric(values, time)
                # update node attribute
                apply_step(node, val, attr_params)
            return True


if __name__ == '__main__':  # pragma: no cover
    e = InterpolateAttribute()
    if e.affect():
        exit(0)
    else:
        exit(1)

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
