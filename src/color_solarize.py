#!/usr/bin/env python
"""
color_solarize - solarize SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


class ColorSolarize(modcol.ColorRGBA):
    """ColorEffect-based class to solarize SVG paint colors."""

    def __init__(self):
        """Init base class and ColorSolarize()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--r_threshold",
                                     action="store", type="int",
                                     dest="r_threshold", default=50,
                                     help="Red threshold")
        self.OptionParser.add_option("--g_threshold",
                                     action="store", type="int",
                                     dest="g_threshold", default=50,
                                     help="Green threshold")
        self.OptionParser.add_option("--b_threshold",
                                     action="store", type="int",
                                     dest="b_threshold", default=50,
                                     help="Blue threshold")
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="solarize",
                                     help="Effect mode")

    def solarize(self, threshold, value):
        """Return solarized value of color component based on *threshold*."""
        # pylint: disable=no-self-use
        return value if value < threshold else 1.0 - value

    def moonarize(self, threshold, value):
        """Return moonarized value of color component based on *threshold*."""
        # pylint: disable=no-self-use
        return 1.0 - value if value < threshold else value

    def solarize_color(self, ink_color):
        """Solarize red, green and blue components of InkColor."""
        r_threshold = self.options.r_threshold / 100.0
        g_threshold = self.options.g_threshold / 100.0
        b_threshold = self.options.b_threshold / 100.0
        coloreffect = getattr(self, self.options.mode)
        r, g, b = ink_color.rgb_float()
        r = coloreffect(r_threshold, r)
        g = coloreffect(g_threshold, g)
        b = coloreffect(b_threshold, b)
        ink_color.from_float(r, g, b)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"solarize_tab"':
            self.solarize_color(ink_color)


if __name__ == '__main__':
    ME = ColorSolarize()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
