#!/usr/bin/env python
"""
color_brilliance - adjust brilliance of SVG RGB paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorBrilliance(modcol.ColorRGBA):
    """ColorEffect-based class to adjust brilliance of SVG paint colors."""

    def __init__(self):
        """Init base class and ColorBrilliance()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--brill_defaults",
                                     action="store", type="inkbool",
                                     dest="brill_defaults",
                                     default=True,
                                     help="Use default values")
        self.OptionParser.add_option("--brill_brightness",
                                     action="store", type="float",
                                     dest="brill_brightness", default="2.0",
                                     help="Adjust Brightness")
        self.OptionParser.add_option("--brill_saturation",
                                     action="store", type="float",
                                     dest="brill_saturation", default="0.5",
                                     help="Adjust Saturation")
        self.OptionParser.add_option("--brill_lightness",
                                     action="store", type="float",
                                     dest="brill_lightness", default="0.0",
                                     help="Adjust Lightness")
        self.OptionParser.add_option("--brill_invert",
                                     action="store", type="inkbool",
                                     dest="brill_invert", default=False,
                                     help="Invert Brilliance")

    def adjust_brilliance(self, ink_color):
        """
        Brightness Filter.

        Based on CPF (Inkscape 0.91)

        feColorMatrix:

            St Vi Vi 0  Li
            Vi St Vi 0  Li
            Vi Vi St 0  Li
            0  0  0  1  0

        """
        if self.options.brill_defaults:
            brightness = 2
            saturation = 0.5
            lightness = 0
            invert = False
        else:
            brightness = self.options.brill_brightness
            saturation = self.options.brill_saturation
            lightness = self.options.brill_lightness
            invert = self.options.brill_invert

        if not (brightness == 1.0 and
                saturation == 0.0 and
                lightness == 0.0 and
                invert is False):

            if invert:
                brightness = -1 * brightness
                saturation = 1.0 + saturation
                lightness = -1 * lightness
            else:
                saturation = -1 * saturation

            mat = [[0.0 for _ in range(5)] for _ in range(5)]
            for i in range(3):
                for j in range(3):
                    mat[i][j] = saturation
                    mat[i][3] = 0.0
                    mat[i][4] = lightness
            for i in range(3):
                mat[i][i] = brightness
            for i in (3, 4):
                mat[i][i] = 1.0

            # apply matrix
            r, g, b = ink_color.rgb_float()
            mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
            if mat_rgb_new is not None:
                ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"brilliance_tab"':
            self.adjust_brilliance(ink_color)


if __name__ == '__main__':
    ME = ColorBrilliance()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
