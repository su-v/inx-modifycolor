#!/usr/bin/env python
"""
color_named - replace SVG paint colors with named colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


TODO:

    - implement option to delete named colors:
      * selected group or all processed?
      * implicitly solidify all uses?
    - add option to convert 'osb:paint' named colors (SVG 1.1) to
      <solidcolor> definitions (SVG 2)

"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorsort as cs
import inkex


# debug
DEBUG = False
# globals
NSS = dict(inkex.NSS)
NSS[u'osb'] = u'http://www.openswatchbook.org/uri/2009/osb'


def add_ns(tag, namespace=None):
    """Helper function to concat XML tag and namespace."""
    val = tag
    if (namespace is not None and len(namespace) > 0 and
            namespace in NSS and len(tag) > 0 and tag[0] != '{'):
        val = "{%s}%s" % (NSS[namespace], tag)
    return val


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


def add_id_prefix(node_id, prefix='OSB_'):
    """Prepend prefix to node_id string."""
    if not node_id.startswith(prefix):
        return prefix + node_id
    else:
        return node_id


def strip_id_prefix(node_id, prefix='OSB_'):
    """Strip known prefix from node_id string."""
    if node_id.startswith(prefix):
        return node_id[len(prefix):]
    else:
        return node_id


def add_id_suffix(node_id, suffix='_OSB'):
    """Append suffix to node_id string."""
    if not node_id.endswith(suffix):
        return node_id + suffix
    else:
        return node_id


def strip_id_suffix(node_id, suffix='_OSB'):
    """Strip known suffix from node_id string."""
    if node_id.endswith(suffix):
        return node_id[:-len(suffix)]
    else:
        return node_id


def get_style_dict(node):
    """Return style dictionary or node.attrib."""
    sdict = ic.string_to_dict(node.get('style', None))
    if not sdict:
        sdict = dict(node.attrib)
    return sdict


def record_osb_swatches(node, rdict, prop='stop-color'):
    """Build dict of existing osb:paint definitions."""
    for child in node:
        if modcol.is_osb_paint(child, 'solid'):
            child_id = child.get('id')
            child_hex = modcol.hexcolor(child[0], paint=prop)
            if child_hex is not None:
                rdict[child_id] = child_hex


def record_solid_colors(node, rdict, prop='solid-color'):
    """Build dict of existing <solidcolor> definitions."""
    for child in node:
        if modcol.is_solid_color(child):
            child_id = child.get('id')
            child_hex = modcol.hexcolor(child, paint=prop)
            if child_hex is not None:
                rdict[child_id] = child_hex


def lookup_id(string, rdict):
    """Return id (key) for value *string* in dict *rdict* or None."""
    cutoff = len(string)
    try:
        return next(key for key, val in rdict.items()
                    if val[:cutoff] == string)
    except StopIteration:
        pass
    return None


class ColorNamed(modcol.ColorRGBA):
    """ColorEffect-based class for palettes with named colors."""
    # pylint: disable=too-many-public-methods

    def __init__(self):
        """Init base class and ColorNamed()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.namedcolor_dict = {}
        self.swatchbook_dict = {}
        self.solidcolor_dict = {}

        # convert to named colors
        self.OptionParser.add_option("--create_named_colors",
                                     action="store",
                                     type="inkbool",
                                     dest="create_named_colors",
                                     default=True,
                                     help="Create named colors")
        self.OptionParser.add_option("--convert_to_named_colors",
                                     action="store",
                                     type="inkbool",
                                     dest="convert_to_named_colors",
                                     default=True,
                                     help="Convert to named colors")
        self.OptionParser.add_option("--record_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="record_alpha",
                                     default=True,
                                     help="Record alpha")
        # convert from named colors
        self.OptionParser.add_option("--delete_named_colors",
                                     action="store",
                                     type="inkbool",
                                     dest="delete_named_colors",
                                     default=False,
                                     help="Delete named colors")
        self.OptionParser.add_option("--convert_to_solid_colors",
                                     action="store",
                                     type="inkbool",
                                     dest="convert_to_solid_colors",
                                     default=True,
                                     help="Convert to solid colors")
        self.OptionParser.add_option("--use_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="use_alpha",
                                     default=True,
                                     help="Use alpha from named color")
        # quick mode
        self.OptionParser.add_option("--quick_mode",
                                     action="store",
                                     type="string",
                                     dest="quick_mode",
                                     default="create_link",
                                     help="Quick mode")
        self.OptionParser.add_option("--record_alpha_quick",
                                     action="store",
                                     type="inkbool",
                                     dest="record_alpha_quick",
                                     default=True,
                                     help="Record alpha in quick mode")
        self.OptionParser.add_option("--use_alpha_quick",
                                     action="store",
                                     type="inkbool",
                                     dest="use_alpha_quick",
                                     default=True,
                                     help="Use alpha in quick mode")
        # sort
        self.OptionParser.add_option("--sort",
                                     action="store",
                                     type="string",
                                     dest="sort",
                                     default="most_used",
                                     help="Color sort order")
        self.OptionParser.add_option("--reverse",
                                     action="store",
                                     type="inkbool",
                                     dest="reverse",
                                     default=False,
                                     help="Reverse sort order")
        # common
        self.OptionParser.add_option("--svg_version",
                                     action="store",
                                     type="string",
                                     dest="svg_version",
                                     default="SVG11",
                                     help="SVG version")

    # ----- finally ...

    def sort_colors(self, colors, rdict):
        """Return sorted colors list based on user input."""
        prop = self.options.sort
        reverse = self.options.reverse
        out_method = None
        if prop.startswith('rgb'):
            out_method = 'rgba_float'
        elif prop.startswith('hsl') or prop.startswith('hsv'):
            out_method = 'rgba_to_{}a'.format(prop[:3])
        elif (prop.startswith('lab') or prop.startswith('lch') or
              prop.startswith('luv')):
            cs.sort_colors_by_cie(colors, prop, reverse)
        elif prop == 'most_used':
            cs.sort_colors_by_count(
                colors, rdict, self.visited_nc, reverse)
        if out_method is not None:
            cs.sort_colors_by_ic(colors, prop, out_method, reverse)

    def sort_named_colors(self, rdict):
        """Sort named color definition."""
        if len(rdict):
            colors = list(rdict.values())
            self.sort_colors(colors, rdict)
            # rewrite them in sorted order (append to defs)
            for hex_color in colors:
                node_id = lookup_id(hex_color, rdict)
                node = self.getElementById(node_id)
                node.getparent().append(node)

    def delete_named_color(self, color_id, rdict, suffix):
        """Check and delete named color definition."""
        swatch_id = strip_id_suffix(color_id, suffix) if suffix else color_id
        if swatch_id in rdict:
            swatch_obj = self.getElementById(swatch_id)
            if swatch_obj is not None:
                swatch_obj.getparent().remove(swatch_obj)
            color_obj = self.getElementById(color_id)
            if color_obj is not None:
                color_obj.set('id', swatch_id)

    def cleanup_doc(self):
        """Run this after all processing of individual props is done."""
        if self.options.svg_version == 'SVG11':
            suffix = '_OSB'
            rdict = self.swatchbook_dict
        else:  # if self.options.svg_version == 'SVG2':
            suffix = '_SC'
            rdict = self.solidcolor_dict
        # action depends on active tab
        if self.options.tab == '"solid_color_tab"':
            if self.options.delete_named_colors:
                for color_id in self.namedcolor_dict.keys():
                    # delete swatch based on selected group
                    self.delete_named_color(color_id, rdict, suffix)
        elif self.options.tab == '"quick_tab"':
            if self.options.quick_mode == 'purge_all':
                for color_id in self.namedcolor_dict.keys():
                    # delete swatches solidified earlier
                    self.delete_named_color(color_id, rdict, '')
        elif self.options.tab == '"sort_tab"':
            # sort named colors depending on sort mode
            self.sort_named_colors(rdict)

    # ----- post-process: apply

    def link_paint(self, sdict, paint, rdict, version):
        """Link paint to paint server with matching RGB(A) color."""
        odict = dict(sdict)
        is_linked = False
        if paint in odict and ic.is_color(odict[paint]):
            ic_paint = ic.InkColor()
            ic_paint.from_style_dict(odict, prop=paint)
            if self.options.record_alpha:
                color_hex = ic_paint.rgba_hex()
            else:
                color_hex = ic_paint.rgb_hex()
            rdict_values = [s[:len(color_hex)] for s in rdict.values()]
            if color_hex in rdict_values:
                is_linked = version
                swatch_id = lookup_id(color_hex, rdict)
                sdict[paint] = 'url(#' + swatch_id + ')'
                if self.options.record_alpha:
                    # reset paint's own opacity
                    sdict[paint + '-opacity'] = 1.0
        return is_linked

    def solidify_paint(self, sdict, paint, rdict, version):
        """Replace linked paint server with its solid color."""
        odict = dict(sdict)
        is_solidified = False
        if paint in odict and not ic.is_color(odict[paint]):
            swatch_id = odict[paint][5:-1]
            if swatch_id in rdict:
                is_solidified = version
                ic_paint = ic.InkColorRGBAHex(rdict[swatch_id])
                sdict[paint] = ic_paint.rgb_hex()
                if self.options.use_alpha:
                    # override paint's own opacity
                    sdict[paint + '-opacity'] = str(ic_paint.alpha)
        return is_solidified

    def purge_paint(self, sdict, paint, rdict, version):
        """Replace linked paint server if in namedcolor_dict."""
        odict = dict(sdict)
        is_purged = False
        if paint in odict and not ic.is_color(odict[paint]):
            swatch_id = odict[paint][5:-1]
            ndict = [s[:s.rfind('_')] for s in self.namedcolor_dict.keys()]
            if swatch_id in rdict and swatch_id in ndict:
                is_purged = version
                ic_paint = ic.InkColorRGBAHex(rdict[swatch_id])
                sdict[paint] = ic_paint.rgb_hex()
                if self.options.use_alpha:
                    # override paint's own opacity
                    sdict[paint + '-opacity'] = str(ic_paint.alpha)
        return is_purged

    def purge_all_paint(self, sdict, paint, rdict, version):
        """Replace all linked paint servers with solid color."""
        odict = dict(sdict)
        is_purged = False
        if paint in odict and not ic.is_color(odict[paint]):
            swatch_id = odict[paint][5:-1]
            if swatch_id in rdict:
                is_purged = version
                ic_paint = ic.InkColorRGBAHex(rdict[swatch_id])
                sdict[paint] = ic_paint.rgb_hex()
                if self.options.use_alpha:
                    # override paint's own opacity
                    sdict[paint + '-opacity'] = str(ic_paint.alpha)
                # record swatch_id for deletion later on
                if swatch_id not in self.namedcolor_dict:
                    self.namedcolor_dict[swatch_id] = rdict[swatch_id]
        return is_purged

    def create_paintserver(self, sdict, paint, rdict, version):
        """Create new paint server."""
        is_created = False
        odict = dict(sdict)
        if paint in odict and ic.is_color(odict[paint]):
            ic_paint = ic.InkColor()
            ic_paint.from_style_dict(odict, prop=paint)
            if self.options.record_alpha:
                color_hex = ic_paint.rgba_hex()
            else:
                color_hex = ic_paint.rgb_hex()
            rdict_values = [s[:len(color_hex)] for s in rdict.values()]
            if color_hex not in rdict_values:
                color_id = color_hex[1:]
                if version == 'SVG11':
                    swatch_id = self.add_osb_swatch(color_hex, color_id)
                else:  # if version == 'SVG2':
                    swatch_id = self.add_solid_color(color_hex, color_id)
                if swatch_id is not None:
                    is_created = version
                    rdict[swatch_id] = color_hex
        return is_created

    def create_link_paint(self, sdict, paint, rdict, version):
        """Create new paint server, re-index and link paint."""
        is_linked = self.link_paint(sdict, paint, rdict, version)
        if not is_linked:
            if self.create_paintserver(sdict, paint, rdict, version):
                is_linked = self.link_paint(sdict, paint, rdict, version)
        return is_linked

    def check_paints(self, action, version, sdict):
        """Check and update paints suitable for change."""
        func = getattr(self, '{}_paint'.format(action), None)
        if func is not None:
            if version == 'SVG11':
                rdict = self.swatchbook_dict
                props = ('fill', 'stroke')
            else:  # if version == 'SVG2':
                rdict = self.solidcolor_dict
                # NOTE: stop-color with solidcolor paint server doesn't
                # seem to work in Inkscape yet.
                props = ('fill', 'stroke', 'stop-color')
            for paint in props:
                func(sdict, paint, rdict, version)

    def update_props(self, sdict, flags):
        """Post-process current style dict for named color matches."""
        version = self.options.svg_version
        if self.options.tab == '"named_color_tab"':
            if self.options.convert_to_named_colors:
                self.check_paints('link', version, sdict)
        elif self.options.tab == '"solid_color_tab"':
            if self.options.delete_named_colors:
                self.check_paints('purge', version, sdict)
            elif self.options.convert_to_solid_colors:
                self.check_paints('solidify', version, sdict)
        elif self.options.tab == '"quick_tab"':
            if self.options.quick_mode == 'create_link':
                self.options.record_alpha = self.options.record_alpha_quick
                self.check_paints('create_link', version, sdict)
            elif self.options.quick_mode == 'purge_all':
                self.options.use_alpha = self.options.use_alpha_quick
                self.check_paints('purge_all', version, sdict)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        pass

    def check_props(self, sdict):
        """Check color properties in style dict *sdict* before processing."""

        def set_skip_flags():
            """Set flags to skip further processing in color_lib.common."""
            flags['skip_color'] = True

        flags = {}
        if self.options.tab == '"named_color_tab"':
            if not self.options.convert_to_named_colors:
                set_skip_flags()
        elif self.options.tab == '"solid_color_tab"':
            if self.options.delete_named_colors:
                pass
            elif not self.options.convert_to_solid_colors:
                set_skip_flags()
        elif self.options.tab == '"quick_tab"':
            pass
        elif self.options.tab == '"sort_tab"':
            pass
        else:
            set_skip_flags()
        return flags

    # ----- pre-process: add / update

    def add_osb_swatch(self, color_hex, color_id):
        """Create new osb gradient with color_hex."""
        ic_color = ic.InkColorRGBAHex(color_hex)
        osb_swatch = inkex.etree.Element(add_ns('linearGradient', 'svg'))
        osb_swatch_color = inkex.etree.Element(add_ns('stop', 'svg'))
        osb_swatch_color.set('stop-color', ic_color.rgb_hex())
        osb_swatch_color.set('stop-opacity', str(ic_color.alpha))
        osb_swatch.append(osb_swatch_color)
        osb_swatch.set(add_ns('paint', 'osb'), 'solid')
        osb_swatch.set('id', color_id)
        defs = modcol.get_defs(self.document.getroot())
        defs.append(osb_swatch)
        return color_id

    def update_osb_swatch(self, color_hex, color_id):
        """Update existing osb:paint swatch with new color."""
        ic_color = ic.InkColorRGBAHex(color_hex)
        osb_swatch = self.getElementById(color_id)
        if osb_swatch is not None:
            osb_swatch_color = osb_swatch[0]
            osb_swatch_color.set('stop-color', ic_color.rgb_hex())
            if self.options.record_alpha:
                osb_swatch_color.set('stop-opacity', str(ic_color.alpha))
        return color_id

    def add_solid_color(self, color_hex, color_id):
        """Create new solidcolor element with color_hex."""
        ic_color = ic.InkColorRGBAHex(color_hex)
        solid_color = inkex.etree.Element(add_ns('solidcolor', 'svg'))
        solid_color.set('solid-color', ic_color.rgb_hex())
        solid_color.set('solid-opacity', str(ic_color.alpha))
        solid_color.set('id', color_id)
        defs = modcol.get_defs(self.document.getroot())
        defs.append(solid_color)
        return color_id

    def update_solid_color(self, color_hex, color_id):
        """Update existing <solidcolor> swatch with new color."""
        ic_color = ic.InkColorRGBAHex(color_hex)
        solid_color = self.getElementById(color_id)
        if solid_color is not None:
            solid_color.set('solid-color', ic_color.rgb_hex())
            if self.options.record_alpha:
                solid_color.set('solid-opacity', str(ic_color.alpha))
        return color_id

    def process_color(self, action, color_hex, color_id):
        """Action dispatcher for *color_hex* based on named color type."""
        if self.options.svg_version == 'SVG11':
            func = getattr(self, '{}_osb_swatch'.format(action), None)
        else:  # if self.options.svg_version == 'SVG2':
            func = getattr(self, '{}_solid_color'.format(action), None)
        if func is not None:
            return func(color_hex, color_id)

    def process_source(self, color_hex, color_id, rdict, suffix):
        """Compare source *color_hex* with named colors in *rdict*."""
        rdict_values = [s[:len(color_hex)] for s in rdict.values()]
        placeholder = self.getElementById(color_id)
        stripped_id = strip_id_suffix(color_id, suffix)
        # update or create named color
        if stripped_id in rdict:
            swatch_id = self.process_color('update', color_hex, stripped_id)
        elif color_hex not in rdict_values:
            swatch_id = self.process_color('add', color_hex, color_id)
        else:
            swatch_id = lookup_id(color_hex, rdict)
        # update placeholder id
        if swatch_id is not None:
            placeholder.set('id', swatch_id + suffix)

    def check_named_color(self, color_hex, color_id):
        """Check source *color_hex* to create or update named color def."""
        if self.options.svg_version == 'SVG11':
            rdict = self.swatchbook_dict
            suffix = '_OSB'
        else:  # if self.options.svg_version == 'SVG2':
            rdict = self.solidcolor_dict
            suffix = '_SC'
        self.process_source(color_hex, color_id, rdict, suffix)

    def update_named_colors(self, defs):
        """Add or update existing named colors with new ones."""
        if self.namedcolor_dict:
            for color_id, color_hex in self.namedcolor_dict.items():
                self.check_named_color(color_hex, color_id)
            # re-index existing named colors
            self.swatchbook_dict = {}
            record_osb_swatches(defs, self.swatchbook_dict)
            self.solidcolor_dict = {}
            record_solid_colors(defs, self.solidcolor_dict)

    # ----- pre-process: index colors in selected groups

    def record_new_colors(self, node, rdict):
        """Record new colors within *node* in dict *rdict*."""
        special_group = False
        for obj in node:
            source_id = obj.get('id', None)
            source_hex = modcol.hexcolor(obj)
            if source_hex is not None:
                if not self.options.record_alpha:
                    source_hex = source_hex[:-2]
                rdict[source_id] = source_hex
                special_group = True
        return special_group

    def record_selected(self, rdict):
        """Record new colors within selected group(s) in dict *rdict*."""
        if len(self.selected):
            deselect_list = []
            # iterate through current selection
            for node in self.selected.values():
                if modcol.is_group(node) and len(node) >= 1:
                    if self.record_new_colors(node, rdict):
                        old_id = node.get('id')
                        deselect_list.append(old_id)
                        if not old_id.startswith(modcol.BASE_ID):
                            node.set('id', self.uniqueId(modcol.BASE_ID))
            # deselect processed groups
            for obj_id in deselect_list:
                self.selected.pop(obj_id, None)

    # ----- pre-process: document/selection

    def prepare_doc(self):
        """Pre-process document or selection."""

        # index existing named colors
        defs = modcol.get_defs(self.document.getroot())
        record_osb_swatches(defs, self.swatchbook_dict)
        record_solid_colors(defs, self.solidcolor_dict)

        if self.options.tab == '"named_color_tab"':
            # add / update named colors
            if self.options.create_named_colors:
                self.record_selected(self.namedcolor_dict)
                self.update_named_colors(defs)
        elif self.options.tab == '"solid_color_tab"':
            # delete named colors
            if self.options.delete_named_colors:
                self.record_selected(self.namedcolor_dict)
        elif self.options.tab == '"quick_tab"':
            if self.options.quick_mode == 'purge_all':
                # do not allow to purge named colors still in use
                self.selected = {}
        elif self.options.tab == '"sort_tab"':
            self.options.track_only = True

        if DEBUG:
            showme(self.namedcolor_dict)
            showme(self.solidcolor_dict)
            showme(self.swatchbook_dict)
            showme(self.selected)

    # ----- pre-process: scope

    def custom_scope(self, ic_props):
        """Build dicts with named colors before processing props."""
        # update ic_props for custom scopes
        if self.options.svg_version == 'SVG2':
            ic_props.pop('solid-color', None)
        if self.options.scope == 'custom':
            pass
        elif self.options.scope == 'custom_fill_only':
            ic_props.pop('stroke', None)
        elif self.options.scope == 'custom_stroke_only':
            ic_props.pop('fill', None)


if __name__ == '__main__':
    ME = ColorNamed()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
