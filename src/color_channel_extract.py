#!/usr/bin/env python
"""
color_channel_extract - extract color channel based on SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Known issue:

    - Doesn't work with (partially transparent) gradients
    - Requires to flatten each extracted color
      (blend 'normal' over a white background)

"""
# local library
from color_lib import common as modcol
from color_lib import colorblend as cb
from color_lib.colorops import color_extract


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class ColorChannelExtract(modcol.ColorRGBA):
    """ColorEffect-based class to extract color channel from SVG paints."""

    def __init__(self):
        """Init base class and ColorChannelExtract()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--channel",
                                     action="store", type="string",
                                     dest="channel", default="red",
                                     help="Channel")
        self.OptionParser.add_option("--greyscale",
                                     action="store", type="inkbool",
                                     dest="greyscale", default=False,
                                     help="Channel to greyscale")
        self.OptionParser.add_option("--flatten",
                                     action="store", type="inkbool",
                                     dest="flatten", default=True,
                                     help="Flatten coverage channel")

    def extract_channel(self, ink_color):
        """Extract a color channel from SVG paint colors."""

        # parameters
        channel = self.options.channel
        greyscale = self.options.greyscale
        flatten = self.options.flatten

        # extract channel value
        new_color = color_extract(ink_color, channel, greyscale=greyscale)

        # flatten extracted channel color (blend over white)
        if flatten:
            cb.blend_over_white(new_color)

        if new_color is not None:
            ink_color.from_float(*new_color.rgba_float())

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"channel_extract_tab"':
            self.extract_channel(ink_color)


if __name__ == '__main__':
    ME = ColorChannelExtract()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
