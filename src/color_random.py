#!/usr/bin/env python
"""
color_random - randomize SVG paint colors

Copyright (C) 2006-2016 <various authors>
Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


TODO:

    - Figure out a better implementation for this new feature:
      option to treat fill and stroke colors of the same object as
      one color to be randomized.

"""
# standard library
import random

# local library
from color_lib import common as modcol
from color_lib import colorconvert as cv
from color_lib.util import randomize_float


# constants
GOLDENRATIO = (1 + 5 ** 0.5) / 2.0


def check_prop(sdict, key):
    """Check color prop *key* in style dict *sdict*."""
    return key in sdict and modcol.ic.is_color(sdict[key])


def random_float_seed():
    """Return random number as seed."""
    return random.random()


def random_float_goldenratio(val):
    """Return random number harmonized with Golden Ratio."""
    goldenratio_conjugate = 1.0 / GOLDENRATIO
    val += goldenratio_conjugate
    return val % 1


def randomize_float_range(limit, val):
    """Return random value within float-based limited range."""
    limit /= 2.0
    r_max = (val * 100) + limit
    r_min = (val * 100) - limit
    if r_max > 100:
        r_min = r_min - (r_max - 100)
        r_max = 100
    if r_min < 0:
        r_max = r_max - r_min
        r_min = 0
    return random.uniform(r_min, r_max) / 100.0


def randomize_float_range_wrap(limit, val):
    """Randomize within range, wrapping around."""
    limit /= 2.0
    r_max = (val * 100) + limit
    r_min = (val * 100) - limit
    r_val = random.uniform(r_min, r_max) / 100.0
    return r_val % 1


def randomize_float_range_clamp(limit, val):
    """Randomize within range, clamping to min/max."""
    limit /= 2.0
    r_max = (val * 100) + limit
    r_min = (val * 100) - limit
    r_val = random.uniform(r_min, r_max) / 100.0
    return max(min(r_val, 1.0), 0.0)


def dispatch_randomize_float(limit, val, mode='shift_range'):
    """Dispatcher for randomizing a float value within range limit."""
    if mode.startswith('shift'):
        return randomize_float_range(limit, val)
    elif mode.startswith('wrap'):
        return randomize_float_range_wrap(limit, val)
    elif mode.startswith('clamp'):
        return randomize_float_range_clamp(limit, val)
    else:
        return val


def ink_color_to_other(ink_color, mode):
    """Convert RGB InkColor to other color space."""
    if mode == 'sRGB':
        comp1, comp2, comp3 = ink_color.rgb_float()
    elif mode == 'linearRGB':
        comp1, comp2, comp3 = ink_color.rgb_linear()
    elif mode == 'HSL':
        comp1, comp2, comp3 = ink_color.rgb_to_hsl()
    elif mode == 'HSV':
        comp1, comp2, comp3 = ink_color.rgb_to_hsv()
    elif mode == 'LUV':
        comp1, comp2, comp3 = cv.rgb_to_luv(*ink_color.rgb_float())
    elif mode == 'LAB':
        comp1, comp2, comp3 = cv.rgb_to_lab(*ink_color.rgb_float())
    elif mode == 'LCH':
        comp1, comp2, comp3 = cv.rgb_to_lch(*ink_color.rgb_float())
    elif mode == 'YIQ':
        comp1, comp2, comp3 = cv.rgb_to_yiq(*ink_color.rgb_float())
    return comp1, comp2, comp3


def other_to_ink_color(comp1, comp2, comp3, mode, ink_color):
    """Convert components in other color space to RGB-based InkColor."""
    if mode == 'sRGB':
        ink_color.from_float(comp1, comp2, comp3)
    elif mode == 'linearRGB':
        ink_color.from_linear(comp1, comp2, comp3)
    elif mode == 'HSL':
        ink_color.from_hsl(comp1, comp2, comp3)
    elif mode == 'HSV':
        ink_color.from_hsv(comp1, comp2, comp3)
    elif mode == 'LUV':
        ink_color.from_float(*cv.luv_to_rgb(comp1, comp2, comp3))
    elif mode == 'LAB':
        ink_color.from_float(*cv.lab_to_rgb(comp1, comp2, comp3))
    elif mode == 'LCH':
        ink_color.from_float(*cv.lch_to_rgb(comp1, comp2, comp3))
    elif mode == 'YIQ':
        ink_color.from_float(*cv.yiq_to_rgb(comp1, comp2, comp3))


class ColorRandomize(modcol.ColorRGBAO):
    """ColorEffect-based class to randomize SVG paint colors."""

    def __init__(self):
        """Init base class and ColorRandomize()."""
        modcol.ColorRGBAO.__init__(self)

        # instance attributes
        self.goldenratio_val = 0

        # default options
        self.OptionParser.add_option("-y", "--hue_range",
                                     action="store", type="int",
                                     dest="hue_range", default=0,
                                     help="Hue range")
        self.OptionParser.add_option("-t", "--saturation_range",
                                     action="store", type="int",
                                     dest="saturation_range", default=0,
                                     help="Saturation range")
        self.OptionParser.add_option("-m", "--lightness_range",
                                     action="store", type="int",
                                     dest="lightness_range", default=0,
                                     help="Lightness range")
        self.OptionParser.add_option("-o", "--opacity_range",
                                     action="store", type="int",
                                     dest="opacity_range", default=0,
                                     help="Opacity range")
        # custom options
        self.OptionParser.add_option("--color_space",
                                     action="store", type="string",
                                     dest="color_space", default="sRGB",
                                     help="Color space")
        self.OptionParser.add_option("--range_comp1",
                                     action="store", type="int",
                                     dest="range_comp1", default=100,
                                     help="Component 1 range")
        self.OptionParser.add_option("--range_comp2",
                                     action="store", type="int",
                                     dest="range_comp2", default=100,
                                     help="Component 2 range")
        self.OptionParser.add_option("--range_comp3",
                                     action="store", type="int",
                                     dest="range_comp3", default=100,
                                     help="Component 3 range")
        self.OptionParser.add_option("--range_alpha",
                                     action="store", type="int",
                                     dest="range_alpha", default=0,
                                     help="Alpha/Opacity range")
        self.OptionParser.add_option("--random_mode",
                                     action="store", type="string",
                                     dest="random_mode", default="wrap_all",
                                     help="Random mode (wrap or clamp)")
        # misc
        self.OptionParser.add_option("--presets",
                                     action="store", type="string",
                                     dest="presets", default="none",
                                     help="Miscellaneous presets")
        self.OptionParser.add_option("--misc_comp1",
                                     action="store", type="int",
                                     dest="misc_comp1", default=0,
                                     help="Component 1")
        self.OptionParser.add_option("--misc_comp2",
                                     action="store", type="int",
                                     dest="misc_comp2", default=50,
                                     help="Component 2")
        self.OptionParser.add_option("--misc_comp3",
                                     action="store", type="int",
                                     dest="misc_comp3", default=95,
                                     help="Component 3")

    # ----- custom scope, prop check

    def custom_scope(self, ic_props):
        """Adjust scope via IC_PROPS dict *ic_props*.

        Modifies IC_PROPS in-place (*ic_props* is a pointer, not a copy).
        """
        if self.options.scope == 'no_stops':
            # NOTE: does not prevent forking of processed gradients
            ic_props.pop('stop-color', None)

    def check_props(self, sdict):
        """Check color properties in style dict *sdict* before processing."""
        flags = {}
        # check for custom 'object' scope
        if self.options.scope == 'object':
            # check the element's fill and stroke RGB color, alpha
            if check_prop(sdict, 'fill') and check_prop(sdict, 'stroke'):
                # convert to InkColor objects objects
                ic_fill = modcol.ic.InkColor()
                ic_fill.from_style_dict(sdict, prop='fill')
                ic_stroke = modcol.ic.InkColor()
                ic_stroke.from_style_dict(sdict, prop='stroke')
                # compare normalized values from InkColor objects
                if ic_stroke.rgb_int() == ic_fill.rgb_int():
                    flags['copy_fill'] = True
                    # if RGB is the same, compare alpha
                    if ic_stroke.alpha == ic_fill.alpha:
                        flags['copy_fill_alpha'] = True
        # return result
        return flags

    def update_props(self, sdict, flags):
        """Update color properties in style dict *sdict* after processing."""
        # check for custom 'object' scope
        if self.options.scope == 'object':
            if flags.get('copy_fill', False):
                if 'fill' in sdict:
                    sdict['stroke'] = sdict['fill']
                flags.pop('copy_fill', None)
                if flags.get('copy_fill_alpha', False):
                    if 'fill-opacity' in sdict:
                        sdict['stroke-opacity'] = sdict['fill-opacity']
                    flags.pop('copy_fill_alpha', None)

    # ----- randomize in specific color space

    def randomize_luv_lab_lch(self, ink_color, mode, rmode):
        """Randomize LUV, LAB or LCH color components."""
        # TODO: add randomize and color checker for LUV, Lab and LCH
        pass

    def randomize_hsl_hsv(self, ink_color, mode, rmode):
        """Randomize HSL or HSV color components."""
        # parameters
        range1 = self.options.range_comp1
        range2 = self.options.range_comp2
        range3 = self.options.range_comp3
        # update ink_color
        if range1 or range2 or range3:
            # convert to selected color space components
            comp1, comp2, comp3 = ink_color_to_other(ink_color, mode)
            # randomize components
            if rmode == 'wrap_polar':
                if range1 > 0:
                    comp1 = dispatch_randomize_float(range1, comp1, 'wrap')
                if range2 > 0:
                    comp2 = dispatch_randomize_float(range2, comp2, 'clamp')
                if range3 > 0:
                    comp3 = dispatch_randomize_float(range3, comp3, 'clamp')
            else:
                if range1 > 0:
                    comp1 = dispatch_randomize_float(range1, comp1, rmode)
                if range2 > 0:
                    comp2 = dispatch_randomize_float(range2, comp2, rmode)
                if range3 > 0:
                    comp3 = dispatch_randomize_float(range3, comp3, rmode)
            # convert back to default sRGB
            other_to_ink_color(comp1, comp2, comp3, mode, ink_color)

    def randomize_rgb(self, ink_color, mode, rmode):
        """Randomize RGB color components."""
        # parameters
        range1 = self.options.range_comp1
        range2 = self.options.range_comp2
        range3 = self.options.range_comp3
        # update ink_color
        if range1 or range2 or range3:
            # convert to selected color space components
            comp1, comp2, comp3 = ink_color_to_other(ink_color, mode)
            # randomize components
            if range1 > 0:
                comp1 = dispatch_randomize_float(range1, comp1, rmode)
            if range2 > 0:
                comp2 = dispatch_randomize_float(range2, comp2, rmode)
            if range3 > 0:
                comp3 = dispatch_randomize_float(range3, comp3, rmode)
            # convert back to default sRGB
            other_to_ink_color(comp1, comp2, comp3, mode, ink_color)

    # ----- core randomize methods

    def randomize_misc(self, ink_color):
        """Other ideas for producing randomized (or unique) colors."""
        if self.options.presets.endswith('goldenratio'):
            # TODO: move to scheme generator
            comp1 = random_float_goldenratio(self.goldenratio_val)
            comp2 = self.options.misc_comp2 / 100.0
            comp3 = self.options.misc_comp3 / 100.0
            if self.options.presets.startswith('hsv'):
                ink_color.from_hsv(comp1, comp2, comp3)
            elif self.options.presets.startswith('hsl'):
                ink_color.from_hsl(comp1, comp2, comp3)
            self.goldenratio_val = comp1

    def randomize_custom(self, ink_color):
        """Randomize color in selected color space.

        The randomness is not the same depending on the color space used.
        """
        # parameters
        mode = self.options.color_space
        rmode = self.options.random_mode
        range_a = self.options.range_alpha
        # color components
        if mode in ('sRGB', 'linearRGB'):
            self.randomize_rgb(ink_color, mode, rmode)
        elif mode in ('HSL', 'HSV'):
            self.randomize_hsl_hsv(ink_color, mode, rmode)
        elif mode in ('LUV', 'LAB', 'LCH'):
            self.randomize_luv_lab_lch(ink_color, mode, rmode)
        else:
            pass
        # alpha, opacity
        if range_a > 0:
            a = ink_color.alpha
            if not self.options.object_opacity:
                a = randomize_float_range(range_a, a)
            ink_color.alpha = a

    def randomize_color(self, ink_color):
        """Randomize color and alpha components of InkColor."""
        h_range = self.options.hue_range
        s_range = self.options.saturation_range
        l_range = self.options.lightness_range
        a_range = self.options.opacity_range
        if h_range or s_range or l_range or a_range:
            h, s, l, a = ink_color.rgba_to_hsla()
            if h_range > 0:
                h = randomize_float(h_range, h)
            if s_range > 0:
                s = randomize_float(s_range, s)
            if l_range > 0:
                l = randomize_float(l_range, l)
            if a_range > 0:
                if not self.options.object_opacity:
                    a = randomize_float(a_range, a)
            ink_color.from_hsla(h, s, l, a)

    def modify_opacity(self, opacity):
        """Modify object opacity of graphics elements."""
        if self.options.object_opacity:
            o_range = False
            if self.options.tab == '"randomize_tab"':
                o_range = self.options.opacity_range
            elif self.options.tab == '"randomize_custom_tab"':
                o_range = self.options.range_alpha
            if o_range and o_range > 0:
                return randomize_float(o_range, float(opacity))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"randomize_tab"':
            self.randomize_color(ink_color)
        elif self.options.tab == '"randomize_custom_tab"':
            self.randomize_custom(ink_color)
        elif self.options.tab == '"randomize_misc_tab"':
            self.randomize_misc(ink_color)

    # ----- pre-process: document/selection

    def prepare_doc(self):
        """Prepare common stuff."""
        if self.options.tab == '"randomize_misc_tab"':
            if self.options.presets.endswith('goldenratio'):
                self.goldenratio_val = random_float_seed()


if __name__ == '__main__':
    ME = ColorRandomize()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
