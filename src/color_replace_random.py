#!/usr/bin/env python
"""
color_replace_random - replace SVG paint color with random from palette

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import random
# local library
from color_lib import common as modcol
from pathmodifier import zSort


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


def randomize_from_palette(ink_color, rdict):
    """Replace current color with random color from palette."""
    if rdict:
        color_id = random.choice(list(rdict.keys()))
        ink_color.from_hex(rdict[color_id])


class ColorReplaceRandom(modcol.ColorRGBA):
    """ColorEffect-based class to replace SVG paint colors."""

    def __init__(self):
        """Init base class and ColorReplaceMany()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.palette_dict = {}

        # replace with random
        self.OptionParser.add_option("--record_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="record_alpha",
                                     default=False,
                                     help="Record alpha")

    # ----- modify current color

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"replace_random_tab"':
            # inspired by
            # http://www.inkscapeforum.com/viewtopic.php?t=4885
            # http://vectorboom.com/load/freebies/freescripts/randomswatchesfill/22-1-0-167
            randomize_from_palette(ink_color, self.palette_dict)

    # ----- pre-process: index colors in selected groups

    def record_palette_colors(self, node, rdict):
        """Record palette colors within *node* in dict *rdict*."""
        special_group = False
        for obj in node:
            source_id = obj.get('id', None)
            source_hex = modcol.hexcolor(obj)
            if source_hex is not None:
                if not self.options.record_alpha:
                    source_hex = source_hex[:-2]
                rdict[source_id] = source_hex
                special_group = True
        return special_group

    def record_selected(self, rdict):
        """Record new colors from top-most selected group in dict *rdict*."""
        if len(self.selected):
            deselect_list = []
            sorted_ids = zSort(self.document.getroot(), self.selected.keys())
            for node_id in reversed(sorted_ids):
                node = self.selected[node_id]
                if modcol.is_group(node) and len(node) >= 1:
                    if self.record_palette_colors(node, rdict):
                        old_id = node.get('id')
                        deselect_list.append(old_id)
                        if not old_id.startswith(modcol.BASE_ID):
                            node.set('id', self.uniqueId(modcol.BASE_ID))
                    break
            # deselect processed groups
            for obj_id in deselect_list:
                self.selected.pop(obj_id, None)

    # ----- pre-process: document/selection

    def prepare_doc(self):
        """Pre-process document to index color pairs."""
        # process selection for first (or top-most) palette group
        if self.options.tab == '"replace_random_tab"':
            self.record_selected(self.palette_dict)


if __name__ == '__main__':
    ME = ColorReplaceRandom()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
