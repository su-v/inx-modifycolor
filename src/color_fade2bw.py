#!/usr/bin/env python
"""
color_fade2bw - fade SVG paint colors to black or white

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorFadeToBW(modcol.ColorRGBA):
    """ColorEffect-based class to fade SVG paint colors to BW."""

    def __init__(self):
        """Init base class and ColorFadeToBW()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--fade2bw_level",
                                     action="store", type="float",
                                     dest="fade2bw_level", default="1.0",
                                     help="Fade level")
        self.OptionParser.add_option("--fade2bw_fadeto",
                                     action="store", type="string",
                                     dest="fade2bw_fadeto", default="black",
                                     help="Fade target")

    def fade_to_back_or_white(self, ink_color):
        """
        Fade to black or white.

        Based on CPF (Inkscape 0.91)

        feColorMatrix:

             black           white
            Lv 0  0  0 0    Lv 0  0  1-lv 0
            0  Lv 0  0 0    0  Lv 0  1-lv 0
            0  0  Lv 0 0    0  0  Lv 1-lv 0
            0  0  0  1 0    0  0  0  1    0

         """

        level = self.options.fade2bw_level

        if level < 1.0:

            if self.options.fade2bw_fadeto == "white":
                wlevel = 1.0 - level
            else:   # self.options.fade2bw_fadeto == "black"
                wlevel = 0.0

            mat = [[0.0 for _ in range(5)] for _ in range(5)]
            for i in range(3):
                mat[i][i] = level
                mat[i][3] = wlevel
                mat[i][4] = 0.0
            for i in (3, 4):
                mat[i][i] = 1.0

            # apply matrix
            r, g, b = ink_color.rgb_float()
            mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
            if mat_rgb_new:
                ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"fade2bw_tab"':
            self.fade_to_back_or_white(ink_color)


if __name__ == '__main__':
    ME = ColorFadeToBW()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
