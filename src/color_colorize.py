#!/usr/bin/env python
"""
color_colorize  - Colorize SVG paints in drawing or selection

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorops as co


class ColorColorize(modcol.ColorRGBA):
    """ColorEffect-based class to colorize SVG paints."""

    def __init__(self):
        """Init base class and ColorColorize()."""
        modcol.ColorRGBA.__init__(self)

        # instance attributes
        self.hsl_h = None
        self.hsl_s = None
        self.lightness = None

        # color from color selector
        self.OptionParser.add_option("--color",
                                     action="store", type="int",
                                     dest="color", default=1086308351,
                                     help="Selected color")
        self.OptionParser.add_option("--color_verbose",
                                     action="store", type="inkbool",
                                     dest="color_verbose", default=False,
                                     help="Display long int of color")
        # hue, saturation and lightness of selected color
        self.OptionParser.add_option("--hue",
                                     action="store", type="float",
                                     dest="hue", default=180.0,
                                     help="Hue")
        self.OptionParser.add_option("--saturation",
                                     action="store", type="float",
                                     dest="saturation", default=50.0,
                                     help="Saturation")
        self.OptionParser.add_option("--lightness",
                                     action="store", type="float",
                                     dest="lightness", default=0.0,
                                     help="Lightness/Value")
        self.OptionParser.add_option("--hsl_verbose",
                                     action="store", type="inkbool",
                                     dest="hsl_verbose", default=False,
                                     help="Display RGB value of color")
        # common
        self.OptionParser.add_option("--gamma_hack",
                                     action="store", type="inkbool",
                                     dest="gamma_hack", default=False,
                                     help="Gamma hack (temp)")

    def colorize(self, ink_color):
        """Colorize current color, preserving luminance."""

        # co.luminance() for linearRGB
        # lum = co.luminance(*ink_color.rgb_linear())

        # co.luma() rec601 for sRGB
        lum = co.luma(*ink_color.rgb_float(), mode='rec601')

        if self.lightness < 0:
            hsl_l = lum * (self.lightness + 1.0)
        elif self.lightness > 0:
            hsl_l = lum * (1.0 - self.lightness)
            hsl_l += 1.0 - (1.0 - self.lightness)
        else:
            hsl_l = lum

        # update ink_color
        ink_color.from_hsl(self.hsl_h, self.hsl_s, hsl_l)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if (self.options.tab == '"color_tab"' or
                self.options.tab == '"colorize_tab"'):
            self.colorize(ink_color)

    def cleanup_doc(self):
        """Display color information after color modifications."""
        if self.options.tab == '"color_tab"':
            if self.options.color_verbose:
                ic_rgb = ic.InkColorRGBALong(self.options.color)
                # output to stderr
                modcol.inkex.debug('long int: {}'.format(self.options.color))
                modcol.inkex.debug(ic_rgb.rgb_css('rgb'))
                modcol.inkex.debug(ic_rgb.rgb_css('hsl'))
        elif self.options.tab == '"colorize_tab"':
            if self.options.hsl_verbose:
                hsl_l = (self.lightness / 2.0) + 0.5
                ic_hsl = ic.InkColorHSL(self.hsl_h, self.hsl_s, hsl_l)
                # output to stderr
                modcol.inkex.debug('hex: {}'.format(ic_hsl.rgb_hex()))
                modcol.inkex.debug(ic_hsl.rgb_css('rgb'))
                modcol.inkex.debug(ic_hsl.rgb_css('hsl'))

    def prepare_doc(self):
        """Prepare common stuff used later on to modify current color."""
        if self.options.tab == '"color_tab"':
            ic_color = ic.InkColorRGBALong(self.options.color)
            self.hsl_h = ic_color.hsl_h
            self.hsl_s = ic_color.hsl_s
            self.lightness = (ic_color.hsl_l - 0.5) * 2.0
        elif self.options.tab == '"colorize_tab"':
            self.hsl_h = self.options.hue / 360.0
            self.hsl_s = self.options.saturation / 100.0
            self.lightness = self.options.lightness / 100.0


if __name__ == '__main__':
    ME = ColorColorize()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
