#!/usr/bin/env python
"""
color_fluorescence - oversaturate colors which can be fluorescent
                     in real world

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorOversaturate(modcol.ColorRGBA):
    """ColorEffect-based class to oversaturate values of SVG paint colors."""

    def __init__(self):
        """Init base class and ColorOversaturate()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--fluorescence_defaults",
                                     action="store", type="inkbool",
                                     dest="fluorescence_defaults",
                                     default=True,
                                     help="Use default values")
        self.OptionParser.add_option("--fluorescence_hue_rotate",
                                     action="store", type="int",
                                     dest="fluorescence_hue_rotate",
                                     default=0,
                                     help="Hue Rotate")
        self.OptionParser.add_option("--fluorescence_saturate",
                                     action="store", type="float",
                                     dest="fluorescence_saturate",
                                     default=1.0,
                                     help="Saturation")
        self.OptionParser.add_option("--fluorescence_cm_param1",
                                     action="store", type="float",
                                     dest="fluorescence_cm_param1",
                                     default=2,
                                     help="Matrix internal")
        self.OptionParser.add_option("--fluorescence_cm_param2",
                                     action="store", type="float",
                                     dest="fluorescence_cm_param2",
                                     default=-1,
                                     help="Matrix internal")
        self.OptionParser.add_option("--fluorescence_cm_param3",
                                     action="store", type="float",
                                     dest="fluorescence_cm_param3",
                                     default=0,
                                     help="Matrix internal")

    def oversaturate_color(self, ink_color):
        """
        Oversaturate RGB color values.

        Based on predefined filter (Inkscape 0.91)
        """

        # parameters
        if self.options.fluorescence_defaults:
            hue_rotation = 0
            saturation = 1
            cm_param1 = 2
            cm_param2 = -1
            cm_param3 = 0
        else:
            hue_rotation = self.options.fluorescence_hue_rotate
            saturation = self.options.fluorescence_saturate
            cm_param1 = self.options.fluorescence_cm_param1
            cm_param2 = self.options.fluorescence_cm_param2
            cm_param3 = self.options.fluorescence_cm_param3

        # 1. feColorMatrix: Shift
        #
        # <feColorMatrix type="hueRotate" values="0" />
        #
        if not (hue_rotation == 0 or hue_rotation == 360):
            r, g, b = ink_color.rgb_float()
            ink_color.from_float(*cm.hue_rotate(r, g, b, hue_rotation))

        # 2. feColorMatrix: Saturate
        #
        # <feColorMatrix type="saturate" values="1" />
        #
        if saturation != 1.0:
            r, g, b = ink_color.rgb_float()
            ink_color.from_float(*cm.saturate(r, g, b, saturation))

        # 3. feColorMatrix: Matrix
        #
        # <feColorMatrix type="matrix"
        #  values="2 -1 0 0 0 0 2 -1 0 0 -1 0 2 0 0 0 0 0 1 0 " />
        #
        mat = [[0.0 for _ in range(5)] for _ in range(5)]
        for i in (3, 4):
            mat[i][i] = 1.0

        # fill parameters
        for i in range(3):
            mat[i][(i+0) % 3] = float(cm_param1)
            mat[i][(i+1) % 3] = float(cm_param2)
            mat[i][(i+2) % 3] = float(cm_param3)

        # apply matrix
        r, g, b = ink_color.rgb_float()
        mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
        if mat_rgb_new is not None:
            ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"fluorescence_tab"':
            self.oversaturate_color(ink_color)


if __name__ == '__main__':
    ME = ColorOversaturate()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
