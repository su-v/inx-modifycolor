#!/usr/bin/env python
"""
color_blindness - simulate color blindness with SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Types of color blindness (from Inkscape CPF):

  Rod monochromacy (atypical achromatopsia)
  "0.618 0.32 0.062 0 0 0.163 0.775 0.062 0 0 0.163 0.32 0.516 0 0 0 0 0 1 0"

  Cone monochromacy (typical achromatopsia)
  "0.299 0.587 0.114 0 0 0.299 0.587 0.114 0 0 0.299 0.587 0.114 0 0 0 0 0 1 0"

  Green weak (deuteranomaly)
  "0.8 0.2 0 0 0 0.2583 0.74167 0 0 0 0 0.14167 0.85833 0 0 0 0 0 1 0"

  Green blind (deuteranopia)
  "0.625 0.375 0 0 0 0.7 0.3 0 0 0 0 0.3 0.7 0 0 0 0 0 1 0"

  Red weak (protanomaly)
  "0.8166 0.1833 0 0 0 0.333 0.666 0 0 0 0 0.125 0.875 0 0 0 0 0 1 0"

  Red blind (protanopia)
  "0.566 0.43333 0 0 0 0.55833 0.4416 0 0 0 0 0.24167 0.75833 0 0 0 0 0 1 0"

  Blue weak (tritanomaly)
  "0.966 0.033 0 0 0 0 0.733 0.266 0 0 0 0.1833 0.816 0 0 0 0 0 1 0"

  Blue blind (tritanopia)
  "0.95 0.05 0 0 0 0.2583 0.4333 0.5667 0 0 0 0.475 0.525 0 0 0 0 0 1 0"

"""
# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorBlindness(modcol.ColorRGBA):
    """ColorEffect-based class to simulate color blindness."""

    def __init__(self):
        """Init base class and ColorBlindness()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--type",
                                     action="store", type="string",
                                     dest="type", default="",
                                     help="Type of color blindness")

    def apply_colormatrix(self, ink_color):
        """
        Simulate color blindness with RGB color values.

        Based on CFP (Inkscape 0.91)
        """
        # convert feColorMtrix values string to matrix
        mat = cm.values_to_matrix(self.options.type)

        # apply matrix
        r, g, b = ink_color.rgb_float()
        mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
        if mat_rgb_new is not None:
            ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"blindness_tab"':
            self.apply_colormatrix(ink_color)


if __name__ == '__main__':
    ME = ColorBlindness()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
