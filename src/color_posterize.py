#!/usr/bin/env python
"""
color_posterize - posterize SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


class ColorPosterize(modcol.ColorRGBA):
    """ColorEffect-based class to posterize SVG paint colors."""

    def __init__(self):
        """Init base class and ColorPosterize()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--posterize_level",
                                     action="store", type="int",
                                     dest="posterize_level", default=6,
                                     help="Adjust Level for Posterize")
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="rgb_int",
                                     help="Mode (int- or float-based)")

    def rgb_int(self, ink_color):
        """Posterize RGB colors; based on RGB integer range 0-255."""

        # Based on (among others) e.g.
        # http://www.axiomx.com/posterize.htm

        # parameters
        num_levels = max(2, min(255, self.options.posterize_level))
        num_areas = 256.0 / num_levels
        num_values = 255.0 / (num_levels - 1)

        # calculate new values and update InkColor
        r, g, b = [int(num_values * int(c / num_areas))
                   for c in ink_color.rgb_int()]
        ink_color.from_int(r, g, b)

    def rgb_float(self, ink_color):
        """Posterize RGB colors; based on RGB float range 0.0-1.0."""

        # Based on GIMP/gegl code, e.g.
        # https://git.gnome.org/browse/gimp/tree/app/operations/gimpoperationposterize.c

        # parameters
        levels = self.options.posterize_level - 1.0

        # calculate new values and update InkColor
        r, g, b = [int(round(c * levels)) / levels
                   for c in ink_color.rgb_float()]
        ink_color.from_float(r, g, b)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"posterize_tab"':
            posterize = getattr(self, self.options.mode)
            posterize(ink_color)


if __name__ == '__main__':
    ME = ColorPosterize()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
