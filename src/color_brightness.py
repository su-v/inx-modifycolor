#!/usr/bin/env python
"""
color_brightness - adjust brightness of SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
from math import tan, pi

# local library
from color_lib import common as modcol


class ColorBrightness(modcol.ColorRGBA):
    """ColorEffect-based class to adjust brightness in SVG paint colors."""

    def __init__(self):
        """Init base class and ColorBrightness()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--br_brightness",
                                     action="store", type="int",
                                     dest="br_brightness",
                                     default=0,
                                     help="Adjust RGB Brightness")
        self.OptionParser.add_option("--co_contrast",
                                     action="store", type="int",
                                     dest="co_contrast",
                                     default=0,
                                     help="Adjust RGB contrast")
        self.OptionParser.add_option("--br_co_brightness",
                                     action="store", type="int",
                                     dest="br_co_brightness",
                                     default=0,
                                     help="Adjust RGB Brightness")
        self.OptionParser.add_option("--br_co_contrast",
                                     action="store", type="int",
                                     dest="br_co_contrast",
                                     default=0,
                                     help="Adjust RGB contrast")
        self.OptionParser.add_option("--br_co_brightness_gimp",
                                     action="store", type="int",
                                     dest="br_co_brightness_gimp",
                                     default=0,
                                     help="Adjust RGB Brightness (LUT)")
        self.OptionParser.add_option("--br_co_contrast_gimp",
                                     action="store", type="int",
                                     dest="br_co_contrast_gimp",
                                     default=0,
                                     help="Adjust RGB contrast (LUT)")
        self.OptionParser.add_option("--co_br_contrast_gegl",
                                     action="store", type="float",
                                     dest="co_br_contrast_gegl",
                                     default=1.0,
                                     help="Adjust RGB contrast (GEGL)")
        self.OptionParser.add_option("--co_br_brightness_gegl",
                                     action="store", type="float",
                                     dest="co_br_brightness_gegl",
                                     default=0.0,
                                     help="Adjust RGB Brightness (GEGL)")
        self.OptionParser.add_option("--co_br_gammahack_gegl",
                                     action="store", type="inkbool",
                                     dest="co_br_gammahack_gegl",
                                     default=False,
                                     help="Gamma hack (see GIMP)")
        self.OptionParser.add_option("--br_co_brightness_pixastic",
                                     action="store", type="int",
                                     dest="br_co_brightness_pixastic",
                                     default=0,
                                     help="Adjust RGB Brightness (Pixastic)")
        self.OptionParser.add_option("--br_co_contrast_pixastic",
                                     action="store", type="float",
                                     dest="br_co_contrast_pixastic",
                                     default=0.0,
                                     help="Adjust RGB contrast (Pixastic)")
        self.OptionParser.add_option("--br_co_brightness_pixastic_new",
                                     action="store", type="float",
                                     dest="br_co_brightness_pixastic_new",
                                     default=0.0,
                                     help="Adjust RGB Brightness (Pixastic)")
        self.OptionParser.add_option("--br_co_contrast_pixastic_new",
                                     action="store", type="float",
                                     dest="br_co_contrast_pixastic_new",
                                     default=0.0,
                                     help="Adjust RGB contrast (Pixastic)")
        self.OptionParser.add_option("--br_co_mode_pixastic",
                                     action="store", type="string",
                                     dest="br_co_mode_pixastic",
                                     default="new",
                                     help="Mode (Pixastic)")

    def brightness_contrast_gimp(self, ink_color):
        """Brightness & contrast combined, GIMP 2.9.3."""

        # Based on
        # https://git.gnome.org/browse/gimp/tree/app/operations/gimpoperationbrightnesscontrast.c

        if not (self.options.br_co_brightness_gimp == 0 and
                self.options.br_co_contrast_gimp == 0):

            midpo = 0.5
            shift = (self.options.br_co_brightness_gimp / 127.0) / 2.0
            # slant = tan((self.options.br_co_contrast_gimp + 1) * atan(1))
            slant = tan((self.options.br_co_contrast_gimp/127.0 + 1) * pi/4)

            r, g, b = ink_color.rgb_float()
            if shift < 0.0:
                # decrease brightness
                r, g, b = [c * (1 + shift)
                           for c in (r, g, b)]

            elif shift > 0.0:
                # increase brightness
                r, g, b = [c + ((1.0 - c) * shift)
                           for c in (r, g, b)]

            if self.options.br_co_contrast_gimp != 0:
                # adjust contrast
                r, g, b = [((c - midpo) * slant) + midpo
                           for c in (r, g, b)]
            ink_color.from_float(r, g, b)

    def contrast_brightness_gegl(self, ink_color):
        """Contrast & brightness combined, GEGL 0.3.8."""

        # Based on
        # https://git.gnome.org/browse/gegl/tree/operations/common/brightness-contrast.c

        if not (self.options.co_br_contrast_gegl == 1.0 and
                self.options.co_br_brightness_gegl == 0.0):

            midpo = 0.5
            slant = self.options.co_br_contrast_gegl
            shift = self.options.co_br_brightness_gegl

            # adjust contrast, brightness
            if self.options.co_br_gammahack_gegl:
                r, g, b = [((c - midpo) * slant) + shift + midpo
                           for c in ink_color.rgb_float()]
                ink_color.from_float(r, g, b)
            else:
                r, g, b = [((c - midpo) * slant) + shift + midpo
                           for c in ink_color.rgb_linear()]
                ink_color.from_linear(r, g, b)

    def brightness_contrast_pixastic(self, ink_color):
        """Brightness & contrast combined, Pixastic Lib.

        (legacy and old Pixastic mode is based on Photoshop CS)
        """
        # pylint: disable=invalid-name

        # legacy, old mode
        # docs: http://dph.am/pixastic-docs/docs/actions/brightness/
        # https://github.com/jseidelin/pixastic/blob/178dd7313b4d7c7dceec6a137467e304f0a1210f/actions/brightness.js

        # new mode
        # docs: -
        # https://github.com/jseidelin/pixastic/blob/0a41a80bcc2aadf082a14be5d2048a1face989f2/pixastic.effects.js#L399

        if self.options.br_co_mode_pixastic == 'legacy':
            brightness = self.options.br_co_brightness_pixastic
            contrast = self.options.br_co_contrast_pixastic
            if not (brightness == 0 and contrast == 0):
                # internal parameters
                contrast = max(0, contrast+1)
                brightness = min(150, max(-150, brightness))
                mul = contrast
                add = ((brightness - 128) * contrast) + 128
                # adjust contrast, brightness
                r, g, b = [int(round((c * mul) + add))
                           for c in ink_color.rgb_int()]
                # adjust contrast, brightness
                ink_color.from_int(r, g, b)
        elif self.options.br_co_mode_pixastic == 'old':
            brightness = self.options.br_co_brightness_pixastic
            contrast = self.options.br_co_contrast_pixastic
            if not (brightness == 0 and contrast == 0):
                # internal parameters
                contrast = max(0, contrast+1)
                brightMul = 1 + (min(150, max(-150, brightness)) / 150.0)
                mul = brightMul * contrast
                add = (-contrast * 128) + 128
                # adjust contrast, brightness
                r, g, b = [int(round((c * mul) + add))
                           for c in ink_color.rgb_int()]
                # adjust contrast, brightness
                ink_color.from_int(r, g, b)
        else:  # self.options.br_co_mode_pixastic == 'new':
            # input parameters
            brightness = 1 + self.options.br_co_brightness_pixastic_new
            contrast = self.options.br_co_contrast_pixastic_new / 2.0
            if not (brightness == 0 and contrast == 0):
                # internal parameters
                bMul = -brightness if brightness < 0 else brightness
                bAdd = 0 if brightness < 0 else brightness
                cont = 0.5 * tan((contrast + 1.0) * pi/4)
                cAdd = - (cont - 0.5) * 255
                # adjust contrast, brightness
                r, g, b = [int(round(((c + (c * bMul) + bAdd) * cont) + cAdd))
                           for c in ink_color.rgb_int()]
                # adjust contrast, brightness
                ink_color.from_int(r, g, b)

    def brightness(self, ink_color):
        """Brightness, Inkscape ColorEffect extensions."""

        # Based on
        # Inkscape's color effect 'color_brighter.py', 'color_darker.py'

        if self.options.br_brightness > 0:
            # brighter
            factor = (1 - (self.options.br_brightness / 100.0))
            threshold = (1.0 / (1.0 - factor)) / 255.0

            if ink_color.rgb_hex() == '#000000':
                # return threshold for black (see color_brighter.py)
                r = g = b = threshold
            else:
                # return color divided by brightness factor
                r, g, b = [factor and (threshold / factor if
                                       c < threshold else c / factor) or 1.0
                           for c in ink_color.rgb_float()]
            ink_color.from_float(r, g, b)

        elif self.options.br_brightness < 0:
            # darker
            factor = 1 + self.options.br_brightness/100.0

            # return color multipled with brightness factor
            r, g, b = [c * factor
                       for c in ink_color.rgb_float()]
            ink_color.from_float(r, g, b)

    def contrast(self, ink_color):
        """Contrast."""

        # *** disabled ***

        # Based on:
        # http://www.dfstudios.co.uk/articles/image-processing-algorithms-part-5/

        if self.options.co_contrast != 0:
            # less/more contrast
            contrast = 255 * self.options.co_contrast / 100.0
            factor = (259.0*(contrast + 255.0)) / (255.0*(259.0 - contrast))
            midpoint = 128

            # return
            r, g, b = [int(round(((c - midpoint) * factor) + midpoint))
                       for c in ink_color.rgb_int()]
            ink_color.from_int(r, g, b)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"brightness_contrast_gimp_tab"':
            self.brightness_contrast_gimp(ink_color)
        elif self.options.tab == '"contrast_brightness_gegl_tab"':
            self.contrast_brightness_gegl(ink_color)
        elif self.options.tab == '"brightness_contrast_pixastic_tab"':
            self.brightness_contrast_pixastic(ink_color)
        elif self.options.tab == '"brightness_tab"':
            self.brightness(ink_color)
        elif self.options.tab == '"contrast_tab"':
            self.contrast(ink_color)


if __name__ == '__main__':
    ME = ColorBrightness()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
