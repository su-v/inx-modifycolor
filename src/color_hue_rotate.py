#!/usr/bin/env python
"""
color_hue_rotate - rotate hue of SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorHueRotate(modcol.ColorRGBA):
    """ColorEffect-based class to rotate hue of SVG paint colors."""

    def __init__(self):
        """Init base class and ColorHueRotate()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--hue_rotate_value",
                                     action="store", type="float",
                                     dest="hue_rotate_value", default=0.0,
                                     help="Hue Rotate (angle)")
        self.OptionParser.add_option("--hue_rotate_mode",
                                     action="store", type="string",
                                     dest="hue_rotate_mode", default="rgb",
                                     help="Hue rotate mode (rgb|hsl)")

    def rotate_hue(self, ink_color):
        """Rotate hue of RGB-based color."""
        angle = float(self.options.hue_rotate_value)
        if angle > 0.0 and angle < 360.0:
            if self.options.hue_rotate_mode == 'rgb':
                r, g, b = ink_color.rgb_float()
                ink_color.from_float(*cm.hue_rotate(r, g, b, angle))
            elif self.options.hue_rotate_mode == 'hsl':
                ink_color.hsl_h_deg += angle

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"hue_rotate_tab"':
            self.rotate_hue(ink_color)


if __name__ == '__main__':
    ME = ColorHueRotate()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
