#!/usr/bin/env python
"""
color_blend - blend custom color over SVG paint color

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorblend as cb


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class ColorBlendOver(modcol.ColorRGBA):
    """ColorEffect-based class to blend custom color over SVG paint colors."""

    def __init__(self):
        """Init base class and ColorBlendOver()."""
        modcol.ColorRGBA.__init__(self)

        # blend options
        self.OptionParser.add_option("--blend_mode",
                                     action="store",
                                     type="string",
                                     dest="blend_mode",
                                     default="blend_normal",
                                     help="Blend mode")
        self.OptionParser.add_option("--blend_order",
                                     action="store",
                                     type="string",
                                     dest="blend_order",
                                     default="custom_over_current",
                                     help="Blend order")
        self.OptionParser.add_option("--blend_colorspace",
                                     action="store",
                                     type="string",
                                     dest="blend_colorspace",
                                     default="sRGB",
                                     help="Blend color space")
        self.OptionParser.add_option("--blend_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="blend_alpha",
                                     default=True,
                                     help="Update blended alpha")
        self.OptionParser.add_option("--blend_color",
                                     action="store",
                                     type="int",
                                     dest="blend_color",
                                     default=-16777088,
                                     help="Source blend color")
        self.OptionParser.add_option("--debug",
                                     action="store",
                                     type="inkbool",
                                     dest="debug",
                                     default=False,
                                     help="Debug output")

    def blend_color(self, ic_dst):
        """Blend current color with custom color, using blend order."""

        # Parameters
        mode = self.options.blend_mode
        source = self.options.blend_color
        order = self.options.blend_order
        alpha = self.options.blend_alpha

        # Blend color space
        linear = self.options.blend_colorspace == 'linearRGB'

        # Custom color for blend source or backdrop (depends on blend order)
        ic_src = ic.InkColorRGBALong(source)

        # blender functions
        blender = getattr(cb, mode[:mode.rfind('_')], None)
        func = getattr(cb, mode, None)

        # Blend custom color with current color (depends on blend order)
        if blender is not None and func is not None:
            if order == 'custom_over_current':
                ic_out = blender(ic_dst, ic_src, func, linear)
            else:
                ic_out = blender(ic_src, ic_dst, func, linear)
        else:
            ic_out = ic_dst

        # Update ic_dst with or without blended alpha
        if alpha:
            # showme('from {} to {}'.format(ic_dst.alpha, ic_out.alpha))
            ic_dst.from_float(*ic_out.rgba_float())
        else:
            ic_dst.from_float(*ic_out.rgb_float())

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"blend_tab"':
            self.blend_color(ink_color)


if __name__ == '__main__':
    ME = ColorBlendOver()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
