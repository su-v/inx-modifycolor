#!/usr/bin/env python
"""
Copyright (C) 2015

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name

# system modules
import copy
import math

# internal modules
import inkex
import simplestyle
import cubicsuperpath
import bezmisc
import simpletransform
import pathmodifier

# color interpolation
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorinterp as ci


# globals
DEBUG = False

STYLEDEFAULTS = {
    'opacity': '1.0',
    'stroke-opacity': '1.0',
    'fill-opacity': '1.0',
    'stroke-width': '1.0',
    'stroke': 'none',
    'fill': 'none'
}


# cubicsuperpath (csp) functions

def numsegs(csp):
    # pylint: disable=missing-docstring
    return sum([len(p)-1 for p in csp])


def interpcoord(v1, v2, p):
    # pylint: disable=missing-docstring
    return v1 + ((v2 - v1)*p)


def interppoints(p1, p2, p):
    # pylint: disable=missing-docstring
    return [interpcoord(p1[0], p2[0], p), interpcoord(p1[1], p2[1], p)]


def pointdistance(x1_y1, x2_y2):
    # pylint: disable=missing-docstring
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    return math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))


def bezlenapprx(sp1, sp2):
    # pylint: disable=missing-docstring
    return (pointdistance(sp1[1], sp1[2]) +
            pointdistance(sp1[2], sp2[0]) +
            pointdistance(sp2[0], sp2[1]))


def tpoint(x1_y1, x2_y2, t=0.5):
    # pylint: disable=missing-docstring
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    return [x1 + t*(x2 - x1), y1 + t*(y2 - y1)]


def cspbezsplit(sp1, sp2, t=0.5):
    # pylint: disable=missing-docstring
    m1 = tpoint(sp1[1], sp1[2], t)
    m2 = tpoint(sp1[2], sp2[0], t)
    m3 = tpoint(sp2[0], sp2[1], t)
    m4 = tpoint(m1, m2, t)
    m5 = tpoint(m2, m3, t)
    m = tpoint(m4, m5, t)
    return [[sp1[0][:], sp1[1][:], m1],
            [m4, m, m5],
            [m3, sp2[1][:], sp2[2][:]]]


def cspbezsplitatlength(sp1, sp2, l=0.5, tolerance=0.001):
    # pylint: disable=missing-docstring
    bez = (sp1[1][:], sp1[2][:], sp2[0][:], sp2[1][:])
    t = bezmisc.beziertatlength(bez, l, tolerance)
    return cspbezsplit(sp1, sp2, t)


def cspseglength(sp1, sp2, tolerance=0.001):
    # pylint: disable=missing-docstring
    bez = (sp1[1][:], sp1[2][:], sp2[0][:], sp2[1][:])
    return bezmisc.bezierlength(bez, tolerance)


def csplength(csp):
    # pylint: disable=missing-docstring
    total = 0
    lengths = []
    for sp in csp:
        lengths.append([])
        for i in range(1, len(sp)):
            l = cspseglength(sp[i-1], sp[i])
            lengths[-1].append(l)
            total += l
    return lengths, total


# interpolate functions

def tweenstylefloat(prop, start, end, time):
    """Tween a float for time between start and end value.

    Returns result as string (to be used as property value)."""
    sp = float(start[prop])
    ep = float(end[prop])
    return str(sp + (time * (ep - sp)))


def tweenstylecolor(prop, start, end, time):
    """Tween colors based on style of start and end path.

    Return hex string of RGB color."""
    sr, sg, sb = parsecolor(start[prop])
    er, eg, eb = parsecolor(end[prop])
    return '#%s%s%s' % (tweenhex(time, sr, er),
                        tweenhex(time, sg, eg),
                        tweenhex(time, sb, eb))


def tweenhex(time, s, e):
    """ Tween start and end color value (one channel, in hex) for time.

    Returns hex value for a single RGB channel.
    """
    s = float(int(s, 16))
    e = float(int(e, 16))
    retval = hex(int(math.floor(s + (time * (e - s)))))[2:]
    if len(retval) == 1:
        retval = '0%s' % retval
    return retval


def parsecolor(c):
    """Parse string *c* and split it into r, g, b hex strings."""
    r, g, b = '0', '0', '0'
    if c[:1] == '#':
        if len(c) == 4:
            r, g, b = c[1:2], c[2:3], c[3:4]
        elif len(c) == 7:
            r, g, b = c[1:3], c[3:5], c[5:7]
    return r, g, b


def divide_into_min_segments(start, end):
    """Divide path, method 1

    Modify pathdata list for start and end path in-place
    """
    # which path has fewer segments?
    lengthdiff = numsegs(start) - numsegs(end)
    # swap shortest first
    if lengthdiff > 0:
        start, end = end, start
    # subdivide the shorter path
    for _ in range(abs(lengthdiff)):
        maxlen = 0
        subpath = 0
        segment = 0
        for y, _ in enumerate(start):
            for z in range(1, len(start[y])):
                leng = bezlenapprx(start[y][z-1], start[y][z])
                if leng > maxlen:
                    maxlen = leng
                    subpath = y
                    segment = z
        sp1, sp2 = start[subpath][segment - 1:segment + 1]
        start[subpath][segment - 1:segment + 1] = cspbezsplit(sp1, sp2)
    # if swapped, swap them back
    if lengthdiff > 0:
        start, end = end, start


def divide_into_equal_lengths(start, end):
    """Divide path, method 2

    Subdivide both paths into segments of relatively equal lengths.
    Return new pathdata list for start and end path
    """
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-branches

    slengths, stotal = csplength(start)
    elengths, etotal = csplength(end)
    lengths = {}
    t = 0
    for sp in slengths:
        for l in sp:
            t += l / stotal
            lengths.setdefault(t, 0)
            lengths[t] += 1
    t = 0
    for sp in elengths:
        for l in sp:
            t += l / etotal
            lengths.setdefault(t, 0)
            lengths[t] += -1
    sadd = [k for (k, v) in lengths.items() if v < 0]
    sadd.sort()
    eadd = [k for (k, v) in lengths.items() if v > 0]
    eadd.sort()
    t = 0
    s = [[]]
    for sp in slengths:
        if not start[0]:
            s.append(start.pop(0))
        s[-1].append(start[0].pop(0))
        for l in sp:
            pt = t
            t += l / stotal
            if sadd and t > sadd[0]:
                while sadd and sadd[0] < t:
                    nt = (sadd[0] - pt) / (t - pt)
                    bezes = cspbezsplitatlength(s[-1][-1][:],
                                                start[0][0][:],
                                                nt)
                    s[-1][-1:] = bezes[:2]
                    start[0][0] = bezes[2]
                    pt = sadd.pop(0)
            s[-1].append(start[0].pop(0))
    t = 0
    e = [[]]
    for sp in elengths:
        if not end[0]:
            e.append(end.pop(0))
        e[-1].append(end[0].pop(0))
        for l in sp:
            pt = t
            t += l / etotal
            if eadd and t > eadd[0]:
                while eadd and eadd[0] < t:
                    nt = (eadd[0] - pt) / (t - pt)
                    bezes = cspbezsplitatlength(e[-1][-1][:],
                                                end[0][0][:],
                                                nt)
                    e[-1][-1:] = bezes[:2]
                    end[0][0] = bezes[2]
                    pt = eadd.pop(0)
            e[-1].append(end[0].pop(0))
    return s[:], e[:]


def equalize_segments(start, end):
    """Break paths into corresponding subpaths with equal number of segments.

    Return new pathdata list for start and end path.
    """
    s = [[]]
    e = [[]]
    while start and end:
        if start[0] and end[0]:
            s[-1].append(start[0].pop(0))
            e[-1].append(end[0].pop(0))
        elif end[0]:
            s.append(start.pop(0))
            e[-1].append(end[0][0])
            e.append([end[0].pop(0)])
        elif start[0]:
            e.append(end.pop(0))
            s[-1].append(start[0][0])
            s.append([start[0].pop(0)])
        else:
            s.append(start.pop(0))
            e.append(end.pop(0))
    return s, e


def prepare_pair_pathdata(start, end, method=1):
    """Prepare path data for interpolation.

    Return new pathdata list for start and end path.
    """
    if method == 2:
        start, end = divide_into_equal_lengths(start, end)
    else:
        divide_into_min_segments(start, end)
    return equalize_segments(start, end)


def get_interp_path(s, e, time):
    """Return interpolation of path data at *time*."""
    interp = []
    # process subpaths
    for ssp, esp in zip(s, e):
        if not (ssp or esp):
            break
        interp.append([])
        # process superpoints
        for sp, ep in zip(ssp, esp):
            if not (sp or ep):
                break
            interp[-1].append([])
            # process points
            for p1, p2 in zip(sp, ep):
                if not (sp or ep):
                    break
                interp[-1][-1].append(interppoints(p1, p2, time))
    # remove final subpath if empty.
    if not interp[-1]:
        del interp[-1]
    return interp


def check_style(start_style, end_style):
    """Test for supported 'stroke' and 'fill' values.

    Return flags for stroke and fill
    """

    def isnotplain(sprop):
        """Inline helper function to check style property value."""
        return True if not (sprop == 'none' or sprop[:1] == "#") else False

    stroke_ok = True
    fill_ok = True
    if (isnotplain(start_style['stroke']) or
            isnotplain(end_style['stroke']) or
            (start_style['stroke'] == 'none' and
             end_style['stroke'] == 'none')):
        stroke_ok = False
    if (isnotplain(start_style['fill']) or
            isnotplain(end_style['fill']) or
            (start_style['fill'] == 'none' and
             end_style['fill'] == 'none')):
        fill_ok = False
    return stroke_ok, fill_ok


class BlendPaths(inkex.Effect):
    """inkex.Effect-based class to interpolate SVG paths and their styles."""

    def __init__(self):
        """Init base class and BlendPaths()."""
        inkex.Effect.__init__(self)

        self.sorted_ids = []
        self.defs = {}
        self.paths = {}
        self.styles = {}
        self.interp_style = False

        # geometry options
        self.OptionParser.add_option("-e", "--exponent",
                                     action="store", type="float",
                                     dest="exponent", default=0.0,
                                     help="Values other than zero " +
                                     "give non linear interpolation")
        self.OptionParser.add_option("-s", "--steps",
                                     action="store", type="int",
                                     dest="steps", default=5,
                                     help="Number of interpolation steps")
        self.OptionParser.add_option("-m", "--method",
                                     action="store", type="string",
                                     dest="method", default="2",
                                     help="Method of interpolation")
        self.OptionParser.add_option("-d", "--dup",
                                     action="store", type="inkbool",
                                     dest="dup", default=True,
                                     help="Duplicate endpaths")
        self.OptionParser.add_option("--zsort",
                                     action="store", type="inkbool",
                                     dest="zsort", default=True,
                                     help="Sort objects in Z-order")
        self.OptionParser.add_option("--reverse",
                                     action="store", type="inkbool",
                                     dest="reverse", default=False,
                                     help="reverse order")
        # style options
        self.OptionParser.add_option("--interp_strokewidth",
                                     action="store", type="inkbool",
                                     dest="interp_strokewidth", default=True,
                                     help="Interpolate stroke widths")
        self.OptionParser.add_option("--interp_opacity",
                                     action="store", type="inkbool",
                                     dest="interp_opacity", default=True,
                                     help="Interpolate object opacity")
        self.OptionParser.add_option("--interp_rgba_fill",
                                     action="store", type="inkbool",
                                     dest="interp_rgba_fill", default=True,
                                     help="Interpolate fill colors")
        self.OptionParser.add_option("--interp_rgba_stroke",
                                     action="store", type="inkbool",
                                     dest="interp_rgba_stroke", default=True,
                                     help="Interpolate stroke colors")
        self.OptionParser.add_option("--color_method",
                                     action="store", type="string",
                                     dest="color_method", default="linear",
                                     help="Interpolation method for colors")
        self.OptionParser.add_option("--color_space",
                                     action="store", type="string",
                                     dest="color_space", default="sRGB",
                                     help="Color space for interpolation")
        self.OptionParser.add_option("--hue_sweep",
                                     action="store", type="string",
                                     dest="hue_sweep", default="min",
                                     help="Hue sweep angle")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    def debug_sel(self):
        """Debug selection order."""
        list_ids = []
        for id_ in self.options.ids:
            list_ids.append(id_)
        inkex.debug('self.options.ids (list):\t{}'.format(list_ids))

        list_selected = []
        for id_, _ in self.selected.iteritems():
            list_selected.append(id_)
        inkex.debug('self.selected (dict):\t{}'.format(list_selected))

    def set_defs(self):
        """Return <defs> element, create one if none found."""
        self.defs = self.xpathSingle('/svg:svg//svg:defs')
        if self.defs is None:
            self.defs = inkex.etree.SubElement(
                self.document.getroot(), inkex.addNS('defs', 'svg'))

    def set_style_check(self):
        """Check style options, and set style switch instance attribute."""
        if (self.options.interp_strokewidth or
                self.options.interp_opacity or
                self.options.interp_rgba_fill or
                self.options.interp_rgba_stroke):
            self.interp_style = True

    def get_exponent(self):
        """Calculate effective exponent based on user input."""
        exponent = self.options.exponent
        if exponent >= 0:
            return 1.0 + exponent
        else:
            return 1.0/(1.0 - exponent)

    def get_steps(self, exponent):
        """Prepare interpolation steps based on chosen exponent."""
        steps = [1.0/(self.options.steps + 1.0)]
        for _ in range(self.options.steps - 1):
            steps.append(steps[0] + steps[-1])
        if self.options.dup:
            steps = [0] + steps + [1]
        return [step**exponent for step in steps]

    def build_lists(self):
        """Build lists for path data and style of all selected paths."""
        if self.options.zsort:
            # work around reversed selection order with Live Preview cycles
            self.sorted_ids = pathmodifier.zSort(self.document.getroot(),
                                                 self.selected.keys())
        else:
            # use selection order (legacy mode)
            self.sorted_ids = self.options.ids

        if self.options.reverse:
            self.sorted_ids.reverse()

        for id_ in self.sorted_ids:
            node = self.selected[id_]
            if node.tag == inkex.addNS('path', 'svg'):
                self.paths[id_] = cubicsuperpath.parsePath(node.get('d'))
                self.styles[id_] = simplestyle.parseStyle(node.get('style'))
                trans = node.get('transform')
                if trans:
                    simpletransform.applyTransformToPath(
                        simpletransform.parseTransform(trans),
                        self.paths[id_])
            else:
                self.sorted_ids.remove(id_)

    def resolve_paint_server(self, href, kind='named_color'):
        """Recursively process paint server href to retrieve definition."""
        val = None
        paint_server = self.getElementById(href)
        xlink = inkex.addNS('href', 'xlink')
        href_new = paint_server.get(xlink, '')
        if href_new.startswith('#'):
            # has chain-linked resource
            val = self.resolve_paint_server(href_new[1:], kind)
        else:
            # check depending on kind of paint server
            if kind == 'named_color':  # named colors
                if modcol.is_named_color(paint_server):
                    if modcol.is_solid_color(paint_server):
                        val = modcol.hexcolor(paint_server, 'solid-color')
                    elif modcol.is_osb_paint(paint_server, 'solid'):
                        val = modcol.hexcolor(paint_server[0], 'stop-color')
            elif kind == 'gradient':
                pass  # not supported by color_interpolate.py
            elif kind == 'pattern':
                pass  # not supported by color_interpolate.py
        return val

    def get_named_colors(self, sdict):
        """Resolve paint servers for named colors."""
        # fill
        props = ('fill', 'stroke')
        for key, val in dict(sdict).items():
            if key in props and val.startswith('url(#'):
                paint_link = val[len('url(#'):val.find(')')]
                hex_color = self.resolve_paint_server(paint_link)
                if hex_color is not None:
                    if len(hex_color) >= 7:
                        # set prop color
                        sdict[key] = hex_color[:7]
                    if len(hex_color) == 9:
                        # set prop alpha
                        key_alpha = '{}-opacity'.format(key)
                        alpha = int(hex_color[-2:], 16) / 255.0
                        new_alpha = float(sdict.get(key_alpha, 1.0)) * alpha
                        sdict[key_alpha] = new_alpha

    def prepare_pair_style(self, start_style, end_style):
        """Prepare styles of a pair of paths.

        Modify style dict values for start and end path in-place.
        Return flags for fill and stroke.
        """
        # fill in missing default values
        for key in STYLEDEFAULTS.keys():
            start_style.setdefault(key, STYLEDEFAULTS[key])
            end_style.setdefault(key, STYLEDEFAULTS[key])
        # resolve linked solid named colors to hex values
        for sdict in (start_style, end_style):
            self.get_named_colors(sdict)
        # check for supported style property values
        stroke_ok, fill_ok = check_style(start_style, end_style)
        # prepare fading to 'none'
        if stroke_ok:
            if start_style['stroke'] == 'none':
                start_style['stroke-width'] = '0.0'
                start_style['stroke-opacity'] = '0.0'
                start_style['stroke'] = end_style['stroke']
            elif end_style['stroke'] == 'none':
                end_style['stroke-width'] = '0.0'
                end_style['stroke-opacity'] = '0.0'
                end_style['stroke'] = start_style['stroke']
        if fill_ok:
            if start_style['fill'] == 'none':
                start_style['fill-opacity'] = '0.0'
                start_style['fill'] = end_style['fill']
            elif end_style['fill'] == 'none':
                end_style['fill-opacity'] = '0.0'
                end_style['fill'] = start_style['fill']
        return stroke_ok, fill_ok

    def tweenstylecolor(self, prop, start, end, time):
        """Tween InkColor objects based on style of start and end path."""
        ic_start = ic.InkColorRGBAHex(start[prop])
        ic_end = ic.InkColorRGBAHex(end[prop])
        return self.tweeninkcolor(ic_start, ic_end, time)

    def tweeninkcolor(self, ic_start, ic_end, time):
        """Tween InkColor objects at *time* based on color mode."""
        sweep = self.options.hue_sweep
        colorspace = self.options.color_space
        color_interp = self.options.color_method
        func = getattr(ci, '{}_ic'.format(color_interp), None)
        if func is not None:
            ic_new = func(ic_start, ic_end, time, colorspace, sweep)
        else:
            ic_new = ic_start.copy()
        return ic_new.rgb_hex()

    def tweenstyleunit(self, prop, start, end, time):
        """Tween stroke width in document units."""
        scale = self.unittouu('1px')
        sp = self.unittouu(start.get(prop, '1px')) / scale
        ep = self.unittouu(end.get(prop, '1px')) / scale
        return str(sp + (time * (ep - sp)))

    def get_interp_style(self, stroke_ok, fill_ok, sst, est, basestyle, time):
        """Basic style tweening at *time*.

        Modify the values of basestyle dict in-place.
        """
        # pylint: disable=too-many-arguments
        if self.options.interp_opacity:
            basestyle['opacity'] = tweenstylefloat('opacity', sst, est, time)
        if self.options.interp_strokewidth:
            basestyle['stroke-width'] = self.tweenstyleunit(
                'stroke-width', sst, est, time)
        if self.options.interp_rgba_stroke:
            if stroke_ok:
                basestyle['stroke-opacity'] = tweenstylefloat(
                    'stroke-opacity', sst, est, time)
                basestyle['stroke'] = self.tweenstylecolor(
                    'stroke', sst, est, time)
        if self.options.interp_rgba_fill:
            if fill_ok:
                basestyle['fill-opacity'] = tweenstylefloat(
                    'fill-opacity', sst, est, time)
                basestyle['fill'] = self.tweenstylecolor(
                    'fill', sst, est, time)

    def draw_interp_step(self, parent, s, e, stroke_ok, fill_ok,
                         sst, est, basestyle, time):
        """Append path interpolated at *time* to *parent*."""
        # pylint: disable=too-many-arguments
        interp = get_interp_path(s, e, time)
        if self.interp_style:
            self.get_interp_style(
                stroke_ok, fill_ok, sst, est, basestyle, time)
        attribs = {
            'style': simplestyle.formatStyle(basestyle),
            'd': cubicsuperpath.formatPath(interp)
        }
        return inkex.etree.SubElement(
            parent, inkex.addNS('path', 'svg'), attribs)

    def interpolate_pair(self, i, steps):
        """Interpolate path data, style of a pair of selected paths."""
        # pylint: disable=too-many-locals
        stroke_ok = fill_ok = False
        # get IDs
        start_id = self.sorted_ids[i-1]
        end_id = self.sorted_ids[i]
        # get path data
        start_d = copy.deepcopy(self.paths[start_id])
        end_d = copy.deepcopy(self.paths[end_id])
        # get style dict
        start_style = copy.deepcopy(self.styles[start_id])
        end_style = copy.deepcopy(self.styles[end_id])
        base_style = copy.deepcopy(start_style)
        if 'stroke-width' in base_style.keys():
            base_style['stroke-width'] = self.tweenstyleunit(
                'stroke-width', start_style, end_style, 0)
        # inkex.debug(base_style)
        # prepare for style tweening
        if self.interp_style:
            stroke_ok, fill_ok = self.prepare_pair_style(start_style,
                                                         end_style)
        # prepare for path tweening
        start_data, end_data = prepare_pair_pathdata(
            start_d, end_d, int(self.options.method))
        # create group for interpolated paths (per pair)
        group = inkex.etree.SubElement(
            self.current_layer, inkex.addNS('g', 'svg'))
        # create an interpolated path for each interval
        for time in steps:
            self.draw_interp_step(
                group,
                start_data, end_data,
                stroke_ok, fill_ok, start_style, end_style, base_style,
                time)
        # TODO: hook up export routine
        # create tar ball or ZIP file for all elements of 'group'
        # for animation
        # Export extension: https://gitlab.com/su-v/inx-exportobjects

    def effect(self):
        """Init lists for path data and style of selected paths."""
        # compat with 0.48
        if not hasattr(self, 'unittouu'):
            self.unittouu = inkex.unittouu  # pylint: disable=no-member
        # defs
        self.set_defs()
        # check options for style properties
        self.set_style_check()
        # init orig options
        steps = self.get_steps(self.get_exponent())
        # create lists of path descriptions and styles (per id)
        self.build_lists()
        # main loop (in pairs of two selected objects)
        for i in range(1, len(self.sorted_ids)):
            self.interpolate_pair(i, steps)


if __name__ == '__main__':
    my_effect = BlendPaths()
    my_effect.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
