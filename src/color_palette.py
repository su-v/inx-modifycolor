#!/usr/bin/env python
"""
color_palette - create palette of unique used SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


IDEAS:

    - option to convert used colors to custom swatches
      (named colors in 'Auto'-palette)
      -> see 'Modify Color > Manage > Named Colors'

"""
# local library
import inkex
from color_lib import common as modcol
from color_lib import util
from color_lib import inkcolor as ic
from color_lib import colorsort as cs


def create_rect(x, y, width, height, style):
    """Return a new SVG rect element with style from string *style*."""
    rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    rect.set('x', str(x))
    rect.set('y', str(y))
    rect.set('width', str(width))
    rect.set('height', str(height))
    rect.set('style', style)
    return rect


def create_square(x, y, width, style):
    """Return a square (SVG rect with equal width and height)."""
    return create_rect(x, y, width, width, style)


class ColorPalette(modcol.ColorRGBA):
    """ColorEffect-based class to render a palette of used colors."""

    def __init__(self):
        """Init base class and ColorPalette()."""
        modcol.ColorRGBA.__init__(self)

        # palette
        self.OptionParser.add_option("--palette",
                                     action="store",
                                     type="inkbool",
                                     dest="palette",
                                     default=True,
                                     help="Create color palette")
        self.OptionParser.add_option("--mode",
                                     action="store",
                                     type="string",
                                     dest="mode",
                                     default="RGB",
                                     help="Color mode (RGBA, RGB)")
        self.OptionParser.add_option("--count",
                                     action="store",
                                     type="int",
                                     dest="count",
                                     default=0,
                                     help="Most common count or all")
        self.OptionParser.add_option("--sort",
                                     action="store",
                                     type="string",
                                     dest="sort",
                                     default="most_used",
                                     help="Color sort order")
        self.OptionParser.add_option("--reverse",
                                     action="store",
                                     type="inkbool",
                                     dest="reverse",
                                     default=False,
                                     help="Reverse sort order")
        self.OptionParser.add_option("--swatch_size",
                                     action="store",
                                     type="int",
                                     dest="swatch_size",
                                     default=18,
                                     help="Approx. swatch size")
        self.OptionParser.add_option("--swatch_gap",
                                     action="store",
                                     type="int",
                                     dest="swatch_gap",
                                     default=2,
                                     help="Approx. swatch gap")
        self.OptionParser.add_option("--columns",
                                     action="store",
                                     type="int",
                                     dest="columns",
                                     default=0,
                                     help="Columns count, 0 = page width")
        self.OptionParser.add_option("--direction",
                                     action="store",
                                     type="string",
                                     dest="direction",
                                     default="horizontal",
                                     help="Layout of palette")
        self.OptionParser.add_option("--position",
                                     action="store",
                                     type="string",
                                     dest="position",
                                     default="top_left",
                                     help="Position of palette")
        self.OptionParser.add_option("--wrap_group",
                                     action="store",
                                     type="inkbool",
                                     dest="wrap_group",
                                     default=False,
                                     help="Wrap swatches in group")

    def counted_colors(self):
        """Return counted colors list based on user input."""
        # counted colors based on color mode
        if self.options.mode == 'RGBA':
            counted = modcol.Counter(self.old_colors)
        else:  # if self.options.mode == 'RGB':
            counted = modcol.Counter(c[:-2] for c in self.old_colors)
        # limit counted colors based on user option
        count = self.options.count or None
        return [color[0] for color in counted.most_common(count)]

    def sort_colors(self, colors):
        """Return sorted colors list based on user input."""
        prop = self.options.sort
        reverse = self.options.reverse
        out_method = None
        if prop.startswith('rgb'):
            out_method = 'rgba_float'
        elif prop.startswith('hsl') or prop.startswith('hsv'):
            out_method = 'rgba_to_{}a'.format(prop[:3])
        elif (prop.startswith('lab') or prop.startswith('lch') or
              prop.startswith('luv')):
            cs.sort_colors_by_cie(colors, prop, reverse)
        if out_method is not None:
            cs.sort_colors_by_ic(colors, prop, out_method, reverse)

    def swatch_size(self, swatch):
        """Set *swatch* size, return palette spacing based on user input."""
        width = int(self.unittouu('{0}px'.format(self.options.swatch_size)))
        gap = int(self.unittouu('{0}px'.format(self.options.swatch_gap)))
        swatch['width'] = width
        return width + gap, gap

    def palette_size(self, colors, spacing, gap):
        """Return palette size based on colors, spacing and user input."""
        if self.options.direction == 'horizontal':
            max_len = self.unittouu(self.getDocumentWidth()) + gap
        else:
            max_len = self.unittouu(self.getDocumentHeight()) + gap
        columns = self.options.columns or int(max_len / spacing)
        rows = util.roundup(len(colors), columns) / columns
        return columns, rows

    def draw_palette(self, colors=None):
        """Draw palette with unique RGB(A) colors in use."""
        # create layer group for palette
        group = self.special_group('Palette', self.options.wrap_group)
        # get counted colors
        if colors is None:
            colors = self.counted_colors()
        # sort colors
        self.sort_colors(colors)
        # init swatch dict
        swatch = {}  # used keys: x, y, width, style
        # set swatch dimension, get palette spacing
        spacing, gap = self.swatch_size(swatch)
        # get palette dimensions
        columns, rows = self.palette_size(colors, spacing, gap)
        # set direction
        row_pos = 'x' if self.options.direction == 'horizontal' else 'y'
        col_pos = 'y' if self.options.direction == 'horizontal' else 'x'

        # set values before initial swatch
        row = 1
        swatch[row_pos] = -spacing
        swatch[col_pos] = 0

        # iterate through all colors and create swatch for each
        ink_color = ic.InkColor()
        for i, color in enumerate(colors):
            if i < row * columns:
                swatch[row_pos] += spacing
            else:
                row += 1
                swatch[row_pos] = 0
                swatch[col_pos] += spacing
            ink_color.from_hex(color)
            swatch['style'] = ink_color.rgba_to_style_str(prop='fill')
            group.append(create_square(**swatch))

        # adjust palette group as needed
        if self.options.direction == 'horizontal':
            if self.options.position == 'top_left':  # top
                transform = 'translate(0,-{})'.format(rows * spacing)
            else:  # bottom
                transform = 'translate(0,{})'.format(
                    self.unittouu(self.getDocumentHeight()) + gap)
        else:
            if self.options.position == 'top_left':  # left
                transform = 'translate(-{},0)'.format(rows * spacing)
            else:  # right
                transform = 'translate({},0)'.format(
                    self.unittouu(self.getDocumentWidth()) + gap)
        group.set('transform', transform)

    def process_allcolors(self):
        """Create palette of all unique RGB(A) colors used in document."""
        if self.options.tab == '"palette_tab"':
            self.draw_palette()
        if self.options.stats:
            count = self.options.count or None
            self.color_stats(mode=self.options.mode, count=count)

    def prepare_doc(self):
        """Set option for color_palette (no color mods, no forks)."""
        self.options.track_only = True


if __name__ == '__main__':
    ME = ColorPalette()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
