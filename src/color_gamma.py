#!/usr/bin/env python
"""
color_gamma     - adjust relative gamma correction (overall brightness)

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math  # pow

# local library
from color_lib import common as modcol


def encode(val, gamma):
    """Encode gamma."""
    return math.pow(val, 1.0 / gamma)


def decode(val, gamma):
    """Decode gamma."""
    return math.pow(val, gamma)


class ColorGamma(modcol.ColorRGBA):
    """ColorEffect-based class to simulate gamma correction."""

    def __init__(self):
        """Init base class and ColorGamma()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--gamma",
                                     action="store", type="float",
                                     dest="gamma",
                                     default=0,
                                     help="Relative gamma change")
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode",
                                     default="encode",
                                     help="Encode or decode gamma")

    def apply_gamma(self, ink_color):
        """Apply gamma factor to current color."""

        # parameters
        gamma = 1.0 + self.options.gamma
        mode = self.options.mode

        # get current sRGB values
        r, g, b = ink_color.rgb_float()

        if mode == 'encode':
            # less contrast (lighter)
            ink_color.from_float(*[encode(v, gamma) for v in (r, g, b)])
        elif mode == 'decode':
            # more contrast (darker)
            ink_color.from_float(*[decode(v, gamma) for v in (r, g, b)])

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"gamma_tab"':
            self.apply_gamma(ink_color)


if __name__ == '__main__':
    ME = ColorGamma()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
