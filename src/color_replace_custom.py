#!/usr/bin/env python
"""
color_replace_custom - replace SVG paint color with other

Copyright (C) 2012, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


TODO:

    - consider adding external dependency e.g. on python-colormath for
      color conversions and color differences:
      http://python-colormath.readthedocs.org/


IDEAS:

    - Adjust target color by the relative distance of the original color
      within the match threshold.

"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colordiff as cd


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


class ColorReplace(modcol.ColorRGBA):
    """ColorEffect-based class to replace SVG paint colors."""

    def __init__(self):
        """Init base class and ColorReplace()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("-f", "--from_color",
                                     action="store",
                                     type="int",
                                     dest="from_color",
                                     default="-1",
                                     help="Replace color")
        self.OptionParser.add_option("-t", "--to_color",
                                     action="store",
                                     type="int",
                                     dest="to_color",
                                     default="255",
                                     help="By color")
        self.OptionParser.add_option("--match_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="match_alpha",
                                     default=True,
                                     help="Match including alpha")
        self.OptionParser.add_option("--update_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="update_alpha",
                                     default=True,
                                     help="Replace including alpha")
        self.OptionParser.add_option("--threshold",
                                     action="store",
                                     type="float",
                                     dest="threshold",
                                     default=0.0,
                                     help="Threshold (for source color)")
        self.OptionParser.add_option("--exclude_match",
                                     action="store",
                                     type="inkbool",
                                     dest="exclude_match",
                                     default=True,
                                     help="Exclude matching colors")
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="verbose",
                                     default=False,
                                     help="Verbose output for threshold > 0")
        self.OptionParser.add_option("--result",
                                     action="store",
                                     type="string",
                                     dest="result",
                                     default="default",
                                     help="Verbose output result")
        self.OptionParser.add_option("--debug",
                                     action="store",
                                     type="inkbool",
                                     dest="debug",
                                     default=False,
                                     help="Debug output")

    def verbose_diff(self, ic_src, ic_cur, delta, match):
        """Show details about color match with threshold."""
        # general options
        result = self.options.result
        threshold = self.options.threshold
        # prepare variable strings
        delta_c = 'dE94: {:7.3f}'.format(delta['c'])
        delta_a = 'dA: {:6.2f}'.format(delta['a'])
        # RGB
        src = 'SRC: {}'.format(ic_src.rgb_hex().upper())
        cur = 'CUR: {}'.format(ic_cur.rgb_hex().upper())
        # RGBA
        srca = 'SRC: {}'.format(ic_src.rgba_hex().upper())
        cura = 'CUR: {}'.format(ic_cur.rgba_hex().upper())
        # strings
        delim = '\t'
        msg = ''
        # concatenate strings based on requested result
        if result == 'default':
            msg = delim.join([cura, srca, delta_c, delta_a, str(match)])
        elif result == 'c_match' and delta['c'] <= threshold:
            msg = delim.join([cur, src, delta_c])
            # msg = 'LAB1: {}\nLAB2: {}\ndE94: {:7.3f}\n'.format(
            #     cd.xyz_to_lab(*cd.rgb_to_xyz(*ic_src.rgb_float())),
            #     cd.xyz_to_lab(*cd.rgb_to_xyz(*ic_cur.rgb_float())),
            #     delta['c'])
        elif result == 'c_fail' and delta['c'] > threshold:
            msg = delim.join([cur, src, delta_c])
        elif result == 'ca_match' and match:
            msg = delim.join([cura, srca, delta_c, delta_a])
        elif result == 'ca_fail' and not match:
            msg = delim.join([cura, srca, delta_c, delta_a])
        elif result == 'a_match' and delta['a'] <= threshold:
            msg = delim.join([cura, srca, delta_a])
        elif result == 'a_fail' and delta['a'] > threshold:
            msg = delim.join([cura, srca, delta_a])
        # return message with result
        if msg:
            showme(msg)

    def match_colors(self, source_color, ink_color):
        """Match two InkColor objects based on user options."""
        match = False
        if self.options.threshold == 0.0:
            # Compare exact match without or with alpha
            if not self.options.match_alpha:
                if ink_color.rgb_int() == source_color.rgb_int():
                    match = True
            else:
                if ink_color == source_color:
                    match = True
        elif self.options.threshold > 0:
            # Compare source color with threshold, alpha separately
            delta = {}
            delta['c'] = cd.color_diff(source_color, ink_color)
            delta['a'] = abs(source_color.alpha - ink_color.alpha) * 100
            if delta['c'] <= self.options.threshold:
                if not self.options.match_alpha:
                    match = True
                elif delta['a'] <= self.options.threshold:
                    match = True
            if self.options.verbose:
                self.verbose_diff(source_color, ink_color, delta, match)
        # include or exclude matching colors
        if not self.options.exclude_match:
            return match
        else:
            return not match

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""

        source_color = ic.InkColorRGBALong(self.options.from_color)
        target_color = ic.InkColorRGBALong(self.options.to_color)

        if self.options.debug:
            showme('from: {}'.format(source_color.rgba_signed()))
            showme('from: {}'.format(repr(source_color)))
            showme('to: {}'.format(target_color.rgba_signed()))
            showme('to: {}'.format(repr(target_color)))

        # Update with or without alpha
        if self.match_colors(source_color, ink_color):
            if self.options.update_alpha:
                ink_color.from_float(*target_color.rgba_float())
            else:
                ink_color.from_float(*target_color.rgb_float())


if __name__ == '__main__':
    ME = ColorReplace()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
