#!/usr/bin/env python
"""
color_linearize - simulate conversion between sRGB and linearRGB
                  with SVG paint colors (in sRGB)

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol


class ColorLinearize(modcol.ColorRGBA):
    """ColorEffect-based class to simulate sRGB <-> linearRGB conversions."""

    def __init__(self):
        """Init base class and ColorLinearize()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--linear_mode",
                                     action="store", type="string",
                                     dest="linear_mode",
                                     default="sRGB_to_linear",
                                     help="Convert sRGB, linearRGB")

    def convert_linear(self, ink_color):
        """Simulate conversion between sRGB and linearRGB in sRGB."""

        if self.options.linear_mode == 'sRGB_to_linear':

            # reverse-encode 'sRGB_to_linear' in sRGB
            r, g, b = ink_color.rgb_float()
            # update ink_color
            ink_color.from_linear(r, g, b)

        elif self.options.linear_mode == 'linear_to_sRGB':

            # reverse-encode 'linear_to_sRGB' in sRGB
            r, g, b = ink_color.rgb_linear()
            # update ink_color
            ink_color.from_float(r, g, b)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"linear_tab"':
            self.convert_linear(ink_color)


if __name__ == '__main__':
    ME = ColorLinearize()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
