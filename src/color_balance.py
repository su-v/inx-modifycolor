#!/usr/bin/env python
"""
color_balance   - Color balance for SVG paints

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic


def color_balance_map(value, lightness, shadows, midtones, highlights):
    """Color balance map (based on GIMP 2.9)."""

    # constants
    a = 0.25
    b = 0.333
    scale = 0.7

    shadows *= ic.clamp(0.0, ((lightness - b) / -a) + 0.5, 1.0) * scale
    midtones *= (ic.clamp(0.0, ((lightness - b) / a) + 0.5, 1.0) *
                 ic.clamp(0.0, ((lightness + b - 1) / -a) + 0.5, 1.0)) * scale
    highlights *= ic.clamp(0.0, ((lightness + b - 1) / a) + 0.5, 1.0) * scale

    value += shadows
    value += midtones
    value += highlights
    value = ic.clamp(0.0, value, 1.0)

    return value


class ColorBalance(modcol.ColorRGBA):
    """ColorEffect-based class for color balance of SVG paint colors."""

    def __init__(self):
        """Init base class and ColorBalance()."""
        modcol.ColorRGBA.__init__(self)

        # color levels per range
        # shadows
        self.OptionParser.add_option("--s_c_r",
                                     action="store", type="float",
                                     dest="s_c_r", default=0,
                                     help="Shadows: cyan - red")
        self.OptionParser.add_option("--s_m_g",
                                     action="store", type="float",
                                     dest="s_m_g", default=0,
                                     help="Shadows: magenta - green")
        self.OptionParser.add_option("--s_y_b",
                                     action="store", type="float",
                                     dest="s_y_b", default=0,
                                     help="Shadows: yellow - blue")
        # midtones
        self.OptionParser.add_option("--m_c_r",
                                     action="store", type="float",
                                     dest="m_c_r", default=0,
                                     help="Midtones: cyan - red")
        self.OptionParser.add_option("--m_m_g",
                                     action="store", type="float",
                                     dest="m_m_g", default=0,
                                     help="Midtones: magenta - green")
        self.OptionParser.add_option("--m_y_b",
                                     action="store", type="float",
                                     dest="m_y_b", default=0,
                                     help="Midtones: yellow - blue")
        # highlights
        self.OptionParser.add_option("--h_c_r",
                                     action="store", type="float",
                                     dest="h_c_r", default=0,
                                     help="Highlights: cyan - red")
        self.OptionParser.add_option("--h_m_g",
                                     action="store", type="float",
                                     dest="h_m_g", default=0,
                                     help="Highlights: magenta - green")
        self.OptionParser.add_option("--h_y_b",
                                     action="store", type="float",
                                     dest="h_y_b", default=0,
                                     help="Highlights: yellow - blue")
        # common
        self.OptionParser.add_option("--preserve_luminosity",
                                     action="store", type="inkbool",
                                     dest="preserve_luminosity", default=True,
                                     help="Preserve Luminosity")
        self.OptionParser.add_option("--gamma_hack",
                                     action="store", type="inkbool",
                                     dest="gamma_hack", default=False,
                                     help="Gamma hack (temp)")
        # notebooks
        self.OptionParser.add_option("--range_nb",
                                     action="store", type="string",
                                     dest="range_nb",
                                     help="Range notebook")

    def color_balance(self, ink_color):
        """Adjust Color balance for selected color range."""

        # get current sRGB values
        r, g, b = ink_color.rgb_float()
        l = ink_color.hsl_l

        # adjust color levels for color range
        r = color_balance_map(r, l,
                              self.options.s_c_r / 100.0,
                              self.options.m_c_r / 100.0,
                              self.options.h_c_r / 100.0)
        g = color_balance_map(g, l,
                              self.options.s_m_g / 100.0,
                              self.options.m_m_g / 100.0,
                              self.options.h_m_g / 100.0)
        b = color_balance_map(b, l,
                              self.options.s_y_b / 100.0,
                              self.options.m_y_b / 100.0,
                              self.options.h_y_b / 100.0)

        # update ink_color
        ink_color.from_float(r, g, b)

        # preserve luminosity
        if self.options.preserve_luminosity:
            ink_color.hsl_l = l

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"balance_tab"':
            self.color_balance(ink_color)


if __name__ == '__main__':
    ME = ColorBalance()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
