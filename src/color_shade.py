#!/usr/bin/env python
"""
color_shade - eperiment with tint, shade, tones of InkColor objects

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import inkcolor as ic
from color_lib import colorops as co
from color_lib import colorblend as cb


def showme(msg):
    """Verbose output."""
    modcol.inkex.debug(msg)


def invert_color(color, linear):
    """Invert color based on *linear*, return sRGB color."""
    ic_copy = color.copy()
    new_color = color.copy()
    if linear:
        ic_copy.from_float(*color.rgb_linear())
        ic_temp = ~ic_copy
        new_color.from_linear(*ic_temp.rgb_float())
    else:
        ic_temp = ~color
        new_color.from_float(*ic_temp.rgb_float())
    return new_color


def opposite_hue(color, linear):
    """Return sRGB with opposite hue, based on *linear*. Return sRGB color."""
    ic_copy = color.copy()
    new_color = color.copy()
    if linear:
        ic_copy.from_float(*color.rgb_linear())
        ic_copy.hsv_h_deg += 180
        new_color.from_linear(*ic_copy.rgb_float())
    else:
        new_color.hsv_h_deg += 180
    return new_color


class ColorTone(modcol.ColorRGBA):
    """ColorEffect-based class to experiment with tint, shade, tone."""

    def __init__(self):
        """Init base class and ColorTone()."""
        modcol.ColorRGBA.__init__(self)

        # shade
        self.OptionParser.add_option("--shade_mode",
                                     action="store",
                                     type="string",
                                     dest="shade_mode",
                                     default="default",
                                     help="Shade mode")
        self.OptionParser.add_option("--shade_percent",
                                     action="store",
                                     type="int",
                                     dest="shade_percent",
                                     default=50,
                                     help="Mix proportion")
        self.OptionParser.add_option("--shade_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="shade_alpha",
                                     default=True,
                                     help="Update shaded alpha")
        self.OptionParser.add_option("--shade_linear",
                                     action="store",
                                     type="inkbool",
                                     dest="shade_linear",
                                     default=False,
                                     help="Mix in linearRGB")
        # mix
        self.OptionParser.add_option("--mix_mode",
                                     action="store",
                                     type="string",
                                     dest="mix_mode",
                                     default="default",
                                     help="Mix mode")
        self.OptionParser.add_option("--mix_colorspace",
                                     action="store",
                                     type="string",
                                     dest="mix_colorspace",
                                     default="sRGB",
                                     help="Mix color space")
        self.OptionParser.add_option("--mix_percent",
                                     action="store",
                                     type="int",
                                     dest="mix_percent",
                                     default=50,
                                     help="Mix proportion")
        self.OptionParser.add_option("--mix_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="mix_alpha",
                                     default=True,
                                     help="Update mixed alpha")
        self.OptionParser.add_option("--mix_color",
                                     action="store",
                                     type="int",
                                     dest="mix_color",
                                     default=-16776961,
                                     help="Mix source color")
        # blend
        self.OptionParser.add_option("--blend_mode",
                                     action="store",
                                     type="string",
                                     dest="blend_mode",
                                     default="default",
                                     help="Blend mode")
        self.OptionParser.add_option("--blend_order",
                                     action="store",
                                     type="string",
                                     dest="blend_order",
                                     default="custom_over_current",
                                     help="Blend order")
        self.OptionParser.add_option("--blend_colorspace",
                                     action="store",
                                     type="string",
                                     dest="blend_colorspace",
                                     default="sRGB",
                                     help="Blend color space")
        self.OptionParser.add_option("--blend_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="blend_alpha",
                                     default=True,
                                     help="Update blended alpha")
        self.OptionParser.add_option("--blend_color",
                                     action="store",
                                     type="int",
                                     dest="blend_color",
                                     default=-16777088,
                                     help="Source blend color")
        # complement
        self.OptionParser.add_option("--complement_mode",
                                     action="store",
                                     type="string",
                                     dest="complement_mode",
                                     default="default",
                                     help="Complement type")
        self.OptionParser.add_option("--complement_blend",
                                     action="store",
                                     type="inkbool",
                                     dest="complement_blend",
                                     default=False,
                                     help="Blend complement over dest")
        self.OptionParser.add_option("--complement_blend_mode",
                                     action="store",
                                     type="string",
                                     dest="complement_blend_mode",
                                     default="default",
                                     help="Complement blend mode")
        self.OptionParser.add_option("--complement_blend_order",
                                     action="store",
                                     type="string",
                                     dest="complement_blend_order",
                                     default="custom_over_current",
                                     help="Complement blend order")
        self.OptionParser.add_option("--complement_blend_colorspace",
                                     action="store",
                                     type="string",
                                     dest="complement_blend_colorspace",
                                     default="sRGB",
                                     help="Complement blend color space")
        self.OptionParser.add_option("--complement_percent",
                                     action="store",
                                     type="int",
                                     dest="complement_percent",
                                     default=50,
                                     help="Complement proportion")
        self.OptionParser.add_option("--complement_alpha",
                                     action="store",
                                     type="inkbool",
                                     dest="complement_alpha",
                                     default=True,
                                     help="Update complement alpha")
        # common options
        self.OptionParser.add_option("--debug",
                                     action="store",
                                     type="inkbool",
                                     dest="debug",
                                     default=False,
                                     help="Debug output")

    def complement_color(self, color):
        """Test complement color and blending with complement."""
        # pylint: disable=too-many-locals,too-many-branches

        # Parameters
        mode = self.options.complement_mode
        # source = None
        order = self.options.complement_blend_order
        percent = self.options.complement_percent / 100.0
        alpha = self.options.complement_alpha

        # Blending
        blend = self.options.complement_blend
        blend_mode = self.options.complement_blend_mode

        # Blend color space
        linear = self.options.complement_blend_colorspace == 'linearRGB'

        # Experimental modes
        if linear:
            # TODO: verify linear mode
            r, g, b = color.rgb_linear()
            ic_src = color.copy()
            ic_src.from_float(*color.rgb_linear())
        else:
            r, g, b = color.rgb_float()
            ic_src = color.copy()

        # Set complement color
        if mode.startswith('invert'):
            ic_src.from_float(*[1.0 - c for c in (r, g, b)])
        elif mode.startswith('complement_adobe'):
            c_sum = min(r, g, b) + max(r, g, b)
            ic_src.from_float(*[c_sum - c for c in (r, g, b)])
        elif mode.startswith('hue_opposite'):
            ic_src.hsl_h_deg += 180

        if linear:
            # TODO: verify linear mode (blender func below expects sRGB)
            ic_src.from_linear(*ic_src.rgb_float())

        # Blend complement color over current color (or reverse)
        if blend:  # source: alpha = percent, dest: alpha = 1.0
            blender = getattr(cb, blend_mode[:blend_mode.rfind('_')], None)
            func = getattr(cb, blend_mode, None)
            if blender is not None and func is not None:
                ic_dest = color.copy()
                if order == 'custom_over_current':
                    ic_dest.alpha = 1.0
                    ic_src.alpha = percent
                    ic_out = blender(ic_dest, ic_src, func, linear)
                else:
                    ic_dest.alpha = percent
                    ic_src.alpha = 1.0
                    ic_out = blender(ic_src, ic_dest, func, linear)
            else:
                ic_out = ic_src
        else:
            ic_out = ic_src

        # Update color with or without complemented alpha
        if alpha:
            # showme('from {} to {}'.format(color.alpha, ic_out.alpha))
            color.from_float(*ic_out.rgba_float())
        else:
            color.from_float(*ic_out.rgb_float())

    def blend_colors(self, ic_dst):
        """Test various blending mode with current color *ic_dst*."""

        # Parameters
        mode = self.options.blend_mode
        source = self.options.blend_color
        order = self.options.blend_order
        # percent = None
        alpha = self.options.blend_alpha

        # Blend color space
        linear = self.options.blend_colorspace == 'linearRGB'

        # Custom color for blend source or backdrop (depends on blend order)
        ic_src = ic.InkColorRGBALong(source)

        # Blender functions
        blender = getattr(cb, mode[:mode.rfind('_')], None)
        func = getattr(cb, mode, None)

        # Blend custom color with current color (depends on blend order)
        if blender is not None and func is not None:
            if order == 'custom_over_current':
                ic_out = blender(ic_dst, ic_src, func, linear)
            else:
                ic_out = blender(ic_src, ic_dst, func, linear)
        else:
            ic_out = ic_dst

        # Update ic_dst with or without blended alpha
        if alpha:
            # showme('from {} to {}'.format(ic_dst.alpha, ic_out.alpha))
            ic_dst.from_float(*ic_out.rgba_float())
        else:
            ic_dst.from_float(*ic_out.rgb_float())

    def mix_colors(self, ic_dst):
        """Test color mixing with current color *ic_dst*."""

        # parameters
        mode = self.options.mix_mode
        source = self.options.mix_color
        # order = None
        percent = self.options.mix_percent / 100.0
        alpha = self.options.mix_alpha

        # Mix color space
        linear = self.options.mix_colorspace == 'linearRGB'

        # mix custom color with current color
        if mode.endswith('mix_weighted'):
            ic_src = ic.InkColorRGBALong(source)
            ic_out = co.color_mix(ic_src, ic_dst, percent, linear)
        else:
            ic_out = ic_dst

        # Update ic_dst with or without mixed alpha
        if alpha:
            # showme('from {} to {}'.format(ic_dst.alpha, ic_out.alpha))
            ic_dst.from_float(*ic_out.rgba_float())
        else:
            ic_dst.from_float(*ic_out.rgb_float())

    def shade_color(self, ic_dst):
        """Modify tint, shade, tone of current color *ic_dst*."""

        # parameters
        mode = self.options.shade_mode
        # source = None
        # order = None
        percent = self.options.shade_percent / 100.0
        alpha = self.options.shade_alpha

        # Mix color space
        linear = self.options.shade_linear

        # get tinted, shaded or toned color
        if mode == 'tint':
            ic_out = co.color_tint(ic_dst, percent, linear)
        elif mode == 'shade':
            ic_out = co.color_shade(ic_dst, percent, linear)
        elif mode == 'tone':
            ic_out = co.color_tone(ic_dst, percent, linear)
        elif mode == 'tone_invert':
            # if linear, invert in linearRGB
            ic_src = invert_color(ic_dst, linear)
            ic_out = co.color_mix(ic_src, ic_dst, percent, linear)
        elif mode == 'tone_opposite':
            # if linear, create hue opposite in linearRGB
            ic_src = opposite_hue(ic_dst, linear)
            ic_out = co.color_mix(ic_src, ic_dst, percent, linear)
        else:  # default
            ic_out = ic_dst

        # Update ic_dst with or without shaded alpha
        if alpha:
            # showme('from {} to {}'.format(ic_dst.alpha, ic_out.alpha))
            ic_dst.from_float(*ic_out.rgba_float())
        else:
            ic_dst.from_float(*ic_out.rgb_float())

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"shade_tab"':
            self.shade_color(ink_color)
        elif self.options.tab == '"mix_tab"':
            self.mix_colors(ink_color)
        elif self.options.tab == '"blend_tab"':
            self.blend_colors(ink_color)
        elif self.options.tab == '"complement_tab"':
            self.complement_color(ink_color)


if __name__ == '__main__':
    ME = ColorTone()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
