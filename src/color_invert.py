#!/usr/bin/env python
"""
color_invert - invert SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colorconst as cc
from color_lib import colormatrix as cm


class ColorInvert(modcol.ColorRGBA):
    """ColorEffect-based class to invert SVG paint colors."""

    def __init__(self):
        """Init base class and ColorInvert()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--invert_channels",
                                     action="store", type="string",
                                     dest="invert_channels", default="none",
                                     help="Invert channels")
        self.OptionParser.add_option("--invert_opacify",
                                     action="store", type="float",
                                     dest="invert_opacify", default=0.0,
                                     help="Light transparency")
        self.OptionParser.add_option("--invert_hue",
                                     action="store", type="inkbool",
                                     dest="invert_hue", default=True,
                                     help="Invert hue")
        self.OptionParser.add_option("--invert_lightness",
                                     action="store", type="inkbool",
                                     dest="invert_lightness", default=True,
                                     help="Invert lightness")
        self.OptionParser.add_option("--invert_transparency",
                                     action="store", type="inkbool",
                                     dest="invert_transparency", default=False,
                                     help="Invert transparency")

    def invert_color(self, ink_color):
        """
        Invert RGB color values.

        Based on CFP (Inkscape 0.91)
        """

        # Transparency option only works as filter effect (CPF)
        opacify = 0.0                   # self.options.invert_opacify
        invert_transparency = False     # self.options.invert_transparency

        # fixed amount for hue rotation
        angle = 180.0                   # self.options.inv_hue_rotate

        # invert color channels
        channels = self.options.invert_channels

        # lightness parameters
        if self.options.invert_lightness:
            light_123 = -1.0
            light_5 = 1.0
        else:
            light_123 = 1.0
            light_5 = 0.0

        # first feColorMatrix filter primitive, rotate hue

        if self.options.invert_hue != self.options.invert_lightness:
            r, g, b = ink_color.rgb_float()
            ink_color.from_float(*cm.hue_rotate(r, g, b, angle))

        # second feColorMatrix filter primitive

        mat = [[0.0 for _ in range(5)] for _ in range(5)]
        for i in (3, 4):
            mat[i][i] = 1.0

        # col1, col2, col3
        if channels == "none":
            mat[0][0] = light_123
            mat[1][1] = light_123
            mat[2][2] = light_123
        elif channels == "red_blue":
            mat[0][2] = light_123
            mat[1][1] = light_123
            mat[2][0] = light_123
        elif channels == "green_red":
            mat[0][1] = light_123
            mat[1][0] = light_123
            mat[2][2] = light_123
        elif channels == "blue_green":
            mat[0][0] = light_123
            mat[1][2] = light_123
            mat[2][1] = light_123
        # 4th row
        if invert_transparency:
            mat[3][0] = cc.IPR
            mat[3][1] = cc.IPG
            mat[3][2] = cc.IPB
            mat[3][3] = 1 - opacify
        else:
            mat[3][0] = -cc.IPR
            mat[3][1] = -cc.IPG
            mat[3][2] = -cc.IPB
            mat[3][3] = 2 - opacify
        # col5
        for i in range(3):
            mat[i][4] = light_5

        # apply matrix
        r, g, b = ink_color.rgb_float()
        mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
        if mat_rgb_new is not None:
            ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"invert_tab"':
            self.invert_color(ink_color)


if __name__ == '__main__':
    ME = ColorInvert()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
