#!/usr/bin/env python
"""
color_lightness_contrast - adjust lightness contrast of SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorLightness(modcol.ColorRGBA):
    """ColorEffect-based class to adjust lightness in SVG paint colors."""

    def __init__(self):
        """Init base class and ColorLightness()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--li_co_lightness",
                                     action="store", type="float",
                                     dest="li_co_lightness", default=0.0,
                                     help="Adjust RGB Lightness")
        self.OptionParser.add_option("--li_co_contrast",
                                     action="store", type="float",
                                     dest="li_co_contrast", default=0.0,
                                     help="Adjust RGB contrast")
        # common
        self.OptionParser.add_option("--linear",
                                     action="store", type="inkbool",
                                     dest="linear", default=False,
                                     help="Adjust in linearRGB")

    def adjust_lightness(self, ink_color):
        """
        Adjust lightness of RGB color.

        Based on CPF (Inkscape 0.91)
        """

        if self.options.li_co_contrast or self.options.li_co_lightness:

            # adjust contrast
            if self.options.li_co_contrast > 0:
                contrast = 1 + (self.options.li_co_contrast / 10.0)
                column5 = (-1 * self.options.li_co_contrast) / 20.0
            else:
                contrast = 1 + (self.options.li_co_contrast / 100.0)
                column5 = (-1 * self.options.li_co_contrast) / 200.0

            # adjust lightness (factors in contrast)
            lightness = (1 - column5) * (self.options.li_co_lightness / 100.0)

            # color matrix based on contrast and lightness
            mat = [[0.0 for _ in range(5)] for _ in range(5)]
            for i in range(3):
                mat[i][i] = contrast
                mat[i][3] = lightness
                mat[i][4] = column5
            for i in (3, 4):
                mat[i][i] = 1.0

            # apply matrix
            r, g, b = ink_color.rgb_float()
            mat_rgb_new = cm.matrixmult(mat, cm.rgb2matrix(r, g, b))
            if mat_rgb_new:
                ink_color.from_float(*cm.matrix2rgb(mat_rgb_new))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"lightness_contrast_tab"':
            # experimental: convert to linearRGB
            if self.options.linear:
                ink_color.from_float(*ink_color.rgb_linear())
            # adjust color with colormatrix
            self.adjust_lightness(ink_color)
            # exerimental: convert back to sRGB
            if self.options.linear:
                ink_color.from_linear(*ink_color.rgb_float())


if __name__ == '__main__':
    ME = ColorLightness()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
