#!/usr/bin/env python
"""
color_sepia - apply Sepia effect to SVG paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
from color_lib import common as modcol
from color_lib import colorconvert as cv
from color_lib import colorops as co


def sepia_default(r_in, g_in, b_in):
    """Default sepia function."""
    # default (Microsoft) via pixastic
    # http://www.pixastic.com/lib/docs/actions/sepia/
    r_out = r_in*0.393 + g_in*0.769 + b_in*0.189
    g_out = r_in*0.349 + g_in*0.686 + b_in*0.168
    b_out = r_in*0.272 + g_in*0.534 + b_in*0.131
    return r_out, g_out, b_out


def sepia_alternate(r_in, g_in, b_in):
    """Alternate sepia function."""
    # alternate matrix from
    # http://stackoverflow.com/a/9709217
    r_out = r_in*0.3588 + g_in*0.7044 + b_in*0.1368
    g_out = r_in*0.2990 + g_in*0.5870 + b_in*0.1140
    b_out = r_in*0.2392 + g_in*0.4696 + b_in*0.0912
    return r_out, g_out, b_out


class ColorSepia(modcol.ColorRGBA):
    """ColorEffect-based class for Sepia effect on SVG paint colors."""

    def __init__(self):
        """Init base class and ColorSepia()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--sepia_mode",
                                     action="store", type="string",
                                     dest="sepia_mode", default="default",
                                     help="Sepia mode")
        self.OptionParser.add_option("--sepia_yiq_y",
                                     action="store", type="int",
                                     dest="sepia_yiq_y", default="0",
                                     help="YIQ: Y channel delta (%)")
        self.OptionParser.add_option("--sepia_yiq_i",
                                     action="store", type="float",
                                     dest="sepia_yiq_i", default="0.0",
                                     help="YIQ: I channel value (absolute)")
        self.OptionParser.add_option("--sepia_yiq_q",
                                     action="store", type="float",
                                     dest="sepia_yiq_q", default="0.0",
                                     help="YIQ: Q channel value (absolute)")

    def sepia_presets(self, ink_color):
        """Apply Sepia effect to ink_color."""
        if self.options.sepia_mode == 'css_sepia':
            new_color = co.css_sepia(ink_color, 1.0)
            ink_color.from_float(*new_color.rgb_float())
        elif self.options.sepia_mode == "default":
            ink_color.from_float(*sepia_default(*ink_color.rgb_float()))
        elif self.options.sepia_mode == "alternate":
            ink_color.from_float(*sepia_alternate(*ink_color.rgb_float()))
        elif self.options.sepia_mode == "yiq1":
            self.sepia_yiq(ink_color, 0, 0.2, 0)
        elif self.options.sepia_mode == "yiq2":
            self.sepia_yiq(ink_color, 0, 0.0750, 0.0010)

    def sepia_yiq(self, ink_color, val_y=None, val_i=None, val_q=None):
        """Apply Sepia effect via YIQ to InkColor."""

        # Based on:
        # https://en.wikipedia.org/wiki/YIQ
        #
        # "The Y component represents the luma information, and is the only
        # component used by black-and-white television receivers. I and Q
        # represent the chrominance information. In YUV, the U and V components
        # can be thought of as X and Y coordinates within the color space. I
        # and Q can be thought of as a second pair of axes on the same graph,
        # rotated 33 deg; therefore IQ and UV represent different coordinate
        # systems on the same plane."
        # https://en.wikipedia.org/wiki/YIQ#/media/File:YIQ_IQ_plane.svg

        # parameters:
        if val_y is None:
            val_y = self.options.sepia_yiq_y / 100.0
        if val_i is None:
            val_i = self.options.sepia_yiq_i
        if val_q is None:
            val_q = self.options.sepia_yiq_q

        # convert ink_color to YIQ
        y, i, q = cv.rgb_to_yiq(*ink_color.rgb_float())

        # adjust (Y) or set (I, Q) parameters
        y += val_y
        i = val_i
        q = val_q

        # update ink_color
        ink_color.from_float(*cv.yiq_to_rgb(y, i, q))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"sepia_tab"':
            self.sepia_presets(ink_color)
        elif self.options.tab == '"sepia_yiq_tab"':
            self.sepia_yiq(ink_color)


if __name__ == '__main__':
    ME = ColorSepia()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
