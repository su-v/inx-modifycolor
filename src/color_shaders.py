#!/usr/bin/env python
"""
color_posterize - posterize SVG paint colors

Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import math

# local library
from color_lib import common as modcol


def func_1(color, curve):
    """Extracted function 1 for 35mm shader effect."""
    color -= 0.5
    dividend = 1/(1 + math.exp(-curve * color)) - 1/(1 + math.exp(curve / 2.0))
    divisor = 1 - 2*(1/(1 + math.exp(curve / 2.0)))
    return dividend / divisor


def func_2(lightness, color):
    """Extracted function 2 for 35mm shader effect."""
    if lightness < 0.5:
        return (2*lightness - 1)*(color - color**2) + color
    else:
        return (2*lightness - 1)*(math.sqrt(color) - color) + color


class ColorShaders(modcol.ColorRGBA):
    """ColorEffect-based class to apply contributed shader effects."""

    def __init__(self):
        """Init base class and ColorShaders()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--loadus_mode",
                                     action="store", type="string",
                                     dest="loadus_mode", default="35mm",
                                     help="Loadus' shader types")

    def loadus_shaders(self, ink_color):
        """Apply one of Loadus' ported Pixel shader effects."""

        # Shared by Loadus:
        # <quote>
        # I had some spare time this weekend and transferred my pixel shaders
        # to Inkscape. I made them for my own works, but I thought I'd share
        # them anyways. Have fun with them. :D
        # </quote>
        # http://www.inkscapeforum.com/viewtopic.php?f=11&t=9158&p=33947

        if self.options.loadus_mode == "35mm":
            r, g, b = ink_color.rgb_float()
            # parameters
            lightness = (max(r, g, b) + min(r, g, b)) / 2.0
            r_curve = 6.0
            g_curve = 2.0
            b_curve = 1.0
            # apply functions
            r = func_2(lightness, func_1(r, r_curve))
            g = func_2(lightness, func_1(g, g_curve))
            b = func_2(lightness, func_1(b, b_curve))
            # update ink_color
            ink_color.from_float(r, g, b)

        elif self.options.loadus_mode == "linearize":
            r, g, b = [pow(c, 0.45) for c in ink_color.rgb_float()]
            # update ink_color
            ink_color.from_float(r, g, b)

        elif self.options.loadus_mode == "vintage1":
            r, g, b = ink_color.rgb_float()
            r = r * 0.75 + g * 0.25
            g = g * 0.75 + b * 0.25
            b = b * 0.75 + b * 0.25
            # update ink_color
            ink_color.from_float(r, g, b)

        elif self.options.loadus_mode == "vintage2":
            r, g, b = ink_color.rgb_float()
            g = g * 0.90 + 0.10
            b = b * 0.60 + 0.20
            # update ink_color
            ink_color.from_float(r, g, b)

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"loadus_tab"':
            self.loadus_shaders(ink_color)


if __name__ == '__main__':
    ME = ColorShaders()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
