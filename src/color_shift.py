#!/usr/bin/env python
"""
color_shift - shift values of SVG RGB paint colors

Copyright (C) 2013, 2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# local library
from color_lib import common as modcol
from color_lib import colormatrix as cm


class ColorShift(modcol.ColorRGBA):
    """ColorEffect-based class to shift values of SVG paint colors."""

    def __init__(self):
        """Init base class and ColorShift()."""
        modcol.ColorRGBA.__init__(self)

        self.OptionParser.add_option("--shift_defaults",
                                     action="store", type="inkbool",
                                     dest="shift_defaults",
                                     default=True,
                                     help="Use default values")
        self.OptionParser.add_option("--shift_shift",
                                     action="store", type="int",
                                     dest="shift_shift", default=330,
                                     help="Shift")
        self.OptionParser.add_option("--shift_sat",
                                     action="store", type="float",
                                     dest="shift_sat", default=0.6,
                                     help="Saturation")

    def shift_color(self, ink_color):
        """
        Shift RGB color values.

        Based on CPF (Inkscape 0.91)
        """

        # parameters
        if self.options.shift_defaults:
            shift = 330
            saturation = 0.6
        else:
            shift = self.options.shift_shift
            saturation = self.options.shift_sat

        if not (shift == 0 or shift == 360):
            # 1. feColorMatrix: Shift
            r, g, b = ink_color.rgb_float()
            ink_color.from_float(*cm.hue_rotate(r, g, b, shift))
        if saturation != 1.0:
            # 2. feColorMatrix: Saturate
            r, g, b = ink_color.rgb_float()
            ink_color.from_float(*cm.saturate(r, g, b, saturation))

    def modify_color(self, ink_color):
        """Modify SVG paint color values."""
        if self.options.tab == '"shift_tab"':
            self.shift_color(ink_color)


if __name__ == '__main__':
    ME = ColorShift()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
