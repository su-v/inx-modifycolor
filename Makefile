NAME = inx-modifycolor

docs = \
       COPYING \
       README \
       TODO

inx = \
      src/color_adjust.inx \
      src/color_adjust.py \
      src/color_adjust_range.inx \
      src/color_adjust_range.py \
      src/color_balance.inx \
      src/color_balance.py \
      src/color_black_white.inx \
      src/color_black_white.py \
      src/color_blend.inx \
      src/color_blend.py \
      src/color_blindness.inx \
      src/color_blindness.py \
      src/color_brightness.inx \
      src/color_brightness.py \
      src/color_brilliance.inx \
      src/color_brilliance.py \
      src/color_channel_extract.inx \
      src/color_channel_extract.py \
      src/color_colorize.inx \
      src/color_colorize.py \
      src/color_colormap.inx \
      src/color_colormap.py \
      src/color_duochrome.inx \
      src/color_duochrome.py \
      src/color_fade2bw.inx \
      src/color_fade2bw.py \
      src/color_fluorescence.inx \
      src/color_fluorescence.py \
      src/color_gamma.inx \
      src/color_gamma.py \
      src/color_greyscale.inx \
      src/color_greyscale.py \
      src/color_hue_rotate.inx \
      src/color_hue_rotate.py \
      src/color_interpolate.inx \
      src/color_interpolate.py \
      src/color_interpolate_attr.inx \
      src/color_interpolate_attr.py \
      src/color_invert.inx \
      src/color_invert.py \
      src/color_lightness_contrast.inx \
      src/color_lightness_contrast.py \
      src/color_linearize.inx \
      src/color_linearize.py \
      src/color_multichrome.inx \
      src/color_multichrome.py \
      src/color_named.inx \
      src/color_named.py \
      src/color_palette.inx \
      src/color_palette.py \
      src/color_posterize.inx \
      src/color_posterize.py \
      src/color_random.inx \
      src/color_random.py \
      src/color_remap.inx \
      src/color_remap.py \
      src/color_replace_custom.inx \
      src/color_replace_custom.py \
      src/color_replace_many.inx \
      src/color_replace_many.py \
      src/color_replace_random.inx \
      src/color_replace_random.py \
      src/color_schemes.inx \
      src/color_schemes.py \
      src/color_sepia.inx \
      src/color_sepia.py \
      src/color_shade.inx \
      src/color_shade.py \
      src/color_shaders.inx \
      src/color_shaders.py \
      src/color_shift.inx \
      src/color_shift.py \
      src/color_solarize.inx \
      src/color_solarize.py \
      src/color_swap.inx \
      src/color_swap.py


modules = \
	  src/color_lib/__init__.py \
	  src/color_lib/colorblend.py \
	  src/color_lib/colorconst.py \
	  src/color_lib/colorconvert.py \
	  src/color_lib/colordiff.py \
	  src/color_lib/colorinterp.py \
	  src/color_lib/colormatrix.py \
	  src/color_lib/colorops.py \
	  src/color_lib/colorsort.py \
	  src/color_lib/common.py \
	  src/color_lib/inkcolor.py \
	  src/color_lib/util.py

data =

# -----

source = $(inx) $(modules)

ChangeLog.txt : $(docs) $(source) $(data)
	rm -f $@
	git log --oneline > $@

all = ChangeLog.txt $(docs) $(source) $(data)

revno = $(shell git rev-parse --short HEAD)
tag = $(shell git tag | tail -1)

.PHONY:	list
list:	$(all)
	@for i in $(all); do \
		echo $$i; \
	done

.PHONY:	zip
zip:	$(all)
	zip "$(NAME)-$(revno).zip" -X $(all)
	@echo "$(NAME)-$(revno).zip"

.PHONY: release
release: \
	$(all)
	zip "$(NAME)-$(tag).zip" -X $(all)
	@echo "$(NAME)-$(tag).zip"
