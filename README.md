# inx-modifycolor

A set of Inkscape extensions to modify RGB(A) colors used in SVG paints.

### Basic operations:
- Create palette of used colors
- Create palette with multi-hue color scale or color scheme
- Display color stats about unique colors processed in drawing
- Adjust color components (RGB, HSL, HSV, HLS, Alpha)
- Adjust brightness/contrast
- Randomize colors
- Replace a color (within a threshold)
- Create, convert to and from named colors
- Swap or copy fill, stroke paints
- Basic color effects (Greyscale, Invert, Hue Rotate)
- Custom color effects (Brilliance, Fade to B&W, Sepia, Shift)


# Requirements

The python-based script extensions have no special external requirements
apart from Inkscape's own python helper modules for extensions.


# Basic Usage

Open a simple drawing with vector elements, apply 'Modify Color > Adjust > Adjust Components'.


# Input and Scope

The modifications apply to a current selection or to the whole drawing
(if nothing is selected). All SVG elements within such a scope are
processed recursively to extract any color information from the
element's presentation attributes or inline CSS style properties.

Within the selection scope, color-modifying extensions can be further
limited to only process colors referenced by fill paints, or by stroke
paints. Individual extensions can add custom scopes (e.g. Randomize).

The use cases can be divided in two main areas:

- Basic color adjustments similar to Inkscape's own extensions based on
  the ColorEffect() class
- Color effects ported from Inkscape's CPF (Custom Predefined Filters)  
  (portable CPFs use feColorMatrix primitives exclusively)

Known limitations:  
Bitmap images embedded or linked in SVG documents are not affected by
these extensions (see '[Extensions > Raster](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Extensions-Raster.html)' and [su-v/inx-modifyimage](https://gitlab.com/su-v/inx-modifyimage) ).


# Output

Most extensions modify the colors used in elements of the drawing.
Shared resources with color information (gradients, patterns) are forked
and relinked before any of their colors are modified, all other colors
are modified in-place (in the element's 'style' attribute, or one of the
presentation attributes).

The extensions optionally return color statistics about unique used
colors before and after the color modifications.

Some extensions may only add new elements without changing existing
colors: the 'Render > Color Palette' extension draws a palette of
rectangles with all (or most-used) unique colors, sorted by use count,
RGB, HSL, HSV, CIE Lab or LCH components.


# Internals

Motivation to write yet another coloreffect module:
- InkColor() class for color objects
- Support for RGBA in color effects (paint-specific alpha)
- Investigate color modifications in linearRGB  
  demo: [interpolate](https://dl.dropboxusercontent.com/u/65084033/irc/Screen%20Recording%2016.mp4)
- Color differences (similar colors within variable threshold)

### InkColor class:
Written in response to [lp1575817](https://bugs.launchpad.net/inkscape/+bug/1575817) to test encapsulating Inkscape-related
color information in objects (includes input from a numeric RGBA integer
value as passed from Inkscape, conversions between different color
representations (RGB, HSL, HSV), and various output formats).

### Support for RGBA:
Recent [additions](https://bugs.launchpad.net/inkscape/+bug/1579939) to Inkscape's own ColorEffect() class added support to
modify an element's global opacity property (for randomize). This type
of 'alpha' component does not really correspond to a color's alpha value
though - one of the goals to rewrite the ColorEffect class was to
extract available 'alpha' information per processed color (fill-opacity,
stroke-opacity, stop-opacity etc.), and allow color extensions to thus
manipulate each color's alpha channel.

The color information is stored as InkColor object which is then passed
to methods defined in sub-classes of ColorEffect() where it can be
modified in-place.

### Color interpolation:
Investigating different presets for greyscale conversions renewed the
interest in 'sRGB vs linearRGB' and its effects on color interpolation.
Experiments with color interpolations in linearRGB, HSL, CIE Lab and LCH
have been added to custom versions of Inkscape's 'Interpolate' and
'Interpolate attribute in a group' extensions.

### Color differences:
Based on user request, new options to replace/adjust colors within a
certain range or threshold are investigated. Test implementions have
been added to 'Adjust > Replace' and in a separate extension 'Adjust >
Adjust Range'.  DeltaE (CIE94) is used to compare colors within a
threshold of a custom source color, color ranges can be defined via
components of RGB, HSL and HSV color representations.


Files and Installation
======================

Copy all *.py and *.inx files as well as the sub-directory 'color_lib/'
from 'src/' to the extensions directory in the Inkscape user config
directory:
- Linux/OS X:   ```~/.config/inkscape/extensions```
- Windows:      ```%APPDATA%\inkscape\extensions```

and relaunch Inkscape.

### The installed extensions will be available as:

#### Extensions > Modify Color:
- Adjust:
  - Adjust Components
  - Adjust Range
  - Black and White
  - Brightness-Contrast
  - Color Balance
  - Colorize
  - Colormap
  - Gamma
  - Greyscale
  - Invert
  - Lightness-Contrast
  - Linearize
  - Randomize
  - Remap Black, White
  - Replace
- Effects:
  - Brilliance
  - Color Blindness
  - Duochrome
  - Fade to Black or White
  - Fluorescence
  - Posterize
  - Sepia
  - Shaders
  - Shift
  - Solarize
- Interpolate:
  - Interpolate (Attributes)
  - Interpolate (Paths, Style)
- Manage:
  - Named Colors
  - Replace Many
  - Replace Random
  - Swap Paints
- Render:
  - Color Palette
  - Color Schemes
- Temp:
  - Blend with custom color
  - Extract Channel
  - Hue Rotate
  - Multichrome
  - Tints, Shades, Tones, Complement


# Source

The extensions are developed and maintained at:  
https://gitlab.com/su-v/inx-modifycolor

ZIP archives:  
https://gitlab.com/su-v/inx-modifycolor/tags

Related feature requests for Inkscape:  
**NA**

Examples:  
**NA**


# License

Same as Inkscape used to be (GPL-2+)
